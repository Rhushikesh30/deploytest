"use strict";
(self["webpackChunkatrio"] = self["webpackChunkatrio"] || []).push([["src_app_common-setup_common-setup_module_ts"],{

/***/ 3035:
/*!*************************************************************!*\
  !*** ./src/app/common-setup/common-setup-routing.module.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CommonSetupRoutingModule": () => (/* binding */ CommonSetupRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _pages_role_link_pages_role_link_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages-role-link/pages-role-link.component */ 5601);
/* harmony import */ var _role_tenant_define_role_tenant_define_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./role-tenant-define/role-tenant-define.component */ 1549);
/* harmony import */ var _tenant_hierarchy_define_tenant_hierarchy_define_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tenant-hierarchy-define/tenant-hierarchy-define.component */ 5560);
/* harmony import */ var _tenant_onboard_tenant_onboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./tenant-onboard/tenant-onboard.component */ 9623);
/* harmony import */ var _user_registration_user_registration_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./user-registration/user-registration.component */ 3459);








const routes = [
    {
        path: 'tenant-hierarchy-define',
        component: _tenant_hierarchy_define_tenant_hierarchy_define_component__WEBPACK_IMPORTED_MODULE_2__.TenantHierarchyDefineComponent,
    },
    {
        path: 'tenant-onboard',
        component: _tenant_onboard_tenant_onboard_component__WEBPACK_IMPORTED_MODULE_3__.TenantOnboardComponent,
    },
    {
        path: 'user-registration',
        component: _user_registration_user_registration_component__WEBPACK_IMPORTED_MODULE_4__.UserRegistrationComponent,
    },
    {
        path: 'role-tenant-define',
        component: _role_tenant_define_role_tenant_define_component__WEBPACK_IMPORTED_MODULE_1__.RoleTenantDefineComponent,
    },
    {
        path: 'pages-role-link',
        component: _pages_role_link_pages_role_link_component__WEBPACK_IMPORTED_MODULE_0__.PagesRoleLinkComponent,
    },
];
let CommonSetupRoutingModule = class CommonSetupRoutingModule {
};
CommonSetupRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_7__.RouterModule]
    })
], CommonSetupRoutingModule);



/***/ }),

/***/ 9192:
/*!*****************************************************!*\
  !*** ./src/app/common-setup/common-setup.module.ts ***!
  \*****************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CommonSetupModule": () => (/* binding */ CommonSetupModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _common_setup_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./common-setup-routing.module */ 3035);
/* harmony import */ var _tenant_hierarchy_define_tenant_hierarchy_define_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tenant-hierarchy-define/tenant-hierarchy-define.component */ 5560);
/* harmony import */ var _tenant_onboard_tenant_onboard_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./tenant-onboard/tenant-onboard.component */ 9623);
/* harmony import */ var _shared_components_components_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../shared/components/components.module */ 5626);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/form-field */ 9076);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/input */ 3365);
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/snack-bar */ 2528);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_20__ = __webpack_require__(/*! @angular/material/icon */ 5590);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_21__ = __webpack_require__(/*! @angular/material/select */ 1434);
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_22__ = __webpack_require__(/*! @angular/material/slide-toggle */ 6623);
/* harmony import */ var _tenant_onboard_tenant_onboard_details_tenant_onboard_details_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./tenant-onboard/tenant-onboard-details/tenant-onboard-details.component */ 6222);
/* harmony import */ var _user_registration_user_registration_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./user-registration/user-registration.component */ 3459);
/* harmony import */ var _user_registration_user_registration_details_user_registration_details_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-registration/user-registration-details/user-registration-details.component */ 5636);
/* harmony import */ var _role_tenant_define_role_tenant_define_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./role-tenant-define/role-tenant-define.component */ 1549);
/* harmony import */ var _role_tenant_define_role_tenant_define_details_role_tenant_define_details_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./role-tenant-define/role-tenant-define-details/role-tenant-define-details.component */ 3083);
/* harmony import */ var _pages_role_link_pages_role_link_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./pages-role-link/pages-role-link.component */ 5601);
/* harmony import */ var _pages_role_link_pages_role_link_details_pages_role_link_details_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./pages-role-link/pages-role-link-details/pages-role-link-details.component */ 4643);























let CommonSetupModule = class CommonSetupModule {
};
CommonSetupModule = (0,tslib__WEBPACK_IMPORTED_MODULE_11__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_12__.NgModule)({
        declarations: [
            _tenant_hierarchy_define_tenant_hierarchy_define_component__WEBPACK_IMPORTED_MODULE_1__.TenantHierarchyDefineComponent,
            _tenant_onboard_tenant_onboard_component__WEBPACK_IMPORTED_MODULE_2__.TenantOnboardComponent,
            _tenant_onboard_tenant_onboard_details_tenant_onboard_details_component__WEBPACK_IMPORTED_MODULE_4__.TenantOnboardDetailsComponent,
            _user_registration_user_registration_component__WEBPACK_IMPORTED_MODULE_5__.UserRegistrationComponent,
            _user_registration_user_registration_details_user_registration_details_component__WEBPACK_IMPORTED_MODULE_6__.UserRegistrationDetailsComponent,
            _role_tenant_define_role_tenant_define_component__WEBPACK_IMPORTED_MODULE_7__.RoleTenantDefineComponent,
            _role_tenant_define_role_tenant_define_details_role_tenant_define_details_component__WEBPACK_IMPORTED_MODULE_8__.RoleTenantDefineDetailsComponent,
            _pages_role_link_pages_role_link_component__WEBPACK_IMPORTED_MODULE_9__.PagesRoleLinkComponent,
            _pages_role_link_pages_role_link_details_pages_role_link_details_component__WEBPACK_IMPORTED_MODULE_10__.PagesRoleLinkDetailsComponent,
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_13__.CommonModule,
            _common_setup_routing_module__WEBPACK_IMPORTED_MODULE_0__.CommonSetupRoutingModule,
            _shared_components_components_module__WEBPACK_IMPORTED_MODULE_3__.ComponentsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_14__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_14__.ReactiveFormsModule,
            _angular_material_paginator__WEBPACK_IMPORTED_MODULE_15__.MatPaginatorModule,
            _angular_material_table__WEBPACK_IMPORTED_MODULE_16__.MatTableModule,
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_17__.MatFormFieldModule,
            _angular_material_input__WEBPACK_IMPORTED_MODULE_18__.MatInputModule,
            _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_19__.MatSnackBarModule,
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_20__.MatIconModule,
            _angular_material_select__WEBPACK_IMPORTED_MODULE_21__.MatSelectModule,
            _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_22__.MatSlideToggleModule
        ]
    })
], CommonSetupModule);



/***/ }),

/***/ 4643:
/*!***********************************************************************************************************!*\
  !*** ./src/app/common-setup/pages-role-link/pages-role-link-details/pages-role-link-details.component.ts ***!
  \***********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesRoleLinkDetailsComponent": () => (/* binding */ PagesRoleLinkDetailsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _pages_role_link_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages-role-link-details.component.html?ngResource */ 9968);
/* harmony import */ var _pages_role_link_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages-role-link-details.component.sass?ngResource */ 1322);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/common-setup.service */ 6491);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);
/* harmony import */ var src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/shared/services/master.service */ 6754);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_5__);









let PagesRoleLinkDetailsComponent = class PagesRoleLinkDetailsComponent {
    constructor(formBuilder, manageSecurityService, commonsetupservice, masterService) {
        this.formBuilder = formBuilder;
        this.manageSecurityService = manageSecurityService;
        this.commonsetupservice = commonsetupservice;
        this.masterService = masterService;
        this.onSave = new _angular_core__WEBPACK_IMPORTED_MODULE_6__.EventEmitter();
        this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_6__.EventEmitter();
        this.cancelFlag = true;
        this.showReset = true;
        this.submitted = false;
        this.BTN_VAL = 'Submit';
        this.tenantData = [];
        this.roleData = [];
        this.userData = [];
        this.leftpaneldata = [];
        this.assign_screen_to_role = [];
        this.color = 'primary';
        this.displayColumn = [
            'check',
            'form_name',
            'read_access',
            'write_access',
            'delete_access'
        ];
        this.shouldDisable = false;
    }
    ngOnInit() {
        this.pagesToRoleForm = this.formBuilder.group({
            tenant_id_id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
            role_id_id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
            user_id_id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_7__.Validators.required],
            is_active: true,
            initialItemRow: this.formBuilder.array([this.initialItemRow()])
        });
        this.manageSecurityService.getLeftPanel().subscribe((data) => {
            let result = [];
            let parent_codes = data.filter(sr => { return sr.is_parent == true; });
            parent_codes.forEach(element => {
                let d = data.filter(srch => {
                    return element.parent_code === srch.parent_code;
                });
                d.forEach(ele => result.push(ele));
            });
            this.leftpaneldata = result.sort((a, b) => a['sequenceId'] > b['sequenceId'] ? 1 : a['sequenceId'] === b['sequenceId'] ? 0 : -1);
            while (this.formArray.length < this.leftpaneldata.length) {
                this.formArray.push(this.initialItemRow());
            }
            this.leftpaneldata.forEach((ele, ind) => {
                this.formArray.at(ind).get('form_id').setValue(ele.id);
                this.formArray.at(ind).get('form_name').setValue(ele.form_name);
                this.formArray.at(ind).get('is_parent').setValue(ele.is_parent);
                this.formArray.at(ind).get('is_child').setValue(ele.is_child);
                this.formArray.at(ind).get('is_sub_child').setValue(ele.is_sub_child);
                this.formArray.at(ind).get('parent_code').setValue(ele.parent_code);
                this.formArray.at(ind).get('child_code').setValue(ele.child_code);
                this.formArray.at(ind).get('sub_child_code').setValue(ele.sub_child_code);
            });
            if (this.rowData.length > 0) {
                this.viewRecord(this.rowData, this.leftpaneldata);
            }
        });
        this.commonsetupservice.getAllTenant().subscribe((data) => {
            this.tenantData = data;
        });
    }
    initialItemRow() {
        return this.formBuilder.group({
            id: '',
            tenant_id: '',
            role_id: '',
            user_id: '',
            form_id: '',
            read_access: false,
            write_access: false,
            delete_access: false,
            is_active: true,
            is_deleted: false,
            check: false,
            form_name: '',
            is_parent: false,
            is_child: false,
            is_sub_child: false,
            parent_code: '',
            child_code: '',
            sub_child_code: ''
        });
    }
    get formArray() {
        return this.pagesToRoleForm.get("initialItemRow");
    }
    tenantSelected(value) {
        this.masterService.getTenantWiseData(value).subscribe((data) => {
            this.roleData = data;
        });
    }
    roleSelected(value) {
        this.commonsetupservice.getUserData(this.pagesToRoleForm.value.tenant_id_id, value).subscribe((data) => {
            this.userData = data;
        });
    }
    onCheckboxChange(leftpanel, event, ind) {
        let fid = this.formArray.value.filter(x => x.parent_code == leftpanel.parent_code);
        const role_id_id = this.pagesToRoleForm.value.role_id_id;
        const tenant_id_id = this.pagesToRoleForm.value.tenant_id_id;
        const user_id_id = this.pagesToRoleForm.value.user_id_id;
        if (event.target.checked == true && tenant_id_id == '') {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default().fire("Please select tenant first");
            event.target.checked = false;
        }
        else if (event.target.checked == true && role_id_id == '') {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default().fire("Please select role first");
            event.target.checked = false;
        }
        else if (event.target.checked == true && user_id_id == '') {
            sweetalert2__WEBPACK_IMPORTED_MODULE_5___default().fire("Please select user first");
            event.target.checked = false;
        }
        else if (event.target.checked == true && tenant_id_id != '' && role_id_id != '' && user_id_id != '') {
            if (leftpanel.is_parent == true) {
                for (let i = 0; i < fid.length; i++) {
                    if (leftpanel.parent_code == fid[i].parent_code) {
                        this.formArray.value.forEach((element, index) => {
                            if (element.form_id == fid[i].form_id) {
                                this.formArray.at(index).get('check').setValue(true);
                            }
                        });
                    }
                    else {
                        this.formArray.at(ind).get('check').setValue(true);
                    }
                }
            }
            else {
                this.formArray.at(ind).get('check').setValue(true);
            }
        }
        else if (event.target.checked == false && leftpanel.is_parent == true) {
            for (let i = 0; i < fid.length; i++) {
                if (leftpanel.parent_code == fid[i].parent_code) {
                    this.formArray.value.forEach((element, index) => {
                        if (element.form_id == fid[i].form_id) {
                            this.formArray.at(index).get('check').setValue(false);
                        }
                    });
                }
                else {
                    this.formArray.at(ind).get('check').setValue(false);
                }
            }
        }
        else if (event.target.checked == false && leftpanel.is_parent == false) {
            this.formArray.at(ind).get('check').setValue(false);
        }
    }
    read(leftpanel, event, ind) {
        let fid = this.formArray.value.filter(x => x.parent_code == leftpanel.parent_code);
        if (event.checked == true) {
            this.formArray.value.forEach(element => {
                if (element.form_id == leftpanel.form_id && element.check == false) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default().fire("Please select checkbox first");
                }
            });
        }
        else if (event.checked == true && leftpanel.is_parent == true) {
            for (let i = 0; i < fid.length; i++) {
                if (leftpanel.parent_code == fid[i].parent_code) {
                    this.formArray.value.forEach((element, index) => {
                        if (element.form_id == fid[i].form_id) {
                            this.formArray.at(index).get('read_access').setValue(true);
                        }
                    });
                }
            }
        }
        else if (event.checked == true && leftpanel.is_child == true) {
            this.formArray.at(ind).get('read_access').setValue(true);
        }
        else if (event.checked == false && leftpanel.is_parent == true) {
            for (let i = 0; i < fid.length; i++) {
                if (leftpanel.parent_code == fid[i].parent_code) {
                    this.formArray.value.forEach((element, index) => {
                        if (element.form_id == fid[i].form_id) {
                            this.formArray.at(index).get('read_access').setValue(false);
                        }
                    });
                }
            }
        }
        else if (event.checked == false && leftpanel.is_child == true) {
            this.formArray.at(ind).get('read_access').setValue(false);
        }
    }
    write(leftpanel, event, ind) {
        let fid = this.formArray.value.filter(x => x.parent_code == leftpanel.parent_code);
        if (event.checked == true) {
            this.formArray.value.forEach(element => {
                if (element.form_id == leftpanel.form_id && element.check == false) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default().fire("Please select checkbox first");
                }
            });
        }
        else if (event.checked == true && leftpanel.is_parent == true) {
            for (let i = 0; i < fid.length; i++) {
                if (leftpanel.parent_code == fid[i].parent_code) {
                    this.formArray.value.forEach((element, index) => {
                        if (element.form_id == fid[i].form_id) {
                            this.formArray.at(index).get('read_access').setValue(true);
                            this.formArray.at(index).get('write_access').setValue(true);
                        }
                    });
                }
            }
        }
        else if (event.checked == true && leftpanel.is_child == true) {
            this.formArray.at(ind).get('read_access').setValue(true);
            this.formArray.at(ind).get('write_access').setValue(true);
        }
        else if (event.checked == false && leftpanel.is_parent == true) {
            for (let i = 0; i < fid.length; i++) {
                if (leftpanel.parent_code == fid[i].parent_code) {
                    this.formArray.value.forEach((element, index) => {
                        if (element.form_id == fid[i].form_id) {
                            this.formArray.at(index).get('read_access').setValue(false);
                            this.formArray.at(index).get('write_access').setValue(false);
                        }
                    });
                }
            }
        }
        else if (event.checked == false && leftpanel.is_child == true) {
            this.formArray.at(ind).get('read_access').setValue(false);
            this.formArray.at(ind).get('write_access').setValue(false);
        }
    }
    delete(leftpanel, event, ind) {
        let fid = this.formArray.value.filter(x => x.parent_code == leftpanel.parent_code);
        if (event.checked == true) {
            this.formArray.value.forEach(element => {
                if (element.form_id == leftpanel.form_id && element.check == false) {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_5___default().fire("Please select checkbox first");
                }
            });
        }
        if (event.checked == true && leftpanel.is_parent == true) {
            for (let i = 0; i < fid.length; i++) {
                if (leftpanel.parent_code == fid[i].parent_code) {
                    this.formArray.value.forEach((element, index) => {
                        if (element.form_id == fid[i].form_id) {
                            this.formArray.at(index).get('read_access').setValue(true);
                            this.formArray.at(index).get('write_access').setValue(true);
                            this.formArray.at(index).get('delete_access').setValue(true);
                        }
                    });
                }
            }
        }
        if (event.checked == true && leftpanel.is_child == true) {
            this.formArray.at(ind).get('read_access').setValue(true);
            this.formArray.at(ind).get('write_access').setValue(true);
            this.formArray.at(ind).get('delete_access').setValue(true);
        }
        if (event.checked == false && leftpanel.is_parent == true) {
            for (let i = 0; i < fid.length; i++) {
                if (leftpanel.parent_code == fid[i].parent_code) {
                    this.formArray.value.forEach((element, index) => {
                        if (element.form_id == fid[i].form_id) {
                            this.formArray.at(index).get('read_access').setValue(false);
                            this.formArray.at(index).get('write_access').setValue(false);
                            this.formArray.at(index).get('delete_access').setValue(false);
                        }
                    });
                }
            }
        }
        else if (event.checked == false && leftpanel.is_child == true) {
            this.formArray.at(ind).get('read_access').setValue(false);
            this.formArray.at(ind).get('write_access').setValue(false);
            this.formArray.at(ind).get('delete_access').setValue(false);
        }
    }
    viewRecord(assignscreen, leftpaneldata) {
        this.shouldDisable = true;
        this.pagesToRoleForm.patchValue({
            tenant_id_id: assignscreen[0].tenant_id_id,
            role_id_id: assignscreen[0].role_id_id,
            user_id_id: assignscreen[0].user_id_id,
            is_active: assignscreen[0].is_active
        });
        this.tenantSelected(assignscreen[0].tenant_id_id);
        this.roleSelected(assignscreen[0].role_id_id);
        this.pagesToRoleForm.setControl("initialItemRow", this.setExistingArray(assignscreen, leftpaneldata));
        if (this.submitBtn === true)
            this.BTN_VAL = 'Update';
    }
    setExistingArray(initialArray = [], leftpaneldata) {
        const formArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormArray([]);
        let ind = 0;
        initialArray.forEach(element => {
            let lpdata = leftpaneldata.filter(x => x.id === element.form_id_id);
            if (lpdata.length > 0) {
                formArray.push(this.formBuilder.group({
                    id: element.id,
                    tenant_id: element.tenant_id_id,
                    role_id: element.role_id_id,
                    user_id: element.user_id_id,
                    form_id: element.form_id_id,
                    read_access: element.read_access,
                    write_access: element.write_access,
                    delete_access: element.delete_access,
                    is_active: element.is_active,
                    is_deleted: element.is_deleted,
                    check: false,
                    form_name: lpdata[0].form_name,
                    is_parent: lpdata[0].is_parent,
                    is_child: lpdata[0].is_child,
                    is_sub_child: lpdata[0].is_sub_child,
                    parent_code: lpdata[0].parent_code,
                    child_code: lpdata[0].child_code,
                    sub_child_code: lpdata[0].sub_child_code
                }));
                if (element.read_access == true) {
                    formArray.at(ind).get('check').setValue(true);
                }
                ind = ind + 1;
            }
            else if (ind != 0) {
                ind = ind - 1;
            }
        });
        return formArray;
    }
    changeState(event) {
        if (!event.checked) {
            this.formArray.value.forEach(element => {
                element.is_active = false;
                element.is_deleted = true;
            });
        }
        else {
            this.formArray.value.forEach(element => {
                element.is_active = true;
                element.is_deleted = false;
            });
        }
    }
    onCancelForm() {
        this.cancelFlag = false;
        this.pagesToRoleForm.reset();
        this.onCancel.emit(false);
    }
    onSubmit() {
        this.formArray.value.forEach(ele => {
            ele.tenant_id = this.pagesToRoleForm.value.tenant_id_id;
            ele.role_id = this.pagesToRoleForm.value.role_id_id;
            ele.user_id = this.pagesToRoleForm.value.user_id_id;
        });
        if (this.pagesToRoleForm.invalid) {
            return;
        }
        else {
            if (this.shouldDisable == true) {
                this.shouldDisable = false;
            }
            this.onSave.emit({ form: this.pagesToRoleForm.value.initialItemRow, submitType: this.BTN_VAL });
        }
    }
};
PagesRoleLinkDetailsComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_7__.FormBuilder },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__.ManageSecurityService },
    { type: src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__.CommonSetupService },
    { type: src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_4__.MasterService }
];
PagesRoleLinkDetailsComponent.propDecorators = {
    onSave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Output }],
    onCancel: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Output }],
    rowData: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Input }],
    assignData: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Input }],
    submitBtn: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Input }],
    showLoader: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.Input }]
};
PagesRoleLinkDetailsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-pages-role-link-details',
        template: _pages_role_link_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_pages_role_link_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], PagesRoleLinkDetailsComponent);



/***/ }),

/***/ 5601:
/*!***************************************************************************!*\
  !*** ./src/app/common-setup/pages-role-link/pages-role-link.component.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "PagesRoleLinkComponent": () => (/* binding */ PagesRoleLinkComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _pages_role_link_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./pages-role-link.component.html?ngResource */ 9848);
/* harmony import */ var _pages_role_link_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./pages-role-link.component.sass?ngResource */ 6120);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/sort */ 4316);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);









let PagesRoleLinkComponent = class PagesRoleLinkComponent {
    constructor(manageSecurity) {
        this.manageSecurity = manageSecurity;
        this.screenName = 'Pages Role Link';
        this.BTN_VAL = 'Submit';
        this.submitBtn = true;
        this.listDiv = false;
        this.showList = true;
        this.rowData = [];
        this.displayColumns = [
            "actions",
            "tenant_name",
            "role_name",
            "username"
        ];
    }
    ngOnInit() {
        this.getAllPagesRole();
        this.USERID = localStorage.getItem('user_id');
        this.manageSecurity.getAccessLeftPanel(this.USERID, 'Pages Role Define').subscribe(data => {
            this.sidebarData = data;
        });
    }
    getAllPagesRole() {
        this.manageSecurity.getPagesRole().subscribe((data) => {
            this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_4__.MatTableDataSource(data);
            this.dataSource.paginator = this.dataSourcePaginator;
            this.dataSource.sort = this.sort;
        });
    }
    showSwalMassage(massage, icon) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_3___default().fire({
            title: massage,
            icon: icon,
            timer: 2000,
            showConfirmButton: false,
        });
    }
    viewRecord(row) {
        this.manageSecurity.getByTenantRoleUser(row.tenant_id_id, row.role_id_id, row.user_id_id).subscribe((data) => {
            this.rowData = data;
            this.showList = false;
            this.submitBtn = false;
            this.listDiv = true;
        });
    }
    editRecord(row) {
        this.manageSecurity.getByTenantRoleUser(row.tenant_id_id, row.role_id_id, row.user_id_id).subscribe((data) => {
            this.rowData = data;
            this.showList = false;
            this.listDiv = true;
            this.submitBtn = true;
            this.BTN_VAL = "Update";
        });
    }
    showFormList(item) {
        this.rowData = [];
        this.BTN_VAL = 'Submit';
        if (item === false) {
            this.listDiv = true;
            this.showList = false;
            this.submitBtn = true;
        }
        else {
            this.listDiv = false;
            this.showList = true;
        }
    }
    onCancel(item) {
        this.listDiv = item;
        this.showList = true;
        this.rowData = [];
        this.BTN_VAL = 'Submit';
    }
    tbl_FilterDatatable(value) {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    }
    onSave(formvalue) {
        this.manageSecurity.createPagesRole(formvalue.form, formvalue.submitType).subscribe((data) => {
            if (data["status"] == 1) {
                this.showSwalMassage("Your record has been updated successfully!", "success");
            }
            if (data["status"] == 2) {
                this.showSwalMassage("Your record has been added successfully!", "success");
            }
            this.showList = true;
            this.listDiv = false;
            this.rowData = [];
            this.BTN_VAL = "Submit";
            this.getAllPagesRole();
        }, (error) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default().fire(error);
        });
    }
};
PagesRoleLinkComponent.ctorParameters = () => [
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_2__.ManageSecurityService }
];
PagesRoleLinkComponent.propDecorators = {
    dataSourcePaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild, args: ["dataSourcePaginator", { read: _angular_material_paginator__WEBPACK_IMPORTED_MODULE_6__.MatPaginator },] }],
    sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.ViewChild, args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_7__.MatSort, { static: true },] }]
};
PagesRoleLinkComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-pages-role-link',
        template: _pages_role_link_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_pages_role_link_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], PagesRoleLinkComponent);



/***/ }),

/***/ 3083:
/*!********************************************************************************************************************!*\
  !*** ./src/app/common-setup/role-tenant-define/role-tenant-define-details/role-tenant-define-details.component.ts ***!
  \********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RoleTenantDefineDetailsComponent": () => (/* binding */ RoleTenantDefineDetailsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _role_tenant_define_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./role-tenant-define-details.component.html?ngResource */ 2427);
/* harmony import */ var _role_tenant_define_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./role-tenant-define-details.component.sass?ngResource */ 7032);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/common-setup.service */ 6491);
/* harmony import */ var src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/services/master.service */ 6754);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);








let RoleTenantDefineDetailsComponent = class RoleTenantDefineDetailsComponent {
    constructor(commonSetupService, masterService, formBuilder) {
        this.commonSetupService = commonSetupService;
        this.masterService = masterService;
        this.formBuilder = formBuilder;
        this.onSave = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
        this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
        this.BTN_VAL = 'Submit';
        this.masterData = [];
        this.tenantData = [];
    }
    ngOnInit() {
        this.roleForm = this.formBuilder.group({
            tenant_id: [""],
            initialItemRow1: this.formBuilder.array([this.initialitemRow()]),
        });
        this.commonSetupService.getAllMasterData('Role Type').subscribe((data) => {
            this.masterData = data;
        });
        this.commonSetupService.getAllTenant().subscribe((data) => {
            this.tenantData = data;
        });
        if (this.rowData.length > 0) {
            this.viewRecord(this.rowData);
        }
    }
    initialitemRow() {
        return this.formBuilder.group({
            id: [],
            tenant_id: [],
            role_name: [],
            role_type: [],
            is_active: [true],
            is_deleted: [false]
        });
    }
    get formArray() {
        return this.roleForm.get("initialItemRow1");
    }
    addNewRow() {
        this.formArray.push(this.initialitemRow());
    }
    deleteRow(index) {
        if (this.formArray.length == 1) {
            return false;
        }
        else {
            if (this.formArray.at(index).get('id').value != null) {
                this.masterService.getRoleExists(this.formArray.at(index).get('id').value).subscribe(data => {
                    if (data) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
                            title: "Role already linked!",
                            icon: "warning",
                            showConfirmButton: false,
                            timer: 20000
                        });
                    }
                    else {
                        this.formArray.removeAt(index);
                        return true;
                    }
                });
            }
            else {
                this.formArray.removeAt(index);
                return true;
            }
        }
    }
    tenantChange(event) {
        this.masterService.getTenantWiseData(event).subscribe((data) => {
            if (data.length > 0) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
                    title: "Tenant already exists!",
                    icon: "warning",
                    showConfirmButton: false,
                    timer: 2000
                });
                this.roleForm.get('tenant_id').setValue('');
            }
        });
    }
    viewRecord(row) {
        if (this.submitBtn === true) {
            this.BTN_VAL = "Update";
            this.roleForm.controls['tenant_id'].disable();
        }
        this.roleForm.patchValue({
            tenant_id: row[0].tenant_id_id,
        });
        this.roleForm.setControl("initialItemRow1", this.setExistingArray(row));
    }
    setExistingArray(initialArray = []) {
        const formArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormArray([]);
        initialArray.forEach((element, ind) => {
            formArray.push(this.formBuilder.group({
                id: element.id,
                tenant_id: element.tenant_id_id,
                role_name: element.role_name,
                role_type: element.role_type_id,
                is_active: element.is_active,
                is_deleted: element.is_deleted
            }));
        });
        return formArray;
    }
    onCancelForm() {
        this.roleForm.reset();
        this.onCancel.emit(false);
    }
    onSubmit() {
        this.roleForm.controls['tenant_id'].enable();
        this.roleForm.value.initialItemRow1.forEach(ele => {
            ele.tenant_id = this.roleForm.value.tenant_id;
        });
        if (this.roleForm.invalid) {
            return;
        }
        else {
            this.onSave.emit({ form: this.roleForm.value.initialItemRow1, submitType: this.BTN_VAL });
        }
    }
};
RoleTenantDefineDetailsComponent.ctorParameters = () => [
    { type: src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__.CommonSetupService },
    { type: src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_3__.MasterService },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder }
];
RoleTenantDefineDetailsComponent.propDecorators = {
    rowData: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input }],
    submitBtn: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input }],
    onSave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output }],
    onCancel: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output }]
};
RoleTenantDefineDetailsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-role-tenant-define-details',
        template: _role_tenant_define_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_role_tenant_define_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], RoleTenantDefineDetailsComponent);



/***/ }),

/***/ 1549:
/*!*********************************************************************************!*\
  !*** ./src/app/common-setup/role-tenant-define/role-tenant-define.component.ts ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "RoleTenantDefineComponent": () => (/* binding */ RoleTenantDefineComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _role_tenant_define_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./role-tenant-define.component.html?ngResource */ 2426);
/* harmony import */ var _role_tenant_define_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./role-tenant-define.component.sass?ngResource */ 3270);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/sort */ 4316);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);
/* harmony import */ var src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/services/master.service */ 6754);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);










let RoleTenantDefineComponent = class RoleTenantDefineComponent {
    constructor(masterService, manageSecurity) {
        this.masterService = masterService;
        this.manageSecurity = manageSecurity;
        this.screenName = 'Role Tenant Define';
        this.BTN_VAL = 'Submit';
        this.submitBtn = true;
        this.listDiv = false;
        this.showList = true;
        this.rowData = [];
        this.displayColumns = [
            "actions",
            "tenant_name"
        ];
    }
    ngOnInit() {
        this.getAllRoleTenant();
        this.USERID = localStorage.getItem('user_id');
        this.manageSecurity.getAccessLeftPanel(this.USERID, 'Role Tenant Define').subscribe(data => {
            this.sidebarData = data;
        });
    }
    getAllRoleTenant() {
        this.masterService.getAllRole().subscribe((data) => {
            this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_5__.MatTableDataSource(data);
            this.dataSource.paginator = this.dataSourcePaginator;
            this.dataSource.sort = this.sort;
        });
    }
    showSwalMassage(massage, icon) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
            title: massage,
            icon: icon,
            timer: 2000,
            showConfirmButton: false,
        });
    }
    viewRecord(row) {
        this.masterService.getTenantWiseData(row.tenant_id_id).subscribe((data) => {
            this.rowData = data;
            this.showList = false;
            this.submitBtn = false;
            this.listDiv = true;
        });
    }
    editRecord(row) {
        this.masterService.getTenantWiseData(row.tenant_id_id).subscribe((data) => {
            this.rowData = data;
            this.showList = false;
            this.listDiv = true;
            this.submitBtn = true;
            this.BTN_VAL = "Update";
        });
    }
    showFormList(item) {
        this.rowData = [];
        this.BTN_VAL = 'Submit';
        if (item === false) {
            this.listDiv = true;
            this.showList = false;
            this.submitBtn = true;
        }
        else {
            this.listDiv = false;
            this.showList = true;
        }
    }
    onCancel(item) {
        this.listDiv = item;
        this.showList = true;
        this.rowData = [];
        this.BTN_VAL = 'Submit';
    }
    tbl_FilterDatatable(value) {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    }
    onSave(formvalue) {
        this.masterService.createRole(formvalue.form, formvalue.submitType).subscribe((data) => {
            if (data["status"] == 1) {
                this.showSwalMassage("Your record has been updated successfully!", "success");
            }
            if (data["status"] == 2) {
                this.showSwalMassage("Your record has been added successfully!", "success");
            }
            this.showList = true;
            this.listDiv = false;
            this.rowData = [];
            this.BTN_VAL = "Submit";
            this.getAllRoleTenant();
        }, (error) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire(error);
        });
    }
};
RoleTenantDefineComponent.ctorParameters = () => [
    { type: src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_3__.MasterService },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_2__.ManageSecurityService }
];
RoleTenantDefineComponent.propDecorators = {
    dataSourcePaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChild, args: ["dataSourcePaginator", { read: _angular_material_paginator__WEBPACK_IMPORTED_MODULE_7__.MatPaginator },] }],
    sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChild, args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_8__.MatSort, { static: true },] }]
};
RoleTenantDefineComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-role-tenant-define',
        template: _role_tenant_define_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_role_tenant_define_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], RoleTenantDefineComponent);



/***/ }),

/***/ 5560:
/*!*******************************************************************************************!*\
  !*** ./src/app/common-setup/tenant-hierarchy-define/tenant-hierarchy-define.component.ts ***!
  \*******************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TenantHierarchyDefineComponent": () => (/* binding */ TenantHierarchyDefineComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _tenant_hierarchy_define_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tenant-hierarchy-define.component.html?ngResource */ 5144);
/* harmony import */ var _tenant_hierarchy_define_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tenant-hierarchy-define.component.sass?ngResource */ 4386);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/common-setup.service */ 6491);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);








let TenantHierarchyDefineComponent = class TenantHierarchyDefineComponent {
    constructor(formBuilder, commonsetupservice, manageSecurity) {
        this.formBuilder = formBuilder;
        this.commonsetupservice = commonsetupservice;
        this.manageSecurity = manageSecurity;
        this.screenName = "Tenant Hierarchy Define";
        this.BTN_VAL = "Submit";
        this.showupdate = false;
        this.tenantHierarchyData = [];
        this.sidebarData = [];
    }
    ngOnInit() {
        let user_id = localStorage.getItem('user_id');
        this.tenanthirarchydefineForm = this.formBuilder.group({
            is_permanent: [false],
            initialItemRow1: this.formBuilder.array([this.initialitemRow()]),
        });
        this.commonsetupservice.getAllMasterData("Tenant Type").subscribe((data) => {
            this.tenantHierarchyData = data;
        });
        this.commonsetupservice.getAllTenantHierarchyDefine().subscribe((data) => {
            if (data.length > 0) {
                this.viewRecord(data[0]);
                this.tenanthirarchydefineForm.setControl("initialItemRow1", this.setExistingArray(data));
            }
        });
        this.manageSecurity.getAccessLeftPanel(user_id, 'Tenant Hierarchy Define').subscribe(data => {
            this.sidebarData = data;
        });
        this.showupdate = true;
    }
    initialitemRow() {
        return this.formBuilder.group({
            id: [],
            tenant_type: [],
            sequence_no: []
        });
    }
    get formArray() {
        return this.tenanthirarchydefineForm.get("initialItemRow1");
    }
    addNewRow() {
        this.formArray.push(this.initialitemRow());
    }
    deleteRow(index) {
        if (this.formArray.length == 1) {
            return false;
        }
        else {
            this.formArray.removeAt(index);
            return true;
        }
    }
    viewRecord(row) {
        this.tenanthirarchydefineForm.patchValue({
            is_permanent: row.is_permanent
        });
        if (this.showupdate && !row.is_permanent) {
            this.BTN_VAL = "Update";
        }
        if (!row.is_permanent) {
            this.showupdate = true;
        }
        else {
            this.showupdate = false;
        }
    }
    getSequence(i) {
        if (i == 0) {
            let tenant_type = this.formArray.at(i).get('tenant_type').value;
            let is_apex = this.tenantHierarchyData.filter(x => x.id === tenant_type && x.master_value === 'Apex Bank');
            if (is_apex.length == 0) {
                this.showSwalMassage("Top most level should be Apex Bank", "error");
                this.formArray.at(i).get('tenant_type').setValue('');
            }
        }
        let tenant_type = this.formArray.at(i).get('tenant_type').value;
        let is_repeate = this.formArray.value.filter(x => x.tenant_type === tenant_type);
        if (is_repeate.length > 1) {
            this.showSwalMassage("Tenant type already exists", "warning");
            this.formArray.at(i).get('tenant_type').setValue('');
        }
        this.formArray.at(i).get('sequence_no').setValue(i + 1);
    }
    setExistingArray(initialArray = []) {
        const formArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormArray([]);
        initialArray.forEach((element) => {
            formArray.push(this.formBuilder.group({
                id: element.id,
                tenant_type: element.tenant_type,
                sequence_no: element.sequence_no
            }));
        });
        return formArray;
    }
    onCancelForm() {
        this.tenanthirarchydefineForm.reset();
        window.location.reload();
    }
    onSubmit() {
        if (this.tenanthirarchydefineForm.invalid) {
            return;
        }
        else {
            var formValue = this.tenanthirarchydefineForm.value;
            for (let i = 0; i < formValue['initialItemRow1'].length; i++) {
                formValue['initialItemRow1'][i]['is_permanent'] = formValue['is_permanent'];
            }
            this.commonsetupservice.createTenantHierarchyDefine(formValue['initialItemRow1']).subscribe((data) => {
                if (data["status"] == 1) {
                    this.showSwalMassage("Your record has been updated successfully!", "success");
                }
                else if (data["status"] == 2) {
                    this.showSwalMassage("Your record has been added successfully!", "success");
                }
                window.location.reload();
                this.BTN_VAL = "Submit";
            }, (error) => {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire(error);
            });
        }
    }
    showSwalMassage(massage, icon) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
            title: massage,
            icon: icon,
            timer: 2000,
            showConfirmButton: false,
        });
    }
};
TenantHierarchyDefineComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormBuilder },
    { type: src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__.CommonSetupService },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__.ManageSecurityService }
];
TenantHierarchyDefineComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-tenant-hierarchy-define',
        template: _tenant_hierarchy_define_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_tenant_hierarchy_define_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], TenantHierarchyDefineComponent);



/***/ }),

/***/ 6222:
/*!********************************************************************************************************!*\
  !*** ./src/app/common-setup/tenant-onboard/tenant-onboard-details/tenant-onboard-details.component.ts ***!
  \********************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TenantOnboardDetailsComponent": () => (/* binding */ TenantOnboardDetailsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _tenant_onboard_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tenant-onboard-details.component.html?ngResource */ 930);
/* harmony import */ var _tenant_onboard_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tenant-onboard-details.component.sass?ngResource */ 48);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/common-setup.service */ 6491);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);







let TenantOnboardDetailsComponent = class TenantOnboardDetailsComponent {
    constructor(formBuilder, commonsetupservice) {
        this.formBuilder = formBuilder;
        this.commonsetupservice = commonsetupservice;
        this.onSave = new _angular_core__WEBPACK_IMPORTED_MODULE_4__.EventEmitter();
        this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_4__.EventEmitter();
        this.BTN_VAL = "Submit";
        this.tenantHierarchyData = [];
        this.countrydata = [];
        this.statedata = [];
        this.cityData = [];
        this.tenantData = [];
        this.BankTenantData = [];
        this.editdisabled = false;
    }
    ngOnInit() {
        this.CREATED_BY = localStorage.getItem("USER_ID");
        this.tenantonboardForm = this.formBuilder.group({
            id: [],
            tenant_name: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.maxLength(30)]],
            tenant_shortname: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.maxLength(30), _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.pattern("^[a-zA-Z0-9_ ]*$")]],
            is_legal_tenant: [false],
            tenant_code: [""],
            tenant_type_ref_id: [""],
            tenant_belongs_to: [],
            address1: [""],
            address2: [""],
            bank_tenant_ref_id: [],
            country_ref_id: [""],
            state_ref_id: [""],
            city_ref_id: [""],
            pincode: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.maxLength(6), _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.pattern("^[0-9_]*$")]],
            is_active: true,
            is_deleted: false
        });
        this.commonsetupservice.getAllTenantHierarchyDefine().subscribe((data) => {
            this.tenantHierarchyData = data;
        });
        this.commonsetupservice.getCurrencyData().subscribe((data) => {
            this.countrydata = data;
        });
        if (!Array.isArray(this.rowData)) {
            this.viewRecord(this.rowData);
        }
    }
    viewRecord(row) {
        if (row.tenant_type_ref_id != 1) {
            this.commonsetupservice.getTenantNameByType(row.tenant_type_ref_id).subscribe((data) => {
                this.tenantData = data;
            });
            this.commonsetupservice.getTenantNameByBankTenantRefId(row.tenant_type_ref_id).subscribe((data) => {
                this.BankTenantData = data;
            });
        }
        this.countrySelected(row.country_ref_id);
        this.stateSelected(row.state_ref_id);
        this.tenantonboardForm.patchValue({
            tenant_name: row.tenant_name,
            tenant_shortname: row.tenant_shortname,
            is_legal_tenant: row.is_legal_tenant,
            tenant_code: row.tenant_code,
            tenant_type_ref_id: row.tenant_type_ref_id,
            tenant_belongs_to: row.tenant_belongs_to,
            address1: row.address1,
            address2: row.address2,
            bank_tenant_ref_id: row.bank_tenant_ref_id,
            country_ref_id: row.country_ref_id,
            id: row.id,
            state_ref_id: row.state_ref_id,
            city_ref_id: row.city_ref_id,
            pincode: row.pincode,
            is_active: row.is_active,
            is_deleted: row.is_deleted
        });
        if (this.submitBtn === true) {
            this.BTN_VAL = "Update";
            this.editdisabled = true;
        }
    }
    onCancelForm() {
        this.tenantonboardForm.reset();
        this.onCancel.emit(false);
    }
    onSubmit() {
        if (this.tenantonboardForm.invalid) {
            return;
        }
        else {
            this.onSave.emit(this.tenantonboardForm.value);
        }
    }
    countrySelected(val) {
        this.commonsetupservice.getStatesData(val).subscribe((data) => {
            this.statedata = data;
        });
    }
    stateSelected(val) {
        this.commonsetupservice.getCitiesData(val).subscribe((data) => {
            this.cityData = data;
        });
    }
    tenant_type_chnage(val) {
        let tenant = this.tenantHierarchyData.filter(x => x.id === val);
        if (this.tenantonboardForm.controls['tenant_belongs_to'].disabled) {
            this.tenantonboardForm.controls['tenant_belongs_to'].enable();
        }
        if (this.tenantonboardForm.controls['bank_tenant_ref_id'].disabled) {
            this.tenantonboardForm.controls['bank_tenant_ref_id'].enable();
        }
        if (tenant[0]['tenant_type_name'] != 'Apex Bank') {
            this.tenantonboardForm.get('tenant_belongs_to').setValue('');
            this.tenantonboardForm.get('bank_tenant_ref_id').setValue('');
            this.commonsetupservice.getTenantNameByType(val).subscribe((data) => {
                this.tenantData = data;
            });
            if (tenant[0]['tenant_type_name'] == 'Member Bank') {
                this.tenantonboardForm.controls['bank_tenant_ref_id'].disable();
            }
            else {
                this.commonsetupservice.getTenantNameByBankTenantRefId(val).subscribe((data) => {
                    this.BankTenantData = data;
                });
            }
        }
        else {
            this.dataSource.forEach(ele => {
                if (ele['tenant_type'] == 'Apex Bank') {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default().fire({
                        title: "Apex Bank is already define, You can not define it again!",
                        icon: "warning",
                        timer: 2000,
                        showConfirmButton: false,
                    });
                    this.tenantonboardForm.get('tenant_type_ref_id').setValue('');
                }
            });
            this.tenantonboardForm.controls['tenant_belongs_to'].disable();
            this.tenantonboardForm.controls['bank_tenant_ref_id'].disable();
        }
    }
    changeState(event) {
        if (!event.checked) {
            this.tenantonboardForm.get('is_deleted').setValue(true);
        }
        else {
            this.tenantonboardForm.get('is_deleted').setValue(false);
        }
    }
};
TenantOnboardDetailsComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormBuilder },
    { type: src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__.CommonSetupService }
];
TenantOnboardDetailsComponent.propDecorators = {
    rowData: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input }],
    submitBtn: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input }],
    dataSource: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input }],
    onSave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Output }],
    onCancel: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Output }]
};
TenantOnboardDetailsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-tenant-onboard-details',
        template: _tenant_onboard_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_tenant_onboard_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], TenantOnboardDetailsComponent);



/***/ }),

/***/ 9623:
/*!*************************************************************************!*\
  !*** ./src/app/common-setup/tenant-onboard/tenant-onboard.component.ts ***!
  \*************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "TenantOnboardComponent": () => (/* binding */ TenantOnboardComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _tenant_onboard_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./tenant-onboard.component.html?ngResource */ 7002);
/* harmony import */ var _tenant_onboard_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./tenant-onboard.component.sass?ngResource */ 3369);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/sort */ 4316);
/* harmony import */ var src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/common-setup.service */ 6491);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! xlsx */ 4126);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);











let TenantOnboardComponent = class TenantOnboardComponent {
    constructor(commonsetupservice, manageSecurity) {
        this.commonsetupservice = commonsetupservice;
        this.manageSecurity = manageSecurity;
        this.displayColumns = [
            "actions",
            "tenant_name",
            "tenant_type",
            "address1",
            "pincode",
        ];
        this.screenName = "Tenant Onboard";
        this.BTN_VAL = "Submit";
        this.submitBtn = true;
        this.listDiv = false;
        this.showList = true;
        this.rowData = [];
    }
    ngOnInit() {
        this.getAllonboardTenant();
        this.USERID = localStorage.getItem('user_id');
        this.manageSecurity.getAccessLeftPanel(this.USERID, 'Tenant Hierarchy Define').subscribe(data => {
            this.sidebarData = data;
        });
    }
    getAllonboardTenant() {
        this.commonsetupservice.getAllTenant().subscribe((data) => {
            this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_5__.MatTableDataSource(data);
            this.dataSource.paginator = this.dataSourcePaginator;
            this.dataSource.sort = this.sort;
            this.commonsetupservice.getAllTenantHierarchyDefine().subscribe((data) => {
                data.forEach(ele => {
                    let tenant_type = this.dataSource.filteredData.filter(x => x.tenant_type_ref_id === ele['id']);
                    if (tenant_type.length > 0) {
                        tenant_type.forEach(e => {
                            if (e.id == this.dataSource.filteredData.filter(x => x.id == e.id)[0].id) {
                                this.dataSource.filteredData.filter(x => x.id == e.id)[0]['tenant_type'] = ele['tenant_type_name'];
                            }
                        });
                    }
                });
            });
        });
    }
    viewRecord(row) {
        this.rowData = row;
        this.showList = false;
        this.submitBtn = false;
        this.listDiv = true;
    }
    editRecord(row) {
        this.rowData = row;
        this.showList = false;
        this.listDiv = true;
        this.submitBtn = true;
        this.BTN_VAL = "Update";
    }
    showFormList(item) {
        this.rowData = [];
        this.BTN_VAL = 'Submit';
        if (item === false) {
            this.listDiv = true;
            this.showList = false;
            this.submitBtn = true;
        }
        else {
            this.listDiv = false;
            this.showList = true;
        }
    }
    onCancel(item) {
        this.listDiv = item;
        this.showList = true;
        this.rowData = [];
        this.BTN_VAL = 'Submit';
    }
    showSwalMassage(massage, icon) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
            title: massage,
            icon: icon,
            timer: 2000,
            showConfirmButton: false,
        });
    }
    onSave(formValue) {
        this.commonsetupservice.createTenant(formValue).subscribe((data) => {
            if (data["status"] == 1) {
                this.showSwalMassage("Your record has been updated successfully!", "success");
            }
            else if (data["status"] == 2) {
                this.showSwalMassage("Your record has been added successfully!", "success");
            }
            this.showList = true;
            this.listDiv = false;
            this.rowData = [];
            this.BTN_VAL = "Submit";
            this.getAllonboardTenant();
        }, (error) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire(error);
        });
    }
    tbl_FilterDatatable(value) {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    }
    export_to_excel() {
        var XlsMasterData = this.dataSource.data.map(({ tenant_name, tenant_type, address1, pincode }) => ({ tenant_name, tenant_type, address1, pincode }));
        const workSheet = xlsx__WEBPACK_IMPORTED_MODULE_6__.utils.json_to_sheet(XlsMasterData);
        const workBook = xlsx__WEBPACK_IMPORTED_MODULE_6__.utils.book_new();
        xlsx__WEBPACK_IMPORTED_MODULE_6__.utils.book_append_sheet(workBook, workSheet, 'SheetName');
        xlsx__WEBPACK_IMPORTED_MODULE_6__.writeFile(workBook, 'Tenant Onboard Data.xlsx');
    }
};
TenantOnboardComponent.ctorParameters = () => [
    { type: src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__.CommonSetupService },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__.ManageSecurityService }
];
TenantOnboardComponent.propDecorators = {
    dataSourcePaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChild, args: ["dataSourcePaginator", { read: _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__.MatPaginator },] }],
    sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChild, args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_9__.MatSort, { static: true },] }]
};
TenantOnboardComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-tenant-onboard',
        template: _tenant_onboard_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_tenant_onboard_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], TenantOnboardComponent);



/***/ }),

/***/ 5636:
/*!*****************************************************************************************************************!*\
  !*** ./src/app/common-setup/user-registration/user-registration-details/user-registration-details.component.ts ***!
  \*****************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserRegistrationDetailsComponent": () => (/* binding */ UserRegistrationDetailsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _user_registration_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user-registration-details.component.html?ngResource */ 4563);
/* harmony import */ var _user_registration_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user-registration-details.component.sass?ngResource */ 1846);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/master.service */ 6754);
/* harmony import */ var src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/common-setup.service */ 6491);







let UserRegistrationDetailsComponent = class UserRegistrationDetailsComponent {
    constructor(formBuilder, masterservice, commonsetupservice) {
        this.formBuilder = formBuilder;
        this.masterservice = masterservice;
        this.commonsetupservice = commonsetupservice;
        this.onSave = new _angular_core__WEBPACK_IMPORTED_MODULE_4__.EventEmitter();
        this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_4__.EventEmitter();
        this.BTN_VAL = "Submit";
        this.screenName = "User Registration";
        this.TenantData = [];
        this.RoleNameData = [];
    }
    ngOnInit() {
        this.UserRegistrationForm = this.formBuilder.group({
            id: [],
            email: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.email]],
            mobile_number: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.pattern("^((\\+91-?)|0)?[0-9]{10}$")]],
            role_name: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required]],
            tenant_ref_id: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_5__.Validators.required]]
        });
        this.commonsetupservice.getAllTenant().subscribe((data) => {
            this.TenantData = data;
            if (!Array.isArray(this.rowData)) {
                this.viewRecord(this.rowData);
            }
        });
        this.masterservice.getAllRoleName().subscribe((data) => {
            this.RoleNameData = data;
        });
    }
    viewRecord(row) {
        let tenant = this.TenantData.filter(x => x.tenant_name === row.tenant_name);
        this.UserRegistrationForm.patchValue({
            email: row.email,
            role_name: row.role_name,
            mobile_number: row.mobile_number,
            tenant_ref_id: tenant[0].id,
            id: row.id,
        });
        if (this.submitBtn === true)
            this.BTN_VAL = "Update";
    }
    onCancelForm() {
        this.UserRegistrationForm.reset();
        this.onCancel.emit(false);
    }
    onSubmit() {
        if (this.UserRegistrationForm.invalid) {
            return;
        }
        else {
            this.onSave.emit(this.UserRegistrationForm.value);
        }
    }
};
UserRegistrationDetailsComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_5__.FormBuilder },
    { type: src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__.MasterService },
    { type: src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_3__.CommonSetupService }
];
UserRegistrationDetailsComponent.propDecorators = {
    rowData: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input }],
    submitBtn: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Input }],
    onSave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Output }],
    onCancel: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_4__.Output }]
};
UserRegistrationDetailsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-user-registration-details',
        template: _user_registration_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_user_registration_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], UserRegistrationDetailsComponent);



/***/ }),

/***/ 3459:
/*!*******************************************************************************!*\
  !*** ./src/app/common-setup/user-registration/user-registration.component.ts ***!
  \*******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "UserRegistrationComponent": () => (/* binding */ UserRegistrationComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _user_registration_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./user-registration.component.html?ngResource */ 7777);
/* harmony import */ var _user_registration_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./user-registration.component.sass?ngResource */ 2745);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/sort */ 4316);
/* harmony import */ var src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/common-setup.service */ 6491);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! xlsx */ 4126);











let UserRegistrationComponent = class UserRegistrationComponent {
    constructor(commonsetupservice, manageSecurity) {
        this.commonsetupservice = commonsetupservice;
        this.manageSecurity = manageSecurity;
        this.displayColumns = [
            "actions",
            "email",
            "mobile_number",
            "role_name",
            "tenant_name",
        ];
        this.screenName = "User Registration";
        this.BTN_VAL = "Submit";
        this.submitBtn = true;
        this.listDiv = false;
        this.showList = true;
        this.rowData = [];
    }
    ngOnInit() {
        this.getAllRegisterUser();
        this.USERID = localStorage.getItem('user_id');
        this.manageSecurity.getAccessLeftPanel(this.USERID, 'Tenant Hierarchy Define').subscribe(data => {
            this.sidebarData = data;
        });
    }
    getAllRegisterUser() {
        this.commonsetupservice.getAllRegisterUser().subscribe((data) => {
            this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_5__.MatTableDataSource(data);
            this.dataSource.paginator = this.dataSourcePaginator;
            this.dataSource.sort = this.sort;
        });
    }
    viewRecord(row) {
        this.rowData = row;
        this.showList = false;
        this.submitBtn = false;
        this.listDiv = true;
    }
    editRecord(row) {
        this.rowData = row;
        this.showList = false;
        this.listDiv = true;
        this.submitBtn = true;
        this.BTN_VAL = "Update";
    }
    showFormList(item) {
        this.rowData = [];
        this.BTN_VAL = 'Submit';
        if (item === false) {
            this.listDiv = true;
            this.showList = false;
            this.submitBtn = true;
        }
        else {
            this.listDiv = false;
            this.showList = true;
        }
    }
    onCancel(item) {
        this.listDiv = item;
        this.showList = true;
        this.rowData = [];
        this.BTN_VAL = 'Submit';
    }
    onSave(formValue) {
        var dict1 = {
            email: formValue['email'],
            user_data: [{ "user_id": formValue['id'], "mobile_number": formValue['mobile_number'], "role_name": formValue['role_name'], "tenant_ref_id": formValue['tenant_ref_id'] }]
        };
        this.commonsetupservice.createUserRegistration(dict1).subscribe((data) => {
            if (data["status"] == 1) {
                this.showSwalMassage("Your record has been updated successfully!", "success");
            }
            else if (data["status"] == 2) {
                this.showSwalMassage("Your record has been added successfully!", "success");
            }
            this.showList = true;
            this.listDiv = false;
            this.rowData = [];
            this.BTN_VAL = "Submit";
            this.getAllRegisterUser();
        }, (error) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire(error);
        });
    }
    showSwalMassage(massage, icon) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
            title: massage,
            icon: icon,
            timer: 2000,
            showConfirmButton: false,
        });
    }
    tbl_FilterDatatable(value) {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    }
    export_to_excel() {
        var XlsMasterData = this.dataSource.data.map(({ email, mobile_number, role_name, tenant_name }) => ({ email, mobile_number, role_name, tenant_name }));
        const workSheet = xlsx__WEBPACK_IMPORTED_MODULE_6__.utils.json_to_sheet(XlsMasterData);
        const workBook = xlsx__WEBPACK_IMPORTED_MODULE_6__.utils.book_new();
        xlsx__WEBPACK_IMPORTED_MODULE_6__.utils.book_append_sheet(workBook, workSheet, 'SheetName');
        xlsx__WEBPACK_IMPORTED_MODULE_6__.writeFile(workBook, 'User Registration Data.xlsx');
    }
};
UserRegistrationComponent.ctorParameters = () => [
    { type: src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__.CommonSetupService },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__.ManageSecurityService }
];
UserRegistrationComponent.propDecorators = {
    dataSourcePaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChild, args: ["dataSourcePaginator", { read: _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__.MatPaginator },] }],
    sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChild, args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_9__.MatSort, { static: true },] }]
};
UserRegistrationComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-user-registration',
        template: _user_registration_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_user_registration_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], UserRegistrationComponent);



/***/ }),

/***/ 1322:
/*!************************************************************************************************************************!*\
  !*** ./src/app/common-setup/pages-role-link/pages-role-link-details/pages-role-link-details.component.sass?ngResource ***!
  \************************************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYWdlcy1yb2xlLWxpbmstZGV0YWlscy5jb21wb25lbnQuc2FzcyJ9 */";

/***/ }),

/***/ 6120:
/*!****************************************************************************************!*\
  !*** ./src/app/common-setup/pages-role-link/pages-role-link.component.sass?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYWdlcy1yb2xlLWxpbmsuY29tcG9uZW50LnNhc3MifQ== */";

/***/ }),

/***/ 7032:
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/common-setup/role-tenant-define/role-tenant-define-details/role-tenant-define-details.component.sass?ngResource ***!
  \*********************************************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyb2xlLXRlbmFudC1kZWZpbmUtZGV0YWlscy5jb21wb25lbnQuc2FzcyJ9 */";

/***/ }),

/***/ 3270:
/*!**********************************************************************************************!*\
  !*** ./src/app/common-setup/role-tenant-define/role-tenant-define.component.sass?ngResource ***!
  \**********************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyb2xlLXRlbmFudC1kZWZpbmUuY29tcG9uZW50LnNhc3MifQ== */";

/***/ }),

/***/ 4386:
/*!********************************************************************************************************!*\
  !*** ./src/app/common-setup/tenant-hierarchy-define/tenant-hierarchy-define.component.sass?ngResource ***!
  \********************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0ZW5hbnQtaGllcmFyY2h5LWRlZmluZS5jb21wb25lbnQuc2FzcyJ9 */";

/***/ }),

/***/ 48:
/*!*********************************************************************************************************************!*\
  !*** ./src/app/common-setup/tenant-onboard/tenant-onboard-details/tenant-onboard-details.component.sass?ngResource ***!
  \*********************************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0ZW5hbnQtb25ib2FyZC1kZXRhaWxzLmNvbXBvbmVudC5zYXNzIn0= */";

/***/ }),

/***/ 3369:
/*!**************************************************************************************!*\
  !*** ./src/app/common-setup/tenant-onboard/tenant-onboard.component.sass?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ0ZW5hbnQtb25ib2FyZC5jb21wb25lbnQuc2FzcyJ9 */";

/***/ }),

/***/ 1846:
/*!******************************************************************************************************************************!*\
  !*** ./src/app/common-setup/user-registration/user-registration-details/user-registration-details.component.sass?ngResource ***!
  \******************************************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1c2VyLXJlZ2lzdHJhdGlvbi1kZXRhaWxzLmNvbXBvbmVudC5zYXNzIn0= */";

/***/ }),

/***/ 2745:
/*!********************************************************************************************!*\
  !*** ./src/app/common-setup/user-registration/user-registration.component.sass?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJ1c2VyLXJlZ2lzdHJhdGlvbi5jb21wb25lbnQuc2FzcyJ9 */";

/***/ }),

/***/ 9968:
/*!************************************************************************************************************************!*\
  !*** ./src/app/common-setup/pages-role-link/pages-role-link-details/pages-role-link-details.component.html?ngResource ***!
  \************************************************************************************************************************/
/***/ ((module) => {

module.exports = "<div id=\"new_entry_form\">\r\n    <div class=\"row clearfix\">\r\n        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n            <form [formGroup]=\"pagesToRoleForm\" (ngSubmit)=\"onSubmit()\">\r\n                <div class=\"card\">\r\n                    <div class=\"body\">\r\n                        <div class=\"row clearfix\">\r\n                            <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                <div class=\"form-group\">\r\n                                    <mat-label>Tenant <i style=\"color: red;\">*</i></mat-label>\r\n                                    <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                        <mat-select formControlName=\"tenant_id_id\" id=\"tenant_id_id\"\r\n                                            (selectionChange)='tenantSelected($event.value)' [disabled]=\"shouldDisable\"\r\n                                            placeholder=\"Select Tenant\" required>\r\n                                            <mat-option *ngFor=\"let data of tenantData\" [value]=\"data.id\">\r\n                                                {{data.tenant_name}}\r\n                                            </mat-option>\r\n                                        </mat-select>\r\n                                        <mat-error *ngIf=\"pagesToRoleForm.get('tenant_id_id')\">Please Select\r\n                                            Tenant</mat-error>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                <mat-label>Role <i style=\"color: red;\">*</i></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"role_id_id\" id=\"role_id_id\" [disabled]=\"shouldDisable\"\r\n                                        (selectionChange)='roleSelected($event.value)' placeholder=\"Select Role\"\r\n                                        required>\r\n                                        <mat-option *ngFor=\"let data of roleData\" [value]=\"data.id\">\r\n                                            {{data.role_name}}\r\n                                        </mat-option>\r\n                                    </mat-select>\r\n                                    <mat-error *ngIf=\"pagesToRoleForm.get('role_id_id')\">Please Select\r\n                                        Role</mat-error>\r\n                                </mat-form-field>\r\n                            </div>\r\n\r\n                            <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                <mat-label>User <i style=\"color: red;\">*</i></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"user_id_id\" id=\"user_id_id\" [disabled]=\"shouldDisable\"\r\n                                        placeholder=\"Select User\" required>\r\n                                        <mat-option *ngFor=\"let data of userData\" [value]=\"data.id\">\r\n                                            {{data.username}}\r\n                                        </mat-option>\r\n                                    </mat-select>\r\n                                    <mat-error *ngIf=\"pagesToRoleForm.get('user_id_id')\">Please Select\r\n                                        User</mat-error>\r\n                                </mat-form-field>\r\n                            </div>\r\n\r\n                            <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                <label class=\"form-label\">Is Active?</label>\r\n                                <mat-label style=\"margin-left: 1rem; margin-right: 0.5rem;\">No</mat-label>\r\n                                <mat-slide-toggle id=\"is_active\" (change)=\"changeState($event)\"\r\n                                    formControlName=\"is_active\">Yes</mat-slide-toggle>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"card\">\r\n                    <div class=\"body\">\r\n                        <div class=\"form-group\">\r\n                            <div class=\"table-responsive\" formArrayName=\"initialItemRow\">\r\n                                <table id=\"tableExport1\"\r\n                                    class=\"display table table-hover table-checkable order-column m-t-20 width-per-100\">\r\n\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th style=\"background-color: #CAD5E2;\"></th>\r\n                                            <th style=\"background-color: #CAD5E2;\">Form Name</th>\r\n                                            <th style=\"background-color: #CAD5E2;\">Read Access</th>\r\n                                            <th style=\"background-color: #CAD5E2;\">Write Access</th>\r\n                                            <th style=\"background-color: #CAD5E2;\">Delete Access</th>\r\n                                        </tr>\r\n                                    </thead>\r\n\r\n                                    <tbody class=\"main_tbody\">\r\n                                        <tr *ngFor=\"let dynamic of pagesToRoleForm.controls.initialItemRow['controls']; let i = index\"\r\n                                            [formGroupName]=\"i\">\r\n                                            <td\r\n                                                [style.background-color]=\"dynamic.value.is_parent === true ? '#90e0ef' : 'd0efff' && dynamic.value.is_child === true ? '#caf0f8' : 'd0efff'\">\r\n                                                <input type=\"checkbox\" id=\"check{{i+1}}\" formControlName=\"check\"\r\n                                                    value=\"{{dynamic.value.check}}\"\r\n                                                    (change)=\"onCheckboxChange(dynamic.value,$event,i)\">\r\n                                            </td>\r\n\r\n                                            <td\r\n                                                [style.background-color]=\"dynamic.value.is_parent === true ? '#90e0ef' : 'd0efff' && dynamic.value.is_child === true ? '#caf0f8' : 'd0efff'\">\r\n                                                <mat-label>{{dynamic.value.form_name}}</mat-label>\r\n                                            </td>\r\n\r\n                                            <td\r\n                                                [style.background-color]=\"dynamic.value.is_parent === true ? '#90e0ef' : 'd0efff' && dynamic.value.is_child === true ? '#caf0f8' : 'd0efff'\">\r\n                                                <mat-label style=\"margin-right: 0.5rem;\">No</mat-label>\r\n                                                <mat-slide-toggle id=\"read_access{{i+1}}\"\r\n                                                    [checked]=\"dynamic.value.read_access\"\r\n                                                    (change)=\"read(dynamic.value,$event,i)\">Yes</mat-slide-toggle>\r\n                                            </td>\r\n\r\n                                            <td\r\n                                                [style.background-color]=\"dynamic.value.is_parent === true ? '#90e0ef' : 'd0efff' && dynamic.value.is_child === true ? '#caf0f8' : 'd0efff'\">\r\n                                                <mat-label style=\"margin-right: 0.5rem;\">No</mat-label>\r\n                                                <mat-slide-toggle id=\"write_access{{i+1}}\"\r\n                                                    [checked]=\"dynamic.value.write_access\"\r\n                                                    (change)=\"write(dynamic.value,$event,i)\">Yes</mat-slide-toggle>\r\n                                            </td>\r\n\r\n                                            <td\r\n                                                [style.background-color]=\"dynamic.value.is_parent === true ? '#90e0ef' : 'd0efff' && dynamic.value.is_child === true ? '#caf0f8' : 'd0efff'\">\r\n                                                <mat-label style=\"margin-right: 0.5rem;\">No</mat-label>\r\n                                                <mat-slide-toggle id=\"delete_access{{i+1}}\"\r\n                                                    [checked]=\"dynamic.value.delete_access\"\r\n                                                    (change)=\"delete(dynamic.value,$event,i)\">Yes</mat-slide-toggle>\r\n                                            </td>\r\n                                        </tr>\r\n                                    </tbody>\r\n\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"card\">\r\n                    <div class=\"body\" style=\"text-align: center\">\r\n                        <div class=\"button-demo\">\r\n                            <button class=\"btn btn-primary mr5\" type=\"submit\" id=\"submit\" *ngIf=\"submitBtn\"\r\n                                [disabled]=\"showLoader\" name=\"submit\" value=\"Submit\">{{BTN_VAL}}</button>\r\n                            <button class=\"btn btn-danger\" type=\"button\" (click)=\"onCancelForm()\">Back</button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),

/***/ 9848:
/*!****************************************************************************************!*\
  !*** ./src/app/common-setup/pages-role-link/pages-role-link.component.html?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <div id=\"list_form\">\r\n        <app-screen-header [screenName]=\"screenName\" (showFormList)=\"showFormList($event)\"\r\n            [showList]=\"showList\"></app-screen-header>\r\n        <app-pages-role-link-details *ngIf=\"showList === false\" [rowData]=\"rowData\" (onSave)=\"onSave($event)\"\r\n            (onCancel)=\"onCancel($event)\" [submitBtn]=\"submitBtn\">\r\n        </app-pages-role-link-details>\r\n\r\n        <div class=\"row clearfix\" [hidden]=\"listDiv\">\r\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                <div class=\"card\">\r\n                    <div class=\"body\">\r\n                        <div class=\"table-responsive\">\r\n                            <div class=\"row clearfix\">\r\n                                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                                    <div class=\"dt-buttons btn-group flex-wrap\">\r\n                                        <button class=\"btn btn-secondary buttons-copy buttons-html5\" tabindex=\"0\"\r\n                                            aria-controls=\"example1\" type=\"button\" (click)=\"export_isp_usrs()\">\r\n                                            <span>Excel</span>\r\n                                        </button>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\"text-align: right;\">\r\n                                    <mat-form-field appearance=\"fill\">\r\n                                        <mat-label>\r\n                                            <i class=\"fa fa-search\" aria-hidden=\"true\"></i> Search\r\n                                        </mat-label>\r\n                                        <input matInput (keyup)='tbl_FilterDatatable($event.target.value)'>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-cell\">\r\n                                <ng-container matColumnDef=\"actions\">\r\n                                    <mat-header-cell *matHeaderCellDef style=\"color: white;\">Actions</mat-header-cell>\r\n                                    <mat-cell *matCellDef=\"let row;\">\r\n                                        <span *ngFor=\"let item of sidebarData\">\r\n                                            <button *ngIf=\"item.read_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                                (click)=\"viewRecord(row)\">\r\n                                                <em class=\"material-icons\">visibility</em>\r\n                                            </button>\r\n                                            <button *ngIf=\"item.write_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                                (click)=\"editRecord(row)\">\r\n                                                <em class=\"material-icons\">mode_edit</em>\r\n                                            </button>\r\n                                            <!-- <button *ngIf=\"item.delete_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                                (click)=\"deleteRecord(row)\">\r\n                                                <i class=\"material-icons\">delete</i>\r\n                                            </button> -->\r\n                                        </span>\r\n                                    </mat-cell>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"tenant_name\">\r\n                                    <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Tenant\r\n                                        Name</mat-header-cell>\r\n                                    <mat-cell *matCellDef=\"let row\"> {{row.tenant_name}}</mat-cell>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"role_name\">\r\n                                    <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Role\r\n                                        Name</mat-header-cell>\r\n                                    <mat-cell *matCellDef=\"let row\"> {{row.role_name}}</mat-cell>\r\n                                </ng-container>\r\n                                <ng-container matColumnDef=\"username\">\r\n                                    <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">User\r\n                                        Name</mat-header-cell>\r\n                                    <mat-cell *matCellDef=\"let row\"> {{row.username}}</mat-cell>\r\n                                </ng-container>\r\n\r\n                                <mat-header-row *matHeaderRowDef=\"displayColumns\"\r\n                                    style=\"background-color:#666666;\"></mat-header-row>\r\n                                <mat-row *matRowDef=\"let row; columns: displayColumns;\" [style.cursor]=\"'pointer'\"\r\n                                    matRipple>\r\n                                </mat-row>\r\n                            </mat-table>\r\n                            <mat-paginator #AssignScreenMasterPaginator=\"matPaginator\" [pageSizeOptions]=\"[ 10, 15, 20]\"\r\n                                [showFirstLastButtons]=\"true\">\r\n                            </mat-paginator>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>";

/***/ }),

/***/ 2427:
/*!*********************************************************************************************************************************!*\
  !*** ./src/app/common-setup/role-tenant-define/role-tenant-define-details/role-tenant-define-details.component.html?ngResource ***!
  \*********************************************************************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"row clearfix\">\r\n    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n        <form [formGroup]=\"roleForm\" (ngSubmit)=\"onSubmit()\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"row clearfix\">\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Tenant Name<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"tenant_id\" required\r\n                                        (selectionChange)='tenantChange($event.value)'\r\n                                        id=\"tenant_id{{i+1}}\" placeholder=\"Select Tenant\">\r\n                                        <mat-option [value]=\"data.id\"\r\n                                            *ngFor=\"let data of tenantData\">\r\n                                            {{data.tenant_name}}</mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"row clearfix\">\r\n                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                    <div class=\"card\">\r\n                        <div class=\"body\">\r\n                            <div class=\"table-responsive\" formArrayName=\"initialItemRow1\">\r\n                                <table id=\"tableExport1\"\r\n                                    class=\"display table table-hover table-checkable order-column m-t-20 width-per-100 table-bordered\">\r\n\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th style=\"text-align:center\">Action</th>\r\n                                            <th style=\"text-align:center\">Role Type</th>\r\n                                            <th style=\"text-align:center\">Role Name</th>\r\n                                        </tr>\r\n                                    </thead>\r\n\r\n                                    <tbody class=\"main_tbody\">\r\n                                        <tr *ngFor=\"let dynamic of roleForm.controls.initialItemRow1['controls']; let i = index\"\r\n                                            [formGroupName]=\"i\">\r\n                                            <td>\r\n                                                <center>\r\n                                                    <ng-container *ngIf=\"i==0\">\r\n                                                        <button class=\"btn btn-primary btn-rounded waves-effect\"\r\n                                                            type=\"button\" (click)=\"addNewRow()\">+</button>\r\n                                                    </ng-container>\r\n                                                    <ng-container *ngIf=\"i>0\">\r\n                                                        <button class=\"btn btn-danger btn-rounded waves-effect\"\r\n                                                            type=\"button\" (click)=\"deleteRow(i)\">-</button>\r\n                                                    </ng-container>\r\n                                                </center>\r\n                                            </td>\r\n\r\n                                            <td>\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"role_type\" required\r\n                                                        id=\"role_type{{i+1}}\" placeholder=\"Select Role Type\">\r\n                                                        <mat-option [value]=\"data.id\"\r\n                                                            *ngFor=\"let data of masterData\">\r\n                                                            {{data.master_value}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <input matInput formControlName=\"role_name\" id=\"role_name\" required\r\n                                                        placeholder=\"Enter Role Name\">\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                        </tr>\r\n                                    </tbody>\r\n\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"card\">\r\n                <div class=\"body\" style=\"text-align: center;\">\r\n                    <div class=\"button-demo\">\r\n                        <button class=\"btn btn-primary mr5\" type=\"submit\" *ngIf=\"submitBtn\" name=\"submit\"\r\n                            value=\"Submit\">{{BTN_VAL}}</button>\r\n                        <button (click)=\"onCancelForm()\" class=\"btn btn-danger\" value=\"Cancel\">Cancel</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>";

/***/ }),

/***/ 2426:
/*!**********************************************************************************************!*\
  !*** ./src/app/common-setup/role-tenant-define/role-tenant-define.component.html?ngResource ***!
  \**********************************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <app-screen-header [screenName]=\"screenName\" (showFormList)=\"showFormList($event)\"\r\n        [showList]=\"showList\"></app-screen-header>\r\n    <app-role-tenant-define-details *ngIf=\"showList === false\" [rowData]=\"rowData\" (onSave)=\"onSave($event)\"\r\n        (onCancel)=\"onCancel($event)\" [submitBtn]=\"submitBtn\">\r\n    </app-role-tenant-define-details>\r\n\r\n    <div class=\"row clearfix\" [hidden]=\"listDiv\">\r\n        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"table-responsive\">\r\n                        <div class=\"row clearfix\">\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                            </div>\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\"text-align: right;\">\r\n                                <mat-form-field appearance=\"fill\">\r\n                                    <mat-label>\r\n                                        <em class=\"fa fa-search\" aria-hidden=\"true\"></em>\r\n                                        Search\r\n                                    </mat-label>\r\n                                    <input matInput (keyup)='tbl_FilterDatatable($event.target.value)'>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n                        <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-cell\" id=\"excel-table\">\r\n                            <ng-container matColumnDef=\"actions\">\r\n                                <mat-header-cell *matHeaderCellDef style=\"color: white;\">Actions</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row;\">\r\n                                    <span *ngFor=\"let item of sidebarData\">\r\n                                        <button *ngIf=\"item.read_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"viewRecord(row)\">\r\n                                            <em class=\"material-icons\">visibility</em>\r\n                                        </button>\r\n                                        <button *ngIf=\"item.write_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"editRecord(row)\">\r\n                                            <em class=\"material-icons\">mode_edit</em>\r\n                                        </button>\r\n                                    </span>\r\n                                </mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"tenant_name\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Role\r\n                                    Name</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.tenant_name}}</mat-cell>\r\n                            </ng-container>\r\n\r\n                            <mat-header-row *matHeaderRowDef=\"displayColumns\"\r\n                                style=\"background-color:#666666;\"></mat-header-row>\r\n                            <mat-row *matRowDef=\"let row; columns: displayColumns;\" [style.cursor]=\"'pointer'\"\r\n                                matRipple></mat-row>\r\n                        </mat-table>\r\n                        <mat-paginator #dataSourcePaginator=\"matPaginator\" [pageSizeOptions]=\"[ 10, 15, 20]\"\r\n                            [showFirstLastButtons]=\"true\">\r\n                        </mat-paginator>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>";

/***/ }),

/***/ 5144:
/*!********************************************************************************************************!*\
  !*** ./src/app/common-setup/tenant-hierarchy-define/tenant-hierarchy-define.component.html?ngResource ***!
  \********************************************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <div class=\"row clearfix\">\r\n        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n            <div class=\"card\">\r\n                <div class=\"row clearfix\">\r\n                    <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                        <div class=\"header\">\r\n                            <h2>\r\n                                <strong>{{screenName}}</strong>\r\n                            </h2>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"row clearfix\">\r\n        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n            <form [formGroup]=\"tenanthirarchydefineForm\" (ngSubmit)=\"onSubmit()\">\r\n                <div class=\"card\">\r\n                    <div class=\"body\">\r\n                        <div class=\"row clearfix\">\r\n                            <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                <div class=\"form-group\">\r\n                                    <mat-label class=\"form-label\">Is Permanent?</mat-label>\r\n                                    <mat-label style=\"margin-left: 1rem; margin-right: 0.5rem;\">No</mat-label>\r\n                                    <mat-slide-toggle id=\"is_permanent\"\r\n                                        formControlName=\"is_permanent\">Yes</mat-slide-toggle>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n                <div class=\"row clearfix\">\r\n                    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                        <div class=\"card\">\r\n                            <div class=\"body\">\r\n                                <h5 style=\"text-align: center;\"><strong>Define {{screenName}} Details</strong></h5><br>\r\n\r\n                                <div class=\"table-responsive\" formArrayName=\"initialItemRow1\">\r\n                                    <table id=\"tableExport1\"\r\n                                        class=\"display table table-hover table-checkable order-column m-t-20 width-per-100 table-bordered\">\r\n\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th style=\"text-align:center\" *ngIf=\"showupdate\">Action</th>\r\n                                                <th style=\"text-align:center\">Tenant Type</th>\r\n                                                <th style=\"text-align:center\">Sequence No</th>\r\n                                            </tr>\r\n                                        </thead>\r\n\r\n                                        <tbody class=\"main_tbody\">\r\n                                            <tr *ngFor=\"let dynamic of tenanthirarchydefineForm.controls.initialItemRow1['controls']; let i = index\"\r\n                                                [formGroupName]=\"i\">\r\n                                                <td *ngIf=\"showupdate\">\r\n                                                    <center>\r\n                                                        <ng-container *ngIf=\"i==0\">\r\n                                                            <button type=\"button\"\r\n                                                                class=\"btn btn-primary btn-rounded waves-effect\"\r\n                                                                (click)=\"addNewRow()\">+</button>\r\n                                                        </ng-container>\r\n                                                        <ng-container *ngIf=\"i>0\">\r\n                                                            <button type=\"button\"\r\n                                                                class=\"btn btn-danger btn-rounded waves-effect\"\r\n                                                                (click)=\"deleteRow(i)\">-</button>\r\n                                                        </ng-container>\r\n                                                    </center>\r\n                                                </td>\r\n\r\n                                                <td class=\"col_input\">\r\n                                                    <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                        <mat-select formControlName=\"tenant_type\"\r\n                                                            (selectionChange)=\"getSequence(i)\" id=\"tenant_type{{i+1}}\"\r\n                                                            placeholder=\"Select Tenant Type\">\r\n                                                            <mat-option *ngFor=\"let data of tenantHierarchyData\"\r\n                                                                [value]=\"data.id\">\r\n                                                                {{ data.master_value }}\r\n                                                            </mat-option>\r\n                                                        </mat-select>\r\n                                                    </mat-form-field>\r\n                                                </td>\r\n\r\n                                                <td class=\"col_input\">\r\n                                                    <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                        <input matInput type=\"number\" id=\"sequence_no{{i+1}}\"\r\n                                                            formControlName=\"sequence_no\"\r\n                                                            placeholder=\"Enter Sequence Number\" readonly>\r\n                                                    </mat-form-field>\r\n                                                </td>\r\n                                            </tr>\r\n                                        </tbody>\r\n\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <div class=\"card\" *ngIf=\"showupdate\">\r\n                    <div class=\"body\" style=\"text-align: center;\">\r\n                        <div class=\"button-demo\">\r\n                            <button class=\"btn btn-primary mr5\" type=\"submit\" name=\"submit\"\r\n                                value=\"Submit\">{{BTN_VAL}}</button>\r\n                            <button (click)=\"onCancelForm()\" class=\"btn btn-danger\" value=\"Cancel\">Cancel</button>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </form>\r\n        </div>\r\n    </div>\r\n</section>";

/***/ }),

/***/ 930:
/*!*********************************************************************************************************************!*\
  !*** ./src/app/common-setup/tenant-onboard/tenant-onboard-details/tenant-onboard-details.component.html?ngResource ***!
  \*********************************************************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"row clearfix\">\r\n    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n        <form [formGroup]=\"tenantonboardForm\" (ngSubmit)=\"onSubmit()\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"row clearfix\">\r\n                        <input type=\"hidden\" formControlName=\"id\" id=\"id\" value=\"[id]\" class=\"form-control\">\r\n                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Tenant Name<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <input matInput formControlName=\"tenant_name\" placeholder=\"Enter Tenant Name\"\r\n                                        required>\r\n                                    <mat-error\r\n                                        *ngIf=\"tenantonboardForm.controls['tenant_name'].hasError('required')\">Tenant\r\n                                        Name is mandatory.</mat-error>\r\n                                    <mat-error\r\n                                        *ngIf=\"tenantonboardForm.controls['tenant_name'].hasError('maxlength')\">Maximum\r\n                                        50 characters.</mat-error>\r\n                                    <mat-error\r\n                                        *ngIf=\"tenantonboardForm.controls['tenant_name'].hasError('pattern')\">Character(s)\r\n                                        are not allowed.</mat-error>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Short Name<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <input matInput formControlName=\"tenant_shortname\" placeholder=\"Enter Short Name\"\r\n                                        required>\r\n                                    <mat-error\r\n                                        *ngIf=\"tenantonboardForm.controls['tenant_shortname'].hasError('maxlength')\">Maximum\r\n                                        30 characters.</mat-error>\r\n                                    <mat-error\r\n                                        *ngIf=\"tenantonboardForm.controls['tenant_shortname'].hasError('required')\">Short\r\n                                        Name is mandatory.</mat-error>\r\n                                    <mat-error\r\n                                        *ngIf=\"tenantonboardForm.controls['tenant_shortname'].hasError('pattern')\">Character(s)\r\n                                        are not allowed.</mat-error>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <label class=\"form-label\">Is Legal Tenant?</label><br>\r\n                                <mat-label style=\"margin-right: 0.5rem;\">No</mat-label>\r\n                                <mat-slide-toggle id=\"is_legal_tenant\"\r\n                                    formControlName=\"is_legal_tenant\">Yes</mat-slide-toggle>\r\n                            </div>\r\n                        </div>\r\n\r\n\r\n                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Tenant Code<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <input matInput formControlName=\"tenant_code\" placeholder=\"Enter Tenant Code\"\r\n                                        required>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Type of Tenant <em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"tenant_type_ref_id\" required\r\n                                        placeholder=\"Select Tenant Type\"\r\n                                        (selectionChange)='tenant_type_chnage($event.value)' [disabled]=\"editdisabled\">\r\n                                        <mat-option [value]=\"tenant.id\"\r\n                                            *ngFor=\"let tenant of tenantHierarchyData\">{{tenant.tenant_type_name}}</mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Tenant Belongs To<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"tenant_belongs_to\" [disabled]=\"editdisabled\"\r\n                                        placeholder=\"Select Parent Tenant\">\r\n                                        <mat-option [value]=\"tenant.id\"\r\n                                            *ngFor=\"let tenant of tenantData\">{{tenant.tenant_name}}</mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Address1<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <textarea matInput formControlName=\"address1\" placeholder=\"Enter Address1\"\r\n                                        required></textarea>\r\n                                    <mat-error\r\n                                        *ngIf=\"tenantonboardForm.controls['address1'].hasError('required')\">Address1 is\r\n                                        mandatory.</mat-error>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Address2</mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <input matInput formControlName=\"address2\" placeholder=\"Enter Address2\">\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Bank Tenant</mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"bank_tenant_ref_id\" [disabled]=\"editdisabled\"\r\n                                        placeholder=\"Select Bank\">\r\n                                        <mat-option [value]=\"tenant.id\"\r\n                                            *ngFor=\"let tenant of BankTenantData\">{{tenant.tenant_name}}</mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n\r\n                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Pin Code<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <input matInput formControlName=\"pincode\" placeholder=\"Enter Pincode\" type=\"number\"\r\n                                        required>\r\n                                    <mat-error\r\n                                        *ngIf=\"tenantonboardForm.controls['pincode'].hasError('required')\">Pincode is\r\n                                        mandatory.</mat-error>\r\n                                    <mat-error *ngIf=\"tenantonboardForm.controls['pincode'].hasError('pattern')\">Pincode\r\n                                        Not Valid.</mat-error>\r\n                                    <mat-error\r\n                                        *ngIf=\"tenantonboardForm.controls['pincode'].hasError('maxlength')\">Maximum 6\r\n                                        characters.</mat-error>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Country<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"country_ref_id\" required placeholder=\"Select Country\"\r\n                                        (selectionChange)='countrySelected($event.value)'>\r\n                                        <mat-option *ngFor=\"let currency of countrydata\"\r\n                                            [value]=\"currency.id\">{{currency.country_name}}</mat-option>\r\n                                    </mat-select>\r\n                                    <mat-error *ngIf=\"tenantonboardForm.get('country_ref_id')\">Please Select\r\n                                        Country</mat-error>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>State<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"state_ref_id\" required placeholder=\"Select State\"\r\n                                        (selectionChange)='stateSelected($event.value)'>\r\n                                        <mat-option [value]=\"state.id\"\r\n                                            *ngFor=\"let state of statedata\">{{state.state}}</mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>City<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"city_ref_id\" required placeholder=\"Select City\">\r\n                                        <mat-option [value]=\"city.id\"\r\n                                            *ngFor=\"let city of cityData\">{{city.city_name}}</mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <label class=\"form-label\">Is Active?</label><br>\r\n                                <mat-label>No</mat-label>\r\n                                <mat-slide-toggle id=\"is_active\" (change)=\"changeState($event)\"\r\n                                    formControlName=\"is_active\">Yes</mat-slide-toggle>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n            <div class=\"card\">\r\n                <div class=\"body\" style=\"text-align: center;\">\r\n                    <div class=\"button-demo\">\r\n                        <button class=\"btn btn-primary mr5\" type=\"submit\" *ngIf=\"submitBtn\" name=\"submit\"\r\n                            value=\"Submit\">{{BTN_VAL}}</button>\r\n                        <button (click)=\"onCancelForm()\" class=\"btn btn-danger\" value=\"Cancel\">Cancel</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>";

/***/ }),

/***/ 7002:
/*!**************************************************************************************!*\
  !*** ./src/app/common-setup/tenant-onboard/tenant-onboard.component.html?ngResource ***!
  \**************************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <app-screen-header [screenName]=\"screenName\" (showFormList)=\"showFormList($event)\"\r\n        [showList]=\"showList\"></app-screen-header>\r\n    <app-tenant-onboard-details *ngIf=\"showList === false\" [rowData]=\"rowData\" (onSave)=\"onSave($event)\"\r\n        (onCancel)=\"onCancel($event)\" [submitBtn]=\"submitBtn\" [dataSource]=\"dataSource.filteredData\">\r\n    </app-tenant-onboard-details>\r\n\r\n    <div class=\"row clearfix\" [hidden]=\"listDiv\">\r\n        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"table-responsive\">\r\n                        <div class=\"row clearfix\">\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                                <div class=\"dt-buttons btn-group flex-wrap\">\r\n                                    <button class=\"btn btn-secondary  buttons-html5\" tabindex=\"0\"\r\n                                        aria-controls=\"example1\" type=\"button\" (click)=\"export_to_excel();\">\r\n                                        <span>Export To Excel</span>\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\"text-align: right;\">\r\n                                <mat-form-field appearance=\"fill\">\r\n                                    <mat-label>\r\n                                        <em class=\"fa fa-search\" aria-hidden=\"true\"></em>\r\n                                        Search\r\n                                    </mat-label>\r\n                                    <input matInput (keyup)='tbl_FilterDatatable($event.target.value)'>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n                        <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-cell\" id=\"excel-table\">\r\n                            <ng-container matColumnDef=\"actions\">\r\n                                <mat-header-cell *matHeaderCellDef style=\"color: white;\">Actions</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row;\">\r\n                                    <span *ngFor=\"let item of sidebarData\">\r\n                                        <button *ngIf=\"item.read_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"viewRecord(row)\">\r\n                                            <em class=\"material-icons\">visibility</em>\r\n                                        </button>\r\n                                        <button *ngIf=\"item.write_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"editRecord(row)\">\r\n                                            <em class=\"material-icons\">mode_edit</em>\r\n                                        </button>\r\n                                    </span>\r\n                                </mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"tenant_name\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Tenant Name\r\n                                </mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.tenant_name}}</mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"is_legal_tenant\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Is Legal\r\n                                    Tenant</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.is_legal_tenant}}</mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"tenant_type\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Tenant\r\n                                    Type</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.tenant_type}}</mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"address1\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                    style=\"color: white;\">Address1</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.address1}}</mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"pincode\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Pin\r\n                                    code</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.pincode}}</mat-cell>\r\n                            </ng-container>\r\n                            <mat-header-row *matHeaderRowDef=\"displayColumns\"\r\n                                style=\"background-color:#666666;\"></mat-header-row>\r\n                            <mat-row *matRowDef=\"let row; columns: displayColumns;\" [style.cursor]=\"'pointer'\"\r\n                                matRipple></mat-row>\r\n                        </mat-table>\r\n                        <mat-paginator #dataSourcePaginator=\"matPaginator\" [pageSizeOptions]=\"[ 10, 15, 20]\"\r\n                            [showFirstLastButtons]=\"true\">\r\n                        </mat-paginator>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>";

/***/ }),

/***/ 4563:
/*!******************************************************************************************************************************!*\
  !*** ./src/app/common-setup/user-registration/user-registration-details/user-registration-details.component.html?ngResource ***!
  \******************************************************************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"row clearfix\">\r\n    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n        <form [formGroup]=\"UserRegistrationForm\" (ngSubmit)=\"onSubmit()\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"row clearfix\">\r\n                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Email<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <input matInput formControlName=\"email\" type=\"email\" required\r\n                                        pattern=\"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}$\" placeholder=\"Enter Email id\">\r\n                                    <mat-icon matSuffix>email</mat-icon>\r\n                                    <mat-error *ngIf=\"UserRegistrationForm.controls['email'].hasError('pattern')\">Enter\r\n                                        Valid Email.</mat-error>\r\n                                    <mat-error *ngIf=\"UserRegistrationForm.controls['email'].hasError('required')\">email\r\n                                        is mandatory.</mat-error>\r\n                                    <mat-error *ngIf=\"UserRegistrationForm.controls['email'].hasError('email')\">Email id\r\n                                        not valid.</mat-error>\r\n                                    <mat-error\r\n                                        *ngIf=\"UserRegistrationForm.controls['email'].hasError('maxlength')\">Maximum 30\r\n                                        characters.</mat-error>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Mobile Number<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <input matInput formControlName=\"mobile_number\" placeholder=\"Enter Mobile Number\"\r\n                                        type=\"number\" required>\r\n                                    <mat-error\r\n                                        *ngIf=\"UserRegistrationForm.controls['mobile_number'].hasError('pattern')\">Enter\r\n                                        Valid Mobile Number</mat-error>\r\n                                    <mat-error\r\n                                        *ngIf=\"UserRegistrationForm.controls['mobile_number'].hasError('required')\">Mobile\r\n                                        Number is mandatory.</mat-error>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Role Name<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"role_name\" required placeholder=\"Select Role Name\">\r\n                                        <mat-option [value]=\"file.role_name\" *ngFor=\"let file of RoleNameData\">\r\n                                            {{file.role_name}}</mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Tenant Name<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"tenant_ref_id\" required placeholder=\"Select Tenant\">\r\n                                        <mat-option [value]=\"file.id\" *ngFor=\"let file of TenantData\">\r\n                                            {{file.tenant_name}}</mat-option>\r\n                                    </mat-select>\r\n\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"card\">\r\n                <div class=\"body\" style=\"text-align: center;\">\r\n                    <div class=\"button-demo\">\r\n                        <button class=\"btn btn-primary mr5\" type=\"submit\" name=\"submit\"\r\n                            value=\"Submit\">{{BTN_VAL}}</button>\r\n                        <button (click)=\"onCancelForm()\" class=\"btn btn-danger\" value=\"Cancel\">Cancel</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>";

/***/ }),

/***/ 7777:
/*!********************************************************************************************!*\
  !*** ./src/app/common-setup/user-registration/user-registration.component.html?ngResource ***!
  \********************************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <app-screen-header [screenName]=\"screenName\" (showFormList)=\"showFormList($event)\"\r\n        [showList]=\"showList\"></app-screen-header>\r\n    <app-user-registration-details *ngIf=\"showList === false\" [rowData]=\"rowData\" (onSave)=\"onSave($event)\"\r\n        (onCancel)=\"onCancel($event)\" [submitBtn]=\"submitBtn\">\r\n    </app-user-registration-details>\r\n\r\n    <div class=\"row clearfix\" [hidden]=\"listDiv\">\r\n        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"table-responsive\">\r\n                        <div class=\"row clearfix\">\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                                <div class=\"dt-buttons btn-group flex-wrap\">\r\n                                    <button class=\"btn btn-secondary  buttons-html5\" tabindex=\"0\"\r\n                                        aria-controls=\"example1\" type=\"button\" (click)=\"export_to_excel();\">\r\n                                        <span>Export To Excel</span>\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\"text-align: right;\">\r\n                                <mat-form-field appearance=\"fill\">\r\n                                    <mat-label>\r\n                                        <em class=\"fa fa-search\" aria-hidden=\"true\"></em>\r\n                                        Search\r\n                                    </mat-label>\r\n                                    <input matInput (keyup)='tbl_FilterDatatable($event.target.value)'>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n                        <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-cell\" id=\"excel-table\">\r\n                            <ng-container matColumnDef=\"actions\">\r\n                                <mat-header-cell *matHeaderCellDef style=\"color: white;\">Actions</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row;\">\r\n                                    <span *ngFor=\"let item of sidebarData\">\r\n                                        <button *ngIf=\"item.read_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"viewRecord(row)\">\r\n                                            <em class=\"material-icons\">visibility</em>\r\n                                        </button>\r\n                                        <button *ngIf=\"item.write_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"editRecord(row)\">\r\n                                            <em class=\"material-icons\">mode_edit</em>\r\n                                        </button>\r\n                                    </span>\r\n                                </mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"email\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Email Id\r\n                                </mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.email}}</mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"mobile_number\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Mobile Number\r\n                                </mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.mobile_number}}</mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"role_name\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Role Name\r\n                                </mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.role_name}}</mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"tenant_name\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Tenant Name\r\n                                </mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.tenant_name}}</mat-cell>\r\n                            </ng-container>\r\n                            <mat-header-row *matHeaderRowDef=\"displayColumns\"\r\n                                style=\"background-color:#666666;\"></mat-header-row>\r\n                            <mat-row *matRowDef=\"let row; columns: displayColumns;\" [style.cursor]=\"'pointer'\"\r\n                                matRipple></mat-row>\r\n                        </mat-table>\r\n                        <mat-paginator #dataSourcePaginator=\"matPaginator\" [pageSizeOptions]=\"[ 10, 15, 20]\"\r\n                            [showFirstLastButtons]=\"true\">\r\n                        </mat-paginator>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>";

/***/ })

}]);
//# sourceMappingURL=src_app_common-setup_common-setup_module_ts.js.map