"use strict";
(self["webpackChunkatrio"] = self["webpackChunkatrio"] || []).push([["src_app_authentication_authentication_module_ts"],{

/***/ 1356:
/*!*****************************************************************!*\
  !*** ./src/app/authentication/authentication-routing.module.ts ***!
  \*****************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthenticationRoutingModule": () => (/* binding */ AuthenticationRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _signin_signin_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./signin/signin.component */ 9320);
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./signup/signup.component */ 7794);
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ 3897);
/* harmony import */ var _locked_locked_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./locked/locked.component */ 11);
/* harmony import */ var _page404_page404_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./page404/page404.component */ 1991);
/* harmony import */ var _page500_page500_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./page500/page500.component */ 1194);
/* harmony import */ var _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./reset-password/reset-password.component */ 3431);










const routes = [
    {
        path: '',
        redirectTo: 'signin',
        pathMatch: 'full'
    },
    {
        path: 'signin',
        component: _signin_signin_component__WEBPACK_IMPORTED_MODULE_0__.SigninComponent
    },
    {
        path: 'signup',
        component: _signup_signup_component__WEBPACK_IMPORTED_MODULE_1__.SignupComponent
    },
    {
        path: 'forgot-password',
        component: _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_2__.ForgotPasswordComponent
    },
    {
        path: 'locked',
        component: _locked_locked_component__WEBPACK_IMPORTED_MODULE_3__.LockedComponent
    },
    {
        path: 'page404',
        component: _page404_page404_component__WEBPACK_IMPORTED_MODULE_4__.Page404Component
    },
    {
        path: 'page500',
        component: _page500_page500_component__WEBPACK_IMPORTED_MODULE_5__.Page500Component
    },
    {
        path: 'reset-password',
        component: _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_6__.ResetPasswordComponent
    }
];
let AuthenticationRoutingModule = class AuthenticationRoutingModule {
};
AuthenticationRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_9__.RouterModule]
    })
], AuthenticationRoutingModule);



/***/ }),

/***/ 1082:
/*!*********************************************************!*\
  !*** ./src/app/authentication/authentication.module.ts ***!
  \*********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "AuthenticationModule": () => (/* binding */ AuthenticationModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _authentication_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./authentication-routing.module */ 1356);
/* harmony import */ var _page500_page500_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./page500/page500.component */ 1194);
/* harmony import */ var _page404_page404_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./page404/page404.component */ 1991);
/* harmony import */ var _signin_signin_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./signin/signin.component */ 9320);
/* harmony import */ var _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./signup/signup.component */ 7794);
/* harmony import */ var _locked_locked_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./locked/locked.component */ 11);
/* harmony import */ var _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./forgot-password/forgot-password.component */ 3897);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/form-field */ 9076);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/input */ 3365);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/icon */ 5590);
/* harmony import */ var _angular_material_button__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/button */ 7317);
/* harmony import */ var _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./reset-password/reset-password.component */ 3431);

















let AuthenticationModule = class AuthenticationModule {
};
AuthenticationModule = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.NgModule)({
        declarations: [
            _page500_page500_component__WEBPACK_IMPORTED_MODULE_1__.Page500Component,
            _page404_page404_component__WEBPACK_IMPORTED_MODULE_2__.Page404Component,
            _signin_signin_component__WEBPACK_IMPORTED_MODULE_3__.SigninComponent,
            _signup_signup_component__WEBPACK_IMPORTED_MODULE_4__.SignupComponent,
            _locked_locked_component__WEBPACK_IMPORTED_MODULE_5__.LockedComponent,
            _forgot_password_forgot_password_component__WEBPACK_IMPORTED_MODULE_6__.ForgotPasswordComponent,
            _reset_password_reset_password_component__WEBPACK_IMPORTED_MODULE_7__.ResetPasswordComponent
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_10__.CommonModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__.ReactiveFormsModule,
            _authentication_routing_module__WEBPACK_IMPORTED_MODULE_0__.AuthenticationRoutingModule,
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_12__.MatFormFieldModule,
            _angular_material_input__WEBPACK_IMPORTED_MODULE_13__.MatInputModule,
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_14__.MatIconModule,
            _angular_material_button__WEBPACK_IMPORTED_MODULE_15__.MatButtonModule
        ]
    })
], AuthenticationModule);



/***/ }),

/***/ 3897:
/*!*****************************************************************************!*\
  !*** ./src/app/authentication/forgot-password/forgot-password.component.ts ***!
  \*****************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ForgotPasswordComponent": () => (/* binding */ ForgotPasswordComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _forgot_password_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./forgot-password.component.html?ngResource */ 9064);
/* harmony import */ var _forgot_password_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./forgot-password.component.scss?ngResource */ 3343);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ 587);






let ForgotPasswordComponent = class ForgotPasswordComponent {
    constructor(formBuilder, route, router) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.router = router;
        this.submitted = false;
    }
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            email: [
                '',
                [_angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.minLength(5)]
            ]
        });
    }
    get f() {
        return this.loginForm.controls;
    }
    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        else {
            this.router.navigate(['/dashboard/main']);
        }
    }
};
ForgotPasswordComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__.UntypedFormBuilder },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router }
];
ForgotPasswordComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-forgot-password',
        template: _forgot_password_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_forgot_password_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ForgotPasswordComponent);



/***/ }),

/***/ 11:
/*!***********************************************************!*\
  !*** ./src/app/authentication/locked/locked.component.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "LockedComponent": () => (/* binding */ LockedComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _locked_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./locked.component.html?ngResource */ 3705);
/* harmony import */ var _locked_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./locked.component.scss?ngResource */ 372);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ 587);






let LockedComponent = class LockedComponent {
    constructor(formBuilder, router) {
        this.formBuilder = formBuilder;
        this.router = router;
        this.submitted = false;
        this.hide = true;
    }
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required]
        });
    }
    get f() {
        return this.loginForm.controls;
    }
    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        else {
            this.router.navigate(['/dashboard/main']);
        }
    }
};
LockedComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__.UntypedFormBuilder },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router }
];
LockedComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-locked',
        template: _locked_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_locked_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], LockedComponent);



/***/ }),

/***/ 1194:
/*!*************************************************************!*\
  !*** ./src/app/authentication/page500/page500.component.ts ***!
  \*************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "Page500Component": () => (/* binding */ Page500Component)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _page500_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./page500.component.html?ngResource */ 2497);
/* harmony import */ var _page500_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./page500.component.scss?ngResource */ 2081);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ 2816);





let Page500Component = class Page500Component {
    constructor(router) {
        this.router = router;
    }
    ngOnInit() { }
    submit() {
        this.router.navigate(['/authentication/signin']);
    }
};
Page500Component.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__.Router }
];
Page500Component = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Component)({
        selector: 'app-page500',
        template: _page500_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_page500_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], Page500Component);



/***/ }),

/***/ 3431:
/*!***************************************************************************!*\
  !*** ./src/app/authentication/reset-password/reset-password.component.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ResetPasswordComponent": () => (/* binding */ ResetPasswordComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _reset_password_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./reset-password.component.html?ngResource */ 510);
/* harmony import */ var _reset_password_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./reset-password.component.sass?ngResource */ 1440);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var src_app_core_service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/auth.service */ 1782);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_3__);









let ResetPasswordComponent = class ResetPasswordComponent {
    constructor(authService, formBuilder, router, datepipe) {
        this.authService = authService;
        this.formBuilder = formBuilder;
        this.router = router;
        this.datepipe = datepipe;
        this.submitted = false;
        this.changepassword = [];
        const navigation = this.router.getCurrentNavigation();
        this.state = navigation.extras.state;
    }
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            old_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required],
            confirm_password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required]
        });
    }
    checkPassword2() {
        var confirm_password = this.loginForm.value.confirm_password;
        var password = this.loginForm.value.password;
        var old_password = this.loginForm.value.old_password;
        if (password != '' && confirm_password != '') {
            if (confirm_password != password) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default().fire({
                    title: 'Enter same confirm password as password',
                    icon: 'info',
                    timer: 1000,
                    showConfirmButton: false
                });
            }
        }
        else if (password != '' && old_password != '' && old_password == password) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default().fire({
                title: 'Enter different password other than old password',
                icon: 'info',
                timer: 1000,
                showConfirmButton: false
            });
        }
    }
    checkPassword() {
        var confirm_password = this.loginForm.value.confirm_password;
        var password = this.loginForm.value.password;
        if (confirm_password != password) {
            sweetalert2__WEBPACK_IMPORTED_MODULE_3___default().fire({
                title: 'Enter same confirm password as password',
                icon: 'info',
                timer: 1000,
                showConfirmButton: false
            });
        }
    }
    get f() { return this.loginForm.controls; }
    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            this.submitted = false;
            return;
        }
        else {
            const password = this.loginForm.value.password;
            const confirm_password = this.loginForm.value.confirm_password;
            if (password == confirm_password) {
                this.authService.changePassword(this.loginForm.value).subscribe(res => {
                    sweetalert2__WEBPACK_IMPORTED_MODULE_3___default().fire({
                        title: "Password updated sucessfully",
                        icon: 'success',
                        timer: 2000,
                        showConfirmButton: false
                    });
                    this.router.navigate(['/authentication/signin']);
                });
            }
            else {
                sweetalert2__WEBPACK_IMPORTED_MODULE_3___default().fire({
                    title: "Passsword does not match!",
                    icon: 'error',
                    timer: 2000,
                    showConfirmButton: false
                });
                this.router.navigate(['/authentication/reset-password']);
            }
        }
    }
};
ResetPasswordComponent.ctorParameters = () => [
    { type: src_app_core_service_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthService },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormBuilder },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_6__.DatePipe }
];
ResetPasswordComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_8__.Component)({
        selector: 'app-reset-password',
        template: _reset_password_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        providers: [_angular_common__WEBPACK_IMPORTED_MODULE_6__.DatePipe],
        styles: [_reset_password_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ResetPasswordComponent);



/***/ }),

/***/ 9320:
/*!***********************************************************!*\
  !*** ./src/app/authentication/signin/signin.component.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SigninComponent": () => (/* binding */ SigninComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _signin_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./signin.component.html?ngResource */ 8610);
/* harmony import */ var _signin_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./signin.component.scss?ngResource */ 6331);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var src_app_core_service_auth_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/auth.service */ 1782);
/* harmony import */ var src_app_shared_UnsubscribeOnDestroyAdapter__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/shared/UnsubscribeOnDestroyAdapter */ 7868);








let SigninComponent = class SigninComponent extends src_app_shared_UnsubscribeOnDestroyAdapter__WEBPACK_IMPORTED_MODULE_3__.UnsubscribeOnDestroyAdapter {
    constructor(formBuilder, router, authService) {
        super();
        this.formBuilder = formBuilder;
        this.router = router;
        this.authService = authService;
        this.submitted = false;
        this.error = '';
        this.hide = true;
    }
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            user_number: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required]
        });
    }
    get f() {
        return this.loginForm.controls;
    }
    onSubmit() {
        this.submitted = true;
        this.error = '';
        if (this.loginForm.invalid) {
            this.error = 'Username and Password not valid !';
            this.submitted = false;
            return;
        }
        else {
            this.subs.sink = this.authService
                .login(this.f.user_number.value, this.f.password.value)
                .subscribe((res) => {
                if (res) {
                    const token = this.authService.currentUserValue['access'];
                    if (token) {
                        localStorage.setItem('token', token);
                        this.authService.refreshToken(this.authService.currentUserValue['refresh']).subscribe(data => {
                            localStorage.removeItem('token');
                            localStorage.setItem('token', data['access']);
                            localStorage.setItem('refresh', data['refresh']);
                            this.authService.getUser().subscribe(data1 => {
                                localStorage.setItem('user_type', data1[0]['user_type']);
                                localStorage.setItem('user_number', data1[0]['user_number']);
                                localStorage.setItem('tenant_ref_id', data1[0]['tenant_ref_id']);
                                localStorage.setItem('user_id', data1[0]['user_id']);
                                localStorage.setItem('role_id', data1[0]['role_id']);
                                localStorage.setItem('role_name', data1[0]['role_name']);
                                this.router.navigate(['/tenant-setup/tenant-hierarchy-define']).then(() => window.location.reload());
                            });
                        });
                    }
                }
                else {
                    this.error = 'Invalid Login';
                }
            }, (error) => {
                error = JSON.parse(error);
                if (error.status === 0) {
                    this.error = '';
                    location.reload();
                }
                else {
                    this.error = error;
                    this.submitted = false;
                }
            });
        }
    }
};
SigninComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.UntypedFormBuilder },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__.Router },
    { type: src_app_core_service_auth_service__WEBPACK_IMPORTED_MODULE_2__.AuthService }
];
SigninComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_6__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-signin',
        template: _signin_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_signin_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], SigninComponent);



/***/ }),

/***/ 7794:
/*!***********************************************************!*\
  !*** ./src/app/authentication/signup/signup.component.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "SignupComponent": () => (/* binding */ SignupComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _signup_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./signup.component.html?ngResource */ 957);
/* harmony import */ var _signup_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./signup.component.scss?ngResource */ 9821);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ 587);






let SignupComponent = class SignupComponent {
    constructor(formBuilder, route, router) {
        this.formBuilder = formBuilder;
        this.route = route;
        this.router = router;
        this.submitted = false;
        this.hide = true;
        this.chide = true;
    }
    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required],
            email: [
                '',
                [_angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.email, _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.minLength(5)]
            ],
            password: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required],
            cpassword: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__.Validators.required]
        });
    }
    get f() {
        return this.loginForm.controls;
    }
    onSubmit() {
        this.submitted = true;
        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }
        else {
            this.router.navigate(['/dashboard/main']);
        }
    }
};
SignupComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__.UntypedFormBuilder },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.ActivatedRoute },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__.Router }
];
SignupComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-signup',
        template: _signup_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_signup_component_scss_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], SignupComponent);



/***/ }),

/***/ 3343:
/*!******************************************************************************************!*\
  !*** ./src/app/authentication/forgot-password/forgot-password.component.scss?ngResource ***!
  \******************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmb3Jnb3QtcGFzc3dvcmQuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 372:
/*!************************************************************************!*\
  !*** ./src/app/authentication/locked/locked.component.scss?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJsb2NrZWQuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 2081:
/*!**************************************************************************!*\
  !*** ./src/app/authentication/page500/page500.component.scss?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwYWdlNTAwLmNvbXBvbmVudC5zY3NzIn0= */";

/***/ }),

/***/ 6331:
/*!************************************************************************!*\
  !*** ./src/app/authentication/signin/signin.component.scss?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzaWduaW4uY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 9821:
/*!************************************************************************!*\
  !*** ./src/app/authentication/signup/signup.component.scss?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzaWdudXAuY29tcG9uZW50LnNjc3MifQ== */";

/***/ }),

/***/ 1440:
/*!****************************************************************************************!*\
  !*** ./src/app/authentication/reset-password/reset-password.component.sass?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJyZXNldC1wYXNzd29yZC5jb21wb25lbnQuc2FzcyJ9 */";

/***/ }),

/***/ 9064:
/*!******************************************************************************************!*\
  !*** ./src/app/authentication/forgot-password/forgot-password.component.html?ngResource ***!
  \******************************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"container-fluid p-0\">\r\n  <div class=\"row g-0\">\r\n    <div class=\"col-lg-4\">\r\n      <div class=\"auth-content p-4 d-flex align-items-center min-vh-100\">\r\n        <div class=\"w-100\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-9\">\r\n              <div>\r\n                <div class=\"text-center\">\r\n                  <div>\r\n                    <a routerLink=\"\" class=\"logo-full\"><img src=\"assets/images/logo-full.png\" alt=\"logo-full\"></a>\r\n                  </div>\r\n\r\n                  <h4 class=\"font-20 mt-4\">Forgotten Password ?</h4>\r\n                  <p class=\"text-muted\">Enter your email to reset your password</p>\r\n                </div>\r\n\r\n                <div class=\"p-2 mt-5\">\r\n                  <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\r\n                    <div class=\"form-group auth-form-group mb-4\">\r\n                      <mat-icon matSuffix class=\"auth-page-icon\">email</mat-icon>\r\n                      <input type=\"email\" formControlName=\"email\" class=\"form-control auth-control\" id=\"email\"\r\n                        placeholder=\"Email\" [ngClass]=\"{ 'is-invalid': submitted && f.email.errors }\">\r\n                      <div *ngIf=\"submitted && f.email.errors\" class=\"invalid-feedback\">\r\n                        <div *ngIf=\"f.email.errors.required\">Email is required</div>\r\n                        <div *ngIf=\"f.email.errors.email\">Email must be a valid email address</div>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"container-login100-form-btn\">\r\n                      <button mat-flat-button color=\"primary\" class=\"login100-form-btn\" type=\"submit\">\r\n                        Reset My Password\r\n                      </button>\r\n                    </div>\r\n\r\n                  </form>\r\n                </div>\r\n\r\n                <div class=\"mt-5 text-center\">\r\n                  <p>Want To Login ? <a routerLink=\"/authentication/signin\" class=\"font-weight-medium text-primary\">\r\n                      Log in </a> </p>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-lg-8\">\r\n      <div class=\"auth-bg\">\r\n        <div class=\"bg-overlay\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n";

/***/ }),

/***/ 3705:
/*!************************************************************************!*\
  !*** ./src/app/authentication/locked/locked.component.html?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"container-fluid p-0\">\r\n  <div class=\"row g-0\">\r\n    <div class=\"col-lg-4\">\r\n      <div class=\"auth-content p-4 d-flex align-items-center min-vh-100\">\r\n        <div class=\"w-100\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-9\">\r\n              <div>\r\n                <div class=\"text-center\">\r\n                  <div>\r\n                    <a routerLink=\"\" class=\"logo-full\"><img src=\"assets/images/logo-full.png\" alt=\"logo-full\"></a>\r\n                  </div>\r\n\r\n                  <h4 class=\"font-20 mt-4\">Lock screen</h4>\r\n                  <p class=\"text-muted\">Enter valid password to unlock the screen.</p>\r\n                </div>\r\n                <div class=\"login100-form-logo\">\r\n                  <div class=\"image\">\r\n                    <img src=\"assets/images/usrbig.jpg\" alt=\"User\">\r\n                  </div>\r\n                </div>\r\n                <span class=\"auth-user-name font-20 p-b-10 p-t-27\">\r\n                  Emily Smith\r\n                </span>\r\n                <div class=\"text-center\">\r\n                  <p class=\"txt1\">\r\n                    Locked\r\n                  </p>\r\n                </div>\r\n                <div class=\"p-2 mt-5\">\r\n                  <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\r\n                    <div class=\"form-group auth-form-group mb-4\">\r\n                      <mat-icon matSuffix class=\"auth-page-icon\">vpn_key</mat-icon>\r\n                      <input type=\"password\" formControlName=\"password\" class=\"form-control auth-control\" id=\"password\"\r\n                        [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\" placeholder=\"Password\" />\r\n                      <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\r\n                        <div *ngIf=\"f.password.errors.required\">Password is required</div>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"flex-sb-m w-full p-t-15 p-b-20\">\r\n                      <div class=\"form-check\">\r\n                        <label class=\"form-check-label\">\r\n                          <input class=\"form-check-input\" type=\"checkbox\" value=\"\"> Remember me\r\n                          <span class=\"form-check-sign\">\r\n                            <span class=\"check\"></span>\r\n                          </span>\r\n                        </label>\r\n                      </div>\r\n                      <div>\r\n                        <a class=\"txt1\" routerLink=\"/authentication/forgot-password\">Forgot Password?</a>\r\n                      </div>\r\n                    </div>\r\n\r\n                    <div class=\"container-login100-form-btn\">\r\n                      <button mat-flat-button color=\"primary\" class=\"login100-form-btn\" type=\"submit\">\r\n                        Login\r\n                      </button>\r\n                    </div>\r\n\r\n                  </form>\r\n                </div>\r\n\r\n                <div class=\"mt-5 text-center\">\r\n                  <p>Don't have an account ? <a routerLink=\"/authentication/signup\"\r\n                      class=\"font-weight-medium text-primary\">\r\n                      Register </a> </p>\r\n                  <div class=\"login100-form-social flex-c-m\">\r\n                    <a href=\"#\" class=\"login100-form-social-item flex-c-m bg1 m-r-5\">\r\n                      <i class=\"fab fa-facebook-f\"></i>\r\n                    </a>\r\n                    <a href=\"#\" class=\"login100-form-social-item flex-c-m bg2 m-r-5\">\r\n                      <i class=\"fab fa-twitter\"></i>\r\n                    </a>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-lg-8\">\r\n      <div class=\"auth-bg\">\r\n        <div class=\"bg-overlay\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n";

/***/ }),

/***/ 2497:
/*!**************************************************************************!*\
  !*** ./src/app/authentication/page500/page500.component.html?ngResource ***!
  \**************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"limiter\">\r\n  <div class=\"container-login100\">\r\n    <div class=\"wrap-login100\">\r\n      <form class=\"login100-form\">\r\n        <span class=\"error-header p-b-45\">\r\n          500\r\n        </span>\r\n        <span class=\"error-subheader2 p-b-5\">\r\n          Oops, Something went wrong. Please try after some times.\r\n        </span>\r\n        <div class=\"container-login100-form-btn p-t-30\t\">\r\n          <button mat-flat-button color=\"primary\" class=\"login100-form-btn\" (click)='submit()'>\r\n            Go To Home Page\r\n          </button>\r\n        </div>\r\n        <div class=\"w-full p-t-15 p-b-15 text-center\">\r\n          <div>\r\n            <a href=\"#\" class=\"txt1\">\r\n              Need Help?\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </form>\r\n      <div class=\"login100-more\" style=\"background-image: url('assets/images/pages/bg-03.png');\">\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n";

/***/ }),

/***/ 510:
/*!****************************************************************************************!*\
  !*** ./src/app/authentication/reset-password/reset-password.component.html?ngResource ***!
  \****************************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"container-fluid p-0\">\r\n    <div class=\"row g-0\">\r\n        <div class=\"col-lg-4\">\r\n            <div class=\"auth-content p-4 d-flex align-items-center min-vh-100\">\r\n                <div class=\"w-100\">\r\n                    <div class=\"row justify-content-center\">\r\n                        <div class=\"col-lg-9\">\r\n                            <div>\r\n                                <div class=\"text-center\">\r\n                                    <div>\r\n                                        <a routerLink=\"\" class=\"logo-full\"><img src=\"assets/images/rhythmflows_logo.png\"\r\n                                                alt=\"logo-full\"></a>\r\n                                    </div>\r\n\r\n                                    <h4 class=\"font-20 mt-4\">Change Password</h4>\r\n                                </div>\r\n\r\n                                <div class=\"p-2 mt-5\">\r\n                                    <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\r\n                                        <div class=\"form-group auth-form-group mb-4\">\r\n                                            <mat-icon matSuffix class=\"auth-page-icon\">vpn_key</mat-icon>\r\n                                            <input type=\"password\" formControlName=\"old_password\"\r\n                                                class=\"form-control auth-control\" id=\"old_password\"\r\n                                                [ngClass]=\"{ 'is-invalid': submitted && f.old_password.errors }\"\r\n                                                placeholder=\"Enter Old Password\" />\r\n                                            <div *ngIf=\"submitted && f.old_password.errors\" class=\"invalid-feedback\">\r\n                                                <div *ngIf=\"f.old_password.errors.required\">Old Password is required</div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group auth-form-group mb-4\">\r\n                                            <mat-icon matSuffix class=\"auth-page-icon\">vpn_key</mat-icon>\r\n                                            <input type=\"password\" formControlName=\"password\"\r\n                                                class=\"form-control auth-control\" id=\"password\"\r\n                                                [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\"\r\n                                                placeholder=\"Enter New Password\" (change)=\"checkPassword2()\" />\r\n                                            <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\r\n                                                <div *ngIf=\"f.password.errors.required\">Password is required</div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <div class=\"form-group auth-form-group mb-4\">\r\n                                            <mat-icon matSuffix class=\"auth-page-icon\">vpn_key</mat-icon>\r\n                                            <input type=\"password\" formControlName=\"confirm_password\"\r\n                                                class=\"form-control auth-control\" id=\"confirm_password\"\r\n                                                [ngClass]=\"{ 'is-invalid': submitted && f.confirm_password.errors }\"\r\n                                                placeholder=\"Confirm Password\" (change)=\"checkPassword()\" />\r\n                                            <div *ngIf=\"submitted && f.confirm_password.errors\" class=\"invalid-feedback\">\r\n                                                <div *ngIf=\"f.confirm_password.errors.required\">Confirm password is required\r\n                                                </div>\r\n                                            </div>\r\n                                        </div>\r\n\r\n                                        <div class=\"container-login100-form-btn p-t-15 p-b-20\">\r\n                                            <button mat-flat-button style=\"background-color: #2e3634;\"\r\n                                                class=\"login100-form-btn\" type=\"submit\" [disabled]=\"submitted\">\r\n                                                Change Password\r\n                                            </button>\r\n                                        </div>\r\n\r\n                                    </form>\r\n                                </div>\r\n                            </div>\r\n\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"col-lg-8\">\r\n            <div class=\"auth-bg\">\r\n                <div class=\"bg-overlay\"></div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>";

/***/ }),

/***/ 8610:
/*!************************************************************************!*\
  !*** ./src/app/authentication/signin/signin.component.html?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"container-fluid p-0\">\r\n  <div class=\"row g-0\">\r\n    <div class=\"col-lg-4\">\r\n      <div class=\"auth-content p-4 d-flex align-items-center min-vh-100\">\r\n        <div class=\"w-100\">\r\n          <div class=\"row justify-content-center\">\r\n            <div class=\"col-lg-9\">\r\n              <div>\r\n                <div class=\"text-center\">\r\n                  <div>\r\n                    <a routerLink=\"\" class=\"logo-full\"><img src=\"assets/images/rhythmflows_logo.png\" alt=\"logo-full\"></a>\r\n                  </div>\r\n                </div>\r\n                <div class=\"p-2 mt-5\">\r\n                  <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\r\n                    <div *ngIf=\"error\" class=\"alert alert-danger mb-3 mt-0\">{{error}}</div>\r\n                    <div class=\"form-group auth-form-group mb-4\">\r\n                      <mat-label>Username</mat-label>\r\n                      <mat-form-field class=\"example-full-width\" appearance=\"outline\">                          \r\n                        <input type=\"number\" matInput formControlName=\"user_number\" id=\"username\" placeholder=\"Username\" required>\r\n                        <mat-icon matSuffix>face</mat-icon>\r\n                        <mat-error *ngIf=\"loginForm.get('user_number').hasError('required')\">\r\n                          Username is required\r\n                        </mat-error>\r\n                      </mat-form-field>\r\n                    </div>\r\n                    <div class=\"form-group auth-form-group mb-4\">\r\n                      <mat-label>Password</mat-label>\r\n                      <mat-form-field class=\"example-full-width\" appearance=\"outline\">                          \r\n                        <input matInput formControlName=\"password\" id=\"password\" placeholder=\"Password\" \r\n                          [type]=\"hide ? 'password' : 'text'\" required>\r\n                        <mat-icon matSuffix (click)=\"hide = !hide\">\r\n                          {{hide ? 'visibility_off' : 'visibility'}}</mat-icon>\r\n                        <mat-error *ngIf=\"loginForm.get('password').hasError('required')\">\r\n                          Password is required\r\n                        </mat-error>\r\n                      </mat-form-field>\r\n                    </div>\r\n\r\n                    <div class=\"container-login100-form-btn\">\r\n                      <button mat-flat-button color=\"primary\" [disabled]=\"submitted\" class=\"login100-form-btn\" type=\"submit\">\r\n                        Login\r\n                        <i class=\"fa fa-spinner fa-spin\" *ngIf=\"submitted\"></i>\r\n                      </button>\r\n                    </div>\r\n                  </form>\r\n                </div>\r\n                <div class=\"mt-5 text-center\">\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n    <div class=\"col-lg-8\">\r\n      <div class=\"auth-bg\">\r\n        <div class=\"bg-overlay\"></div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n";

/***/ }),

/***/ 957:
/*!************************************************************************!*\
  !*** ./src/app/authentication/signup/signup.component.html?ngResource ***!
  \************************************************************************/
/***/ ((module) => {

module.exports = "  <div class=\"container-fluid p-0\">\r\n    <div class=\"row g-0\">\r\n      <div class=\"col-lg-4\">\r\n        <div class=\"auth-content p-4 d-flex align-items-center min-vh-100\">\r\n          <div class=\"w-100\">\r\n            <div class=\"row justify-content-center\">\r\n              <div class=\"col-lg-9\">\r\n                <div>\r\n                  <div class=\"text-center\">\r\n                    <div>\r\n                      <a routerLink=\"\" class=\"logo-full\"><img src=\"assets/images/logo-full.png\" alt=\"logo-full\"></a>\r\n                    </div>\r\n                    <h4 class=\"font-20 mt-4\">Register New User</h4>\r\n                    <p class=\"text-muted\">Lifetime Free Account.</p>\r\n                  </div>\r\n                  <div class=\"p-2 mt-5\">\r\n                    <form [formGroup]=\"loginForm\" (ngSubmit)=\"onSubmit()\">\r\n                      <div class=\"form-group auth-form-group mb-4\">\r\n                        <mat-icon matSuffix class=\"auth-page-icon\">face</mat-icon>\r\n                        <input type=\"text\" formControlName=\"username\" class=\"form-control auth-control\" id=\"username\"\r\n                          placeholder=\"Username\" [ngClass]=\"{ 'is-invalid': submitted && f.username.errors }\">\r\n                        <div *ngIf=\"submitted && f.username.errors\" class=\"invalid-feedback\">\r\n                          <div *ngIf=\"f.username.errors.required\">Username is required</div>\r\n                        </div>\r\n                      </div>\r\n                      <div class=\"form-group auth-form-group mb-4\">\r\n                        <mat-icon matSuffix class=\"auth-page-icon\">email</mat-icon>\r\n                        <input type=\"email\" formControlName=\"email\" class=\"form-control auth-control\" id=\"email\"\r\n                          placeholder=\"Email\" [ngClass]=\"{ 'is-invalid': submitted && f.email.errors }\">\r\n                        <div *ngIf=\"submitted && f.email.errors\" class=\"invalid-feedback\">\r\n                          <div *ngIf=\"f.email.errors.required\">Email is required</div>\r\n                          <div *ngIf=\"f.email.errors.email\">Email must be a valid email address</div>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"form-group auth-form-group mb-4\">\r\n                        <mat-icon matSuffix class=\"auth-page-icon\">vpn_key</mat-icon>\r\n                        <input type=\"password\" formControlName=\"password\" class=\"form-control auth-control\"\r\n                          id=\"password\" [ngClass]=\"{ 'is-invalid': submitted && f.password.errors }\"\r\n                          placeholder=\"Password\" />\r\n                        <div *ngIf=\"submitted && f.password.errors\" class=\"invalid-feedback\">\r\n                          <div *ngIf=\"f.password.errors.required\">Password is required</div>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"form-group auth-form-group mb-4\">\r\n                        <mat-icon matSuffix class=\"auth-page-icon\">vpn_key</mat-icon>\r\n                        <input type=\"password\" formControlName=\"cpassword\" class=\"form-control auth-control\"\r\n                          id=\"cpassword\" [ngClass]=\"{ 'is-invalid': submitted && f.cpassword.errors }\"\r\n                          placeholder=\"Confirm Password\" />\r\n                        <div *ngIf=\"submitted && f.cpassword.errors\" class=\"invalid-feedback\">\r\n                          <div *ngIf=\"f.cpassword.errors.required\">Confirm Password is required</div>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"flex-sb-m w-full p-t-15 p-b-20\">\r\n                        <div>\r\n                          <span>Already Registered? <a routerLink=\"/authentication/signin\">\r\n                              Login\r\n                            </a></span>\r\n                        </div>\r\n                      </div>\r\n\r\n                      <div class=\"container-login100-form-btn\">\r\n                        <button mat-flat-button color=\"primary\" class=\"login100-form-btn\" type=\"submit\">\r\n                          Register\r\n                        </button>\r\n                      </div>\r\n                    </form>\r\n                  </div>\r\n                  <div class=\"mt-3 text-center\">\r\n                    <p> or sign up using </p>\r\n                    <ul class=\"list-unstyled social-icon mb-0 mt-3\">\r\n                      <li class=\"list-inline-item m-2\"><a href=\"javascript:void(0)\" class=\"rounded\">\r\n                          <i class=\"fab fa-google\"></i>\r\n                        </a></li>\r\n                      <li class=\"list-inline-item m-2\"><a href=\"javascript:void(0)\" class=\"rounded flex-c-m\">\r\n                          <i class=\"fab fa-facebook-f\"></i>\r\n                        </a></li>\r\n                      <li class=\"list-inline-item m-2\"><a href=\"javascript:void(0)\" class=\"rounded\">\r\n                          <i class=\"fab fa-twitter\"></i>\r\n                        </a></li>\r\n                      <li class=\"list-inline-item m-2\"><a href=\"javascript:void(0)\" class=\"rounded\">\r\n                          <i class=\"fab fa-linkedin-in\"></i>\r\n                        </a></li>\r\n                    </ul>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"col-lg-8\">\r\n        <div class=\"auth-bg\">\r\n          <div class=\"bg-overlay\"></div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n";

/***/ })

}]);
//# sourceMappingURL=src_app_authentication_authentication_module_ts.js.map