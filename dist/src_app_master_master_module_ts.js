"use strict";
(self["webpackChunkatrio"] = self["webpackChunkatrio"] || []).push([["src_app_master_master_module_ts"],{

/***/ 6933:
/*!*****************************************************************************************************!*\
  !*** ./src/app/master/category-define/category-define-details/category-define-details.component.ts ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CategoryDefineDetailsComponent": () => (/* binding */ CategoryDefineDetailsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _category_define_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-define-details.component.html?ngResource */ 4464);
/* harmony import */ var _category_define_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./category-define-details.component.sass?ngResource */ 9367);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/master.service */ 6754);
/* harmony import */ var src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/common-setup.service */ 6491);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);








let CategoryDefineDetailsComponent = class CategoryDefineDetailsComponent {
    constructor(MasterService, formBuilder, commonsetupservice) {
        this.MasterService = MasterService;
        this.formBuilder = formBuilder;
        this.commonsetupservice = commonsetupservice;
        this.onSave = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
        this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_5__.EventEmitter();
        this.summaryType = [];
        this.summaryTypeData = [];
        this.summaryParentData = [];
        this.screenName = 'Category Define';
        this.BTN_VAL = 'Submit';
        this.editdisabled = false;
    }
    ngOnInit() {
        this.CategoryDefineForm = this.formBuilder.group({
            initialItemRow1: this.formBuilder.array([this.initialitemRow()]),
        });
        this.commonsetupservice.getAllMasterData("Summary Type").subscribe((data) => {
            this.summaryTypeData = data;
            if (!Array.isArray(this.rowData)) {
                this.viewRecord(this.rowData);
            }
        });
        this.commonsetupservice.getAllMasterData("Product Type").subscribe((data) => {
            this.summaryType = data;
        });
    }
    viewRecord(row) {
        if (this.submitBtn === true) {
            this.BTN_VAL = "Update";
            this.editdisabled = true;
        }
        this.MasterService.getAllCategoryDefine(row.summary_product_type_id).subscribe((data) => {
            this.CategoryDefineForm.setControl("initialItemRow1", this.setExistingArray(data));
        });
    }
    setExistingArray(initialArray = []) {
        const formArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormArray([]);
        initialArray.forEach((element, ind) => {
            let parent = null;
            if (element.summary_parent_ref_id_id != null) {
                parent = initialArray.filter(x => x.id === element.summary_parent_ref_id_id)[0]['summary_name'];
            }
            this.summaryParentData.push({ id: ind, parent_name: element.summary_name });
            formArray.push(this.formBuilder.group({
                summary_product_type: element.summary_product_type_id,
                summary_name: element.summary_name,
                summary_type_ref_id: element.summary_type_ref_id_id,
                summary_parent_ref_id: parent,
                id: element.id,
                is_linked: element.is_linked,
                is_active: element.is_active,
                is_deleted: element.is_deleted
            }));
            if (element.is_linked) {
                formArray.at(ind).get('summary_product_type').disable();
                formArray.at(ind).get('summary_type_ref_id').disable();
                formArray.at(ind).get('summary_parent_ref_id').disable();
                formArray.at(ind).get('is_linked').disable();
            }
            else if (parent == null) {
                formArray.at(ind).get('summary_parent_ref_id').disable();
            }
        });
        return formArray;
    }
    initialitemRow() {
        return this.formBuilder.group({
            id: [],
            summary_product_type: [""],
            summary_name: [""],
            summary_parent_ref_id: [],
            summary_type_ref_id: [],
            index: [],
            is_linked: [false],
            is_active: [true],
            is_deleted: [false]
        });
    }
    get formArray() {
        return this.CategoryDefineForm.get("initialItemRow1");
    }
    addNewRow() {
        this.formArray.push(this.initialitemRow());
    }
    deleteRow(index) {
        if (this.formArray.length == 1) {
            return false;
        }
        else {
            if (this.editdisabled && this.formArray.at(index).get('is_linked').value) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
                    title: 'You can not remove....',
                    text: 'It is linked with another!',
                    icon: 'warning',
                    timer: 2000,
                    showConfirmButton: false
                });
            }
            else {
                let summary_name = this.formArray.at(index).get('summary_name').value;
                let sum_ind = this.summaryParentData.findIndex(x => x.parent_name === summary_name && x.id === index);
                if (sum_ind >= 0) {
                    this.summaryParentData.splice(sum_ind);
                }
                this.formArray.removeAt(index);
                return true;
            }
        }
    }
    productTypeChange(event, ind) {
        if (this.editdisabled) {
            let exists = this.formArray.value.find((x, i) => x.summary_product_type == event && i != ind);
            if (exists == undefined) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
                    title: 'Different Category!',
                    text: 'Please select same category!',
                    icon: 'warning',
                    timer: 2000,
                    showConfirmButton: false
                });
                this.formArray.at(ind).get('summary_product_type').reset();
            }
        }
    }
    summaryName(summary_name, event, i) {
        if (summary_name != null && event != null) {
            let category = this.summaryTypeData.filter(x => x.id === event);
            if (category[0]['master_value'] == 'Category') {
                this.formArray.at(i).get('summary_parent_ref_id').disable();
            }
            let index = this.summaryParentData.findIndex(x => x.id == i);
            let summary = this.summaryParentData.findIndex(x => x.parent_name == summary_name);
            if (index < 0 && summary < 0) {
                this.summaryParentData.push({ id: i, parent_name: summary_name });
                this.formArray.at(i).get('index').setValue(i);
            }
            else {
                if (index >= 0) {
                    this.summaryParentData[index]['parent_name'] = summary_name;
                    let ind = this.formArray.value.find(x => x.index == i);
                }
            }
        }
    }
    onCancelForm() {
        this.CategoryDefineForm.reset();
        this.onCancel.emit(false);
    }
    onSubmit() {
        if (this.CategoryDefineForm.invalid) {
            return;
        }
        else {
            for (let i = 0; i < this.CategoryDefineForm.value.initialItemRow1.length; i++) {
                if (this.formArray.at(i).get('summary_product_type').disabled)
                    this.formArray.at(i).get('summary_product_type').enable();
                if (this.formArray.at(i).get('summary_type_ref_id').disabled)
                    this.formArray.at(i).get('summary_type_ref_id').enable();
                if (this.formArray.at(i).get('summary_parent_ref_id').disabled)
                    this.formArray.at(i).get('summary_parent_ref_id').enable();
                if (this.formArray.at(i).get('is_linked').disabled)
                    this.formArray.at(i).get('is_linked').enable();
            }
            this.onSave.emit(this.CategoryDefineForm.value);
        }
    }
};
CategoryDefineDetailsComponent.ctorParameters = () => [
    { type: src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__.MasterService },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_3__.CommonSetupService }
];
CategoryDefineDetailsComponent.propDecorators = {
    rowData: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input }],
    submitBtn: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Input }],
    onSave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output }],
    onCancel: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_5__.Output }]
};
CategoryDefineDetailsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_7__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.Component)({
        selector: 'app-category-define-details',
        template: _category_define_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_category_define_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], CategoryDefineDetailsComponent);



/***/ }),

/***/ 3766:
/*!*********************************************************************!*\
  !*** ./src/app/master/category-define/category-define.component.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CategoryDefineComponent": () => (/* binding */ CategoryDefineComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _category_define_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./category-define.component.html?ngResource */ 5110);
/* harmony import */ var _category_define_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./category-define.component.sass?ngResource */ 7098);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/sort */ 4316);
/* harmony import */ var src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/master.service */ 6754);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);










let CategoryDefineComponent = class CategoryDefineComponent {
    constructor(MasterService, manageSecurity) {
        this.MasterService = MasterService;
        this.manageSecurity = manageSecurity;
        this.screenName = 'Category Define';
        this.BTN_VAL = 'Submit';
        this.submitBtn = true;
        this.listDiv = false;
        this.showList = true;
        this.rowData = [];
        this.displayColumns = [
            "actions",
            "product_type"
        ];
    }
    ngOnInit() {
        this.getAllCategoryDefine();
        this.USERID = localStorage.getItem('user_id');
        this.manageSecurity.getAccessLeftPanel(this.USERID, 'Tenant Hierarchy Define').subscribe(data => {
            this.sidebarData = data;
        });
    }
    getAllCategoryDefine() {
        this.MasterService.getAllCategoryList().subscribe((data) => {
            this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_5__.MatTableDataSource(data);
            this.dataSource.paginator = this.dataSourcePaginator;
            this.dataSource.sort = this.sort;
        });
    }
    showSwalMassage(massage, icon) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
            title: massage,
            icon: icon,
            timer: 2000,
            showConfirmButton: false,
        });
    }
    viewRecord(row) {
        this.rowData = row;
        this.showList = false;
        this.submitBtn = false;
        this.listDiv = true;
    }
    editRecord(row) {
        this.rowData = row;
        this.showList = false;
        this.listDiv = true;
        this.submitBtn = true;
        this.BTN_VAL = "Update";
    }
    showFormList(item) {
        this.rowData = [];
        this.BTN_VAL = 'Submit';
        if (item === false) {
            this.listDiv = true;
            this.showList = false;
            this.submitBtn = true;
        }
        else {
            this.listDiv = false;
            this.showList = true;
        }
    }
    onCancel(item) {
        this.listDiv = item;
        this.showList = true;
        this.rowData = [];
        this.BTN_VAL = 'Submit';
    }
    tbl_FilterDatatable(value) {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    }
    onSave(formvalue) {
        this.MasterService.createCategoryDefine(formvalue, this.BTN_VAL).subscribe((data) => {
            if (data["status"] == 1) {
                this.showSwalMassage("Your record has been updated successfully!", "success");
            }
            else if (data["status"] == 2) {
                this.showSwalMassage("Your record has been added successfully!", "success");
            }
            this.showList = true;
            this.listDiv = false;
            this.rowData = [];
            this.BTN_VAL = "Submit";
            this.getAllCategoryDefine();
        }, (error) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire(error);
        });
    }
};
CategoryDefineComponent.ctorParameters = () => [
    { type: src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__.MasterService },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__.ManageSecurityService }
];
CategoryDefineComponent.propDecorators = {
    dataSourcePaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChild, args: ["dataSourcePaginator", { read: _angular_material_paginator__WEBPACK_IMPORTED_MODULE_7__.MatPaginator },] }],
    sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChild, args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_8__.MatSort, { static: true },] }]
};
CategoryDefineComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-category-define',
        template: _category_define_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_category_define_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], CategoryDefineComponent);



/***/ }),

/***/ 6353:
/*!***********************************************************************************************************************!*\
  !*** ./src/app/master/file-structure-define/file-structure-define-details/file-structure-define-details.component.ts ***!
  \***********************************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FileStructureDefineDetailsComponent": () => (/* binding */ FileStructureDefineDetailsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _file_structure_define_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./file-structure-define-details.component.html?ngResource */ 772);
/* harmony import */ var _file_structure_define_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./file-structure-define-details.component.sass?ngResource */ 1015);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/core/service/common-setup.service */ 6491);






let FileStructureDefineDetailsComponent = class FileStructureDefineDetailsComponent {
    constructor(formBuilder, commonsetupservice) {
        this.formBuilder = formBuilder;
        this.commonsetupservice = commonsetupservice;
        this.onSave = new _angular_core__WEBPACK_IMPORTED_MODULE_3__.EventEmitter();
        this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_3__.EventEmitter();
        this.BTN_VAL = "Submit";
        this.screenName = "File Structure Define";
        this.fileTypeData = [];
        this.ReferFromData = [];
        this.DatatypeData = [];
    }
    ngOnInit() {
        this.FileStructureDefineForm = this.formBuilder.group({
            id: [],
            file_name: ["", [_angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required]],
            file_type_ref_id: ['', [_angular_forms__WEBPACK_IMPORTED_MODULE_4__.Validators.required]],
            is_temporary_flag: true,
            is_active: true,
            is_deleted: false,
            initialItemRow1: this.formBuilder.array([this.initialitemRow()]),
        });
        if (!Array.isArray(this.rowData)) {
            this.viewRecord(this.rowData);
        }
        this.ReferFromData = ['tbl_loan_shadow_staging', 'tbl_deposit_shadow_staging', 'tbl_trial_balance_staging'];
        this.commonsetupservice.getAllMasterData("File Type").subscribe((data) => {
            this.fileTypeData = data;
        });
        this.commonsetupservice.getAllMasterData("Data Type").subscribe((data) => {
            this.DatatypeData = data;
        });
    }
    initialitemRow() {
        return this.formBuilder.group({
            id: [],
            actual_field_name: [],
            field_name: [],
            data_type: [],
            field_length: [],
            refer_from: [],
            is_active: [true],
            is_deleted: [false]
        });
    }
    get formArray() {
        return this.FileStructureDefineForm.get("initialItemRow1");
    }
    addNewRow() {
        this.formArray.push(this.initialitemRow());
    }
    deleteRow(index) {
        if (this.formArray.length == 1) {
            return false;
        }
        else {
            this.formArray.removeAt(index);
            return true;
        }
    }
    viewRecord(row) {
        this.FileStructureDefineForm.patchValue({
            file_name: row.file_name,
            file_type_ref_id: row.file_type_ref_id,
            is_temporary_flag: row.is_temporary_flag,
            id: row.id,
            is_active: row.is_active,
            is_deleted: row.is_deleted
        });
        if (this.submitBtn === true)
            this.BTN_VAL = "Update";
        this.FileStructureDefineForm.setControl("initialItemRow1", this.setExistingArray(row.initialItemRow1));
    }
    setExistingArray(initialArray = []) {
        const formArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormArray([]);
        initialArray.forEach((element) => {
            formArray.push(this.formBuilder.group({
                actual_field_name: element.actual_field_name,
                field_name: element.field_name,
                field_length: element.field_length,
                data_type: element.data_type,
                refer_from: element.refer_from,
                id: element.id,
                is_active: element.is_active,
                is_deleted: element.is_deleted
            }));
        });
        return formArray;
    }
    changeState(event) {
        if (!event.checked) {
            this.FileStructureDefineForm.get('is_deleted').setValue(true);
        }
        else {
            this.FileStructureDefineForm.get('is_deleted').setValue(false);
        }
    }
    onCancelForm() {
        this.FileStructureDefineForm.reset();
        this.onCancel.emit(false);
    }
    onSubmit() {
        if (this.FileStructureDefineForm.invalid) {
            return;
        }
        else {
            this.onSave.emit(this.FileStructureDefineForm.value);
        }
    }
};
FileStructureDefineDetailsComponent.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormBuilder },
    { type: src_app_core_service_common_setup_service__WEBPACK_IMPORTED_MODULE_2__.CommonSetupService }
];
FileStructureDefineDetailsComponent.propDecorators = {
    rowData: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    submitBtn: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    onSave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Output }],
    onCancel: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Output }]
};
FileStructureDefineDetailsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-file-structure-define-details',
        template: _file_structure_define_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_file_structure_define_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], FileStructureDefineDetailsComponent);



/***/ }),

/***/ 1391:
/*!*********************************************************************************!*\
  !*** ./src/app/master/file-structure-define/file-structure-define.component.ts ***!
  \*********************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "FileStructureDefineComponent": () => (/* binding */ FileStructureDefineComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _file_structure_define_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./file-structure-define.component.html?ngResource */ 2953);
/* harmony import */ var _file_structure_define_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./file-structure-define.component.sass?ngResource */ 8403);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/sort */ 4316);
/* harmony import */ var src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/master.service */ 6754);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! xlsx */ 4126);











let FileStructureDefineComponent = class FileStructureDefineComponent {
    constructor(MasterService, manageSecurity) {
        this.MasterService = MasterService;
        this.manageSecurity = manageSecurity;
        this.screenName = "File Structure Define";
        this.BTN_VAL = "Submit";
        this.submitBtn = true;
        this.listDiv = false;
        this.showList = true;
        this.rowData = [];
        this.displayColumns = [
            "actions",
            "file_name",
            "file_type_ref_id",
            "is_temporary_flag",
        ];
    }
    ngOnInit() {
        this.getAllFilestructureDefine();
        this.USERID = localStorage.getItem('user_id');
        this.manageSecurity.getAccessLeftPanel(this.USERID, 'Tenant Hierarchy Define').subscribe(data => {
            this.sidebarData = data;
        });
    }
    getAllFilestructureDefine() {
        this.MasterService.getAllFilestructureDefine().subscribe((data) => {
            this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_5__.MatTableDataSource(data);
            this.dataSource.paginator = this.dataSourcePaginator;
            this.dataSource.sort = this.sort;
            this.dataSource.filteredData.forEach(ele => {
                ele.initialItemRow1.forEach((e, i) => {
                    if (!e.is_active && e.is_deleted) {
                        ele.initialItemRow1.splice(i, 1);
                    }
                });
            });
        });
    }
    viewRecord(row) {
        this.rowData = row;
        this.showList = false;
        this.submitBtn = false;
        this.listDiv = true;
    }
    editRecord(row) {
        this.rowData = row;
        this.showList = false;
        this.listDiv = true;
        this.submitBtn = true;
        this.BTN_VAL = "Update";
    }
    showFormList(item) {
        this.rowData = [];
        this.BTN_VAL = 'Submit';
        if (item === false) {
            this.listDiv = true;
            this.showList = false;
            this.submitBtn = true;
        }
        else {
            this.listDiv = false;
            this.showList = true;
        }
    }
    onCancel(item) {
        this.listDiv = item;
        this.showList = true;
        this.rowData = [];
        this.BTN_VAL = 'Submit';
    }
    showSwalMassage(massage, icon) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
            title: massage,
            icon: icon,
            timer: 2000,
            showConfirmButton: false,
        });
    }
    onSave(formValue) {
        this.MasterService.createFileStructureDefine(formValue).subscribe((data) => {
            if (data["status"] == 1) {
                this.showSwalMassage("Your record has been updated successfully!", "success");
            }
            else if (data["status"] == 2) {
                this.showSwalMassage("Your record has been added successfully!", "success");
            }
            this.showList = true;
            this.listDiv = false;
            this.rowData = [];
            this.BTN_VAL = "Submit";
            this.getAllFilestructureDefine();
        }, (error) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire(error);
        });
    }
    tbl_FilterDatatable(value) {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    }
    export_to_excel() {
        var XlsMasterData = this.dataSource.data.map(({ file_name, file_type_name, is_temporary_flag }) => ({ file_name, file_type_name, is_temporary_flag }));
        const workSheet = xlsx__WEBPACK_IMPORTED_MODULE_6__.utils.json_to_sheet(XlsMasterData);
        const workBook = xlsx__WEBPACK_IMPORTED_MODULE_6__.utils.book_new();
        xlsx__WEBPACK_IMPORTED_MODULE_6__.utils.book_append_sheet(workBook, workSheet, 'SheetName');
        xlsx__WEBPACK_IMPORTED_MODULE_6__.writeFile(workBook, 'File Structure Define Data.xlsx');
    }
};
FileStructureDefineComponent.ctorParameters = () => [
    { type: src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__.MasterService },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__.ManageSecurityService }
];
FileStructureDefineComponent.propDecorators = {
    dataSourcePaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChild, args: ["dataSourcePaginator", { read: _angular_material_paginator__WEBPACK_IMPORTED_MODULE_8__.MatPaginator },] }],
    sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_7__.ViewChild, args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_9__.MatSort, { static: true },] }]
};
FileStructureDefineComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_10__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_7__.Component)({
        selector: 'app-file-structure-define',
        template: _file_structure_define_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_file_structure_define_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], FileStructureDefineComponent);



/***/ }),

/***/ 2070:
/*!*************************************************!*\
  !*** ./src/app/master/master-routing.module.ts ***!
  \*************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MasterRoutingModule": () => (/* binding */ MasterRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _file_structure_define_file_structure_define_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./file-structure-define/file-structure-define.component */ 1391);
/* harmony import */ var _category_define_category_define_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./category-define/category-define.component */ 3766);
/* harmony import */ var _product_mapping_product_mapping_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./product-mapping/product-mapping.component */ 8248);






const routes = [
    {
        path: 'file-structure-define',
        component: _file_structure_define_file_structure_define_component__WEBPACK_IMPORTED_MODULE_0__.FileStructureDefineComponent,
    },
    {
        path: 'category-define',
        component: _category_define_category_define_component__WEBPACK_IMPORTED_MODULE_1__.CategoryDefineComponent,
    },
    {
        path: 'product-mapping',
        component: _product_mapping_product_mapping_component__WEBPACK_IMPORTED_MODULE_2__.ProductMappingComponent,
    },
];
let MasterRoutingModule = class MasterRoutingModule {
};
MasterRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
    })
], MasterRoutingModule);



/***/ }),

/***/ 9992:
/*!*****************************************!*\
  !*** ./src/app/master/master.module.ts ***!
  \*****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "MasterModule": () => (/* binding */ MasterModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _shared_components_components_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../shared/components/components.module */ 5626);
/* harmony import */ var _master_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./master-routing.module */ 2070);
/* harmony import */ var _file_structure_define_file_structure_define_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./file-structure-define/file-structure-define.component */ 1391);
/* harmony import */ var _file_structure_define_file_structure_define_details_file_structure_define_details_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./file-structure-define/file-structure-define-details/file-structure-define-details.component */ 6353);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/form-field */ 9076);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @angular/material/input */ 3365);
/* harmony import */ var _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @angular/material/snack-bar */ 2528);
/* harmony import */ var _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @angular/material/icon */ 5590);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! @angular/material/select */ 1434);
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! @angular/material/slide-toggle */ 6623);
/* harmony import */ var _category_define_category_define_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./category-define/category-define.component */ 3766);
/* harmony import */ var _product_mapping_product_mapping_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./product-mapping/product-mapping.component */ 8248);
/* harmony import */ var _category_define_category_define_details_category_define_details_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./category-define/category-define-details/category-define-details.component */ 6933);
/* harmony import */ var _product_mapping_product_mapping_details_product_mapping_details_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./product-mapping/product-mapping-details/product-mapping-details.component */ 2829);




















let MasterModule = class MasterModule {
};
MasterModule = (0,tslib__WEBPACK_IMPORTED_MODULE_8__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.NgModule)({
        declarations: [
            _file_structure_define_file_structure_define_component__WEBPACK_IMPORTED_MODULE_2__.FileStructureDefineComponent,
            _file_structure_define_file_structure_define_details_file_structure_define_details_component__WEBPACK_IMPORTED_MODULE_3__.FileStructureDefineDetailsComponent,
            _category_define_category_define_component__WEBPACK_IMPORTED_MODULE_4__.CategoryDefineComponent,
            _product_mapping_product_mapping_component__WEBPACK_IMPORTED_MODULE_5__.ProductMappingComponent,
            _category_define_category_define_details_category_define_details_component__WEBPACK_IMPORTED_MODULE_6__.CategoryDefineDetailsComponent,
            _product_mapping_product_mapping_details_product_mapping_details_component__WEBPACK_IMPORTED_MODULE_7__.ProductMappingDetailsComponent
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_10__.CommonModule,
            _master_routing_module__WEBPACK_IMPORTED_MODULE_1__.MasterRoutingModule,
            _shared_components_components_module__WEBPACK_IMPORTED_MODULE_0__.ComponentsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_11__.FormsModule, _angular_forms__WEBPACK_IMPORTED_MODULE_11__.ReactiveFormsModule,
            _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__.MatPaginatorModule,
            _angular_material_table__WEBPACK_IMPORTED_MODULE_13__.MatTableModule,
            _angular_material_form_field__WEBPACK_IMPORTED_MODULE_14__.MatFormFieldModule,
            _angular_material_input__WEBPACK_IMPORTED_MODULE_15__.MatInputModule,
            _angular_material_snack_bar__WEBPACK_IMPORTED_MODULE_16__.MatSnackBarModule,
            _angular_material_icon__WEBPACK_IMPORTED_MODULE_17__.MatIconModule,
            _angular_material_select__WEBPACK_IMPORTED_MODULE_18__.MatSelectModule,
            _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_19__.MatSlideToggleModule
        ]
    })
], MasterModule);



/***/ }),

/***/ 2829:
/*!*****************************************************************************************************!*\
  !*** ./src/app/master/product-mapping/product-mapping-details/product-mapping-details.component.ts ***!
  \*****************************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductMappingDetailsComponent": () => (/* binding */ ProductMappingDetailsComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _product_mapping_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./product-mapping-details.component.html?ngResource */ 4347);
/* harmony import */ var _product_mapping_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product-mapping-details.component.sass?ngResource */ 8044);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/master.service */ 6754);






let ProductMappingDetailsComponent = class ProductMappingDetailsComponent {
    constructor(MasterService, formBuilder) {
        this.MasterService = MasterService;
        this.formBuilder = formBuilder;
        this.onSave = new _angular_core__WEBPACK_IMPORTED_MODULE_3__.EventEmitter();
        this.onCancel = new _angular_core__WEBPACK_IMPORTED_MODULE_3__.EventEmitter();
        this.screenName = 'Product Mapping';
        this.BTN_VAL = 'Submit';
        this.other_segment_code = 'false';
        this.BankNamesdata = [];
        this.AccountTypeData = [];
        this.SegmentCodeData = [];
        this.IntCatData = [];
        this.Comp1Data = [];
        this.Comp2Data = [];
        this.SubCategoryData = [];
        this.ReportNameData = [];
    }
    ngOnInit() {
        this.ProductMappingForm = this.formBuilder.group({
            bank_ref_id: [""],
            initialItemRow1: this.formBuilder.array([this.initialitemRow()]),
        });
        this.MasterService.getAllBankNames().subscribe((data) => {
            this.BankNamesdata = data;
        });
        this.MasterService.getCodeDefine("Account Type").subscribe((data) => {
            this.AccountTypeData = data;
        });
        this.MasterService.getAllReportName().subscribe((data) => {
            this.ReportNameData = data;
        });
        this.MasterService.getCodeDefine("Segment Code").subscribe((data) => {
            this.SegmentCodeData = data;
        });
        this.MasterService.getCodeDefine("Int Cat").subscribe((data) => {
            this.IntCatData = data;
        });
        this.MasterService.getCodeDefine("Comp1").subscribe((data) => {
            this.Comp1Data = data;
            console.log(this.Comp1Data);
        });
        this.MasterService.getCodeDefine("Comp2").subscribe((data) => {
            this.Comp2Data = data;
        });
        this.MasterService.getSubCategoryData().subscribe((data) => {
            this.SubCategoryData = data;
        });
        if (this.rowData.length > 0) {
            this.viewRecord(this.rowData);
        }
    }
    initialitemRow() {
        return this.formBuilder.group({
            id: [],
            description: [''],
            comp1: [''],
            comp2: [''],
            segment_code: [''],
            acct_type: [''],
            int_cat: [''],
            details_ref_id: [],
            report_ref_id: [],
            cgl_code: [],
            is_linked: [false],
            is_active: [true],
            is_deleted: [false],
            bank_ref_id: [],
            // is_alm:[false]
        });
    }
    get formArray() {
        return this.ProductMappingForm.get("initialItemRow1");
    }
    addNewRow() {
        this.formArray.push(this.initialitemRow());
    }
    deleteRow(index) {
        if (this.formArray.length == 1) {
            return false;
        }
        else {
            this.formArray.removeAt(index);
            return true;
        }
    }
    viewRecord(row) {
        if (this.submitBtn === true) {
            this.BTN_VAL = "Update";
        }
        this.ProductMappingForm.patchValue({
            bank_ref_id: row[0].bank_ref_id,
        });
        this.ProductMappingForm.setControl("initialItemRow1", this.setExistingArray(row));
    }
    setExistingArray(initialArray = []) {
        const formArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormArray([]);
        initialArray.forEach((element, ind) => {
            formArray.push(this.formBuilder.group({
                description: element.description,
                segment_code: element.segment_code,
                comp1: element.comp1,
                comp2: element.comp2,
                acct_type: element.acct_type,
                int_cat: element.int_cat,
                details_ref_id: element.details_ref_id,
                report_ref_id: element.report_ref_id,
                cgl_code: element.cgl_code,
                id: element.id,
                is_linked: element.is_linked,
                is_active: element.is_active,
                is_deleted: element.is_deleted,
                bank_ref_id: element.bank_ref_id,
                // is_alm: element.is_alm
            }));
            if (element.is_linked) {
                this.ProductMappingForm.controls['bank_ref_id'].disable();
                formArray.at(ind).get('details_ref_id').disable();
                formArray.at(ind).get('report_ref_id').disable();
                formArray.at(ind).get('segment_code').disable();
                formArray.at(ind).get('comp1').disable();
                formArray.at(ind).get('comp2').disable();
                formArray.at(ind).get('acct_type').disable();
                formArray.at(ind).get('int_cat').disable();
                formArray.at(ind).get('cgl_code').disable();
                // formArray.at(ind).get('is_alm').disable();
            }
        });
        return formArray;
    }
    cgl_code(i) {
        let value = this.formArray.at(i).get("comp1").value + this.formArray.at(i).get("comp2").value + this.formArray.at(i).get("segment_code").value;
        this.formArray.at(i).get("cgl_code").setValue(value);
        this.formArray.at(i).get("bank_ref_id").setValue(this.ProductMappingForm.get('bank_ref_id').value);
        if (this.formArray.at(i).get("segment_code").value == '5050') {
            this.other_segment_code = 'true';
        }
        else {
            this.other_segment_code = 'false';
        }
    }
    onCancelForm() {
        this.ProductMappingForm.reset();
        this.onCancel.emit(false);
    }
    onSubmit() {
        if (this.ProductMappingForm.invalid) {
            console.log(this.ProductMappingForm);
            return;
        }
        else {
            this.onSave.emit({ form: this.ProductMappingForm.value.initialItemRow1, submitType: this.BTN_VAL });
        }
    }
};
ProductMappingDetailsComponent.ctorParameters = () => [
    { type: src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__.MasterService },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_4__.FormBuilder }
];
ProductMappingDetailsComponent.propDecorators = {
    rowData: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    submitBtn: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Input }],
    onSave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Output }],
    onCancel: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__.Output }]
};
ProductMappingDetailsComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_5__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_3__.Component)({
        selector: 'app-product-mapping-details',
        template: _product_mapping_details_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_product_mapping_details_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProductMappingDetailsComponent);



/***/ }),

/***/ 8248:
/*!*********************************************************************!*\
  !*** ./src/app/master/product-mapping/product-mapping.component.ts ***!
  \*********************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ProductMappingComponent": () => (/* binding */ ProductMappingComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _product_mapping_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./product-mapping.component.html?ngResource */ 1727);
/* harmony import */ var _product_mapping_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./product-mapping.component.sass?ngResource */ 1187);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/sort */ 4316);
/* harmony import */ var src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/shared/services/master.service */ 6754);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_4__);










let ProductMappingComponent = class ProductMappingComponent {
    constructor(MasterService, manageSecurity) {
        this.MasterService = MasterService;
        this.manageSecurity = manageSecurity;
        this.screenName = 'Product Mapping';
        this.BTN_VAL = 'Submit';
        this.submitBtn = true;
        this.listDiv = false;
        this.showList = true;
        this.rowData = [];
        this.displayColumns = [
            "actions",
            "tenant_name"
        ];
    }
    ngOnInit() {
        this.getAllProductMapping();
        this.USERID = localStorage.getItem('user_id');
        this.manageSecurity.getAccessLeftPanel(this.USERID, 'Tenant Hierarchy Define').subscribe(data => {
            this.sidebarData = data;
        });
    }
    getAllProductMapping() {
        this.MasterService.getAllProductMapping().subscribe((data) => {
            this.dataSource = new _angular_material_table__WEBPACK_IMPORTED_MODULE_5__.MatTableDataSource(data);
            this.dataSource.paginator = this.dataSourcePaginator;
            this.dataSource.sort = this.sort;
        });
    }
    showSwalMassage(massage, icon) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire({
            title: massage,
            icon: icon,
            timer: 2000,
            showConfirmButton: false,
        });
    }
    viewRecord(row) {
        this.MasterService.getBankWiseProductMapping(row.bank_ref_id_id).subscribe((data) => {
            this.rowData = data;
            this.showList = false;
            this.submitBtn = false;
            this.listDiv = true;
        });
    }
    editRecord(row) {
        this.MasterService.getBankWiseProductMapping(row.bank_ref_id_id).subscribe((data) => {
            this.rowData = data;
            this.showList = false;
            this.listDiv = true;
            this.submitBtn = true;
            this.BTN_VAL = "Update";
        });
    }
    showFormList(item) {
        this.rowData = [];
        this.BTN_VAL = 'Submit';
        if (item === false) {
            this.listDiv = true;
            this.showList = false;
            this.submitBtn = true;
        }
        else {
            this.listDiv = false;
            this.showList = true;
        }
    }
    onCancel(item) {
        this.listDiv = item;
        this.showList = true;
        this.rowData = [];
        this.BTN_VAL = 'Submit';
    }
    tbl_FilterDatatable(value) {
        this.dataSource.filter = value.trim().toLocaleLowerCase();
    }
    onSave(formvalue) {
        this.MasterService.createProductMapping(formvalue.form, formvalue.submitType).subscribe((data) => {
            if (data["status"] == 1) {
                this.showSwalMassage("Your record has been updated successfully!", "success");
            }
            if (data["status"] == 2) {
                this.showSwalMassage("Your record has been added successfully!", "success");
            }
            this.showList = true;
            this.listDiv = false;
            this.rowData = [];
            this.BTN_VAL = "Submit";
            this.getAllProductMapping();
        }, (error) => {
            sweetalert2__WEBPACK_IMPORTED_MODULE_4___default().fire(error);
        });
    }
};
ProductMappingComponent.ctorParameters = () => [
    { type: src_app_shared_services_master_service__WEBPACK_IMPORTED_MODULE_2__.MasterService },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_3__.ManageSecurityService }
];
ProductMappingComponent.propDecorators = {
    dataSourcePaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChild, args: ["dataSourcePaginator", { read: _angular_material_paginator__WEBPACK_IMPORTED_MODULE_7__.MatPaginator },] }],
    sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_6__.ViewChild, args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_8__.MatSort, { static: true },] }]
};
ProductMappingComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_9__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_6__.Component)({
        selector: 'app-product-mapping',
        template: _product_mapping_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_product_mapping_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ProductMappingComponent);



/***/ }),

/***/ 9367:
/*!******************************************************************************************************************!*\
  !*** ./src/app/master/category-define/category-define-details/category-define-details.component.sass?ngResource ***!
  \******************************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjYXRlZ29yeS1kZWZpbmUtZGV0YWlscy5jb21wb25lbnQuc2FzcyJ9 */";

/***/ }),

/***/ 7098:
/*!**********************************************************************************!*\
  !*** ./src/app/master/category-define/category-define.component.sass?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJjYXRlZ29yeS1kZWZpbmUuY29tcG9uZW50LnNhc3MifQ== */";

/***/ }),

/***/ 1015:
/*!************************************************************************************************************************************!*\
  !*** ./src/app/master/file-structure-define/file-structure-define-details/file-structure-define-details.component.sass?ngResource ***!
  \************************************************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmaWxlLXN0cnVjdHVyZS1kZWZpbmUtZGV0YWlscy5jb21wb25lbnQuc2FzcyJ9 */";

/***/ }),

/***/ 8403:
/*!**********************************************************************************************!*\
  !*** ./src/app/master/file-structure-define/file-structure-define.component.sass?ngResource ***!
  \**********************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJmaWxlLXN0cnVjdHVyZS1kZWZpbmUuY29tcG9uZW50LnNhc3MifQ== */";

/***/ }),

/***/ 8044:
/*!******************************************************************************************************************!*\
  !*** ./src/app/master/product-mapping/product-mapping-details/product-mapping-details.component.sass?ngResource ***!
  \******************************************************************************************************************/
/***/ ((module) => {

module.exports = ".row-container {\n  display: flex;\n  align-items: center;\n}\n\n.before-text,\n.after-text {\n  margin-left: 10px;\n  margin-right: 10px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2R1Y3QtbWFwcGluZy1kZXRhaWxzLmNvbXBvbmVudC5zYXNzIiwiLi5cXC4uXFwuLlxcLi5cXC4uXFwuLlxcLi5cXC4uXFxHaXRsYWIlMjBQZXJzb25hbFxcZGVwbG95dGVzdG1haW5cXGZyb250ZW5kXFxzcmNcXGFwcFxcbWFzdGVyXFxwcm9kdWN0LW1hcHBpbmdcXHByb2R1Y3QtbWFwcGluZy1kZXRhaWxzXFxwcm9kdWN0LW1hcHBpbmctZGV0YWlscy5jb21wb25lbnQuc2FzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGFBQUE7RUFDQSxtQkFBQTtBQ0NGOztBREVBOztFQUVFLGlCQUFBO0VBQ0Esa0JBQUE7QUNDRiIsImZpbGUiOiJwcm9kdWN0LW1hcHBpbmctZGV0YWlscy5jb21wb25lbnQuc2FzcyIsInNvdXJjZXNDb250ZW50IjpbIi5yb3ctY29udGFpbmVyIFxyXG4gIGRpc3BsYXk6IGZsZXhcclxuICBhbGlnbi1pdGVtczogY2VudGVyXHJcblxyXG5cclxuLmJlZm9yZS10ZXh0LFxyXG4uYWZ0ZXItdGV4dCBcclxuICBtYXJnaW4tbGVmdDogMTBweFxyXG4gIG1hcmdpbi1yaWdodDogMTBweFxyXG4iLCIucm93LWNvbnRhaW5lciB7XG4gIGRpc3BsYXk6IGZsZXg7XG4gIGFsaWduLWl0ZW1zOiBjZW50ZXI7XG59XG5cbi5iZWZvcmUtdGV4dCxcbi5hZnRlci10ZXh0IHtcbiAgbWFyZ2luLWxlZnQ6IDEwcHg7XG4gIG1hcmdpbi1yaWdodDogMTBweDtcbn0iXX0= */";

/***/ }),

/***/ 1187:
/*!**********************************************************************************!*\
  !*** ./src/app/master/product-mapping/product-mapping.component.sass?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9kdWN0LW1hcHBpbmcuY29tcG9uZW50LnNhc3MifQ== */";

/***/ }),

/***/ 4464:
/*!******************************************************************************************************************!*\
  !*** ./src/app/master/category-define/category-define-details/category-define-details.component.html?ngResource ***!
  \******************************************************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"row clearfix\">\r\n    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n        <form [formGroup]=\"CategoryDefineForm\" (ngSubmit)=\"onSubmit()\">\r\n            <div class=\"row clearfix\">\r\n                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                    <div class=\"card\">\r\n                        <div class=\"body\">\r\n\r\n                            <div class=\"table-responsive\" formArrayName=\"initialItemRow1\">\r\n                                <table id=\"tableExport1\"\r\n                                    class=\"display table table-hover table-checkable order-column m-t-20 width-per-100 table-bordered\">\r\n\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th style=\"text-align:center\">Action</th>\r\n                                            <th style=\"text-align:center\">Category</th>\r\n                                            <th style=\"text-align:center\">Sub Category</th>\r\n                                            <th style=\"text-align:center\">Sub Category Type</th>\r\n                                            <th style=\"text-align:center\">Sub Category Parent</th>\r\n                                        </tr>\r\n                                    </thead>\r\n\r\n                                    <tbody class=\"main_tbody\">\r\n                                        <tr *ngFor=\"let dynamic of CategoryDefineForm.controls.initialItemRow1['controls']; let i = index\"\r\n                                            [formGroupName]=\"i\">\r\n                                            <td>\r\n                                                <center>\r\n                                                    <ng-container *ngIf=\"i==0\">\r\n                                                        <button class=\"btn btn-primary btn-rounded waves-effect\"\r\n                                                            type=\"button\" (click)=\"addNewRow()\">+</button>\r\n                                                    </ng-container>\r\n                                                    <ng-container *ngIf=\"i>0\">\r\n                                                        <button class=\"btn btn-danger btn-rounded waves-effect\"\r\n                                                            type=\"button\" (click)=\"deleteRow(i)\">-</button>\r\n                                                    </ng-container>\r\n                                                </center>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"summary_product_type\"\r\n                                                        id=\"summary_product_type{{i+1}}\"\r\n                                                        (selectionChange)=\"productTypeChange($event.value, i)\"\r\n                                                        placeholder=\"Select Summary Product Type\" required>\r\n                                                        <mat-option [value]=\"data.id\" *ngFor=\"let data of summaryType\">\r\n                                                            {{data.master_value}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <input matInput formControlName=\"summary_name\"\r\n                                                        id=\"summary_name{{i+1}}\" required\r\n                                                        placeholder=\"Enter Summary Name\"\r\n                                                        (change)=\"summaryName(CategoryDefineForm.value.initialItemRow1[i].summary_name, CategoryDefineForm.value.initialItemRow1[i].summary_type_ref_id, i)\">\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"summary_type_ref_id\"\r\n                                                        id=\"summary_type_ref_id{{i+1}}\"\r\n                                                        placeholder=\"Select Summary Type\" required\r\n                                                        (selectionChange)=\"summaryName(CategoryDefineForm.value.initialItemRow1[i].summary_name, $event.value, i)\">\r\n                                                        <mat-option [value]=\"data.id\"\r\n                                                            *ngFor=\"let data of summaryTypeData\">\r\n                                                            {{data.master_value}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"summary_parent_ref_id\"\r\n                                                        id=\"summary_parent_ref_id{{i+1}}\"\r\n                                                        placeholder=\"Select Summary Parent\" required>\r\n                                                        <mat-option [value]=\"data.parent_name\"\r\n                                                            *ngFor=\"let data of summaryParentData\">\r\n                                                            {{data.parent_name}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                        </tr>\r\n                                    </tbody>\r\n\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"card\">\r\n                <div class=\"body\" style=\"text-align: center;\">\r\n                    <div class=\"button-demo\">\r\n                        <button class=\"btn btn-primary mr5\" type=\"submit\" *ngIf=\"submitBtn\" name=\"submit\"\r\n                            value=\"Submit\">{{BTN_VAL}}</button>\r\n                        <button (click)=\"onCancelForm()\" class=\"btn btn-danger\" value=\"Cancel\">Cancel</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>";

/***/ }),

/***/ 5110:
/*!**********************************************************************************!*\
  !*** ./src/app/master/category-define/category-define.component.html?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <app-screen-header [screenName]=\"screenName\" (showFormList)=\"showFormList($event)\"\r\n        [showList]=\"showList\"></app-screen-header>\r\n    <app-category-define-details *ngIf=\"showList === false\" [rowData]=\"rowData\" (onSave)=\"onSave($event)\"\r\n        (onCancel)=\"onCancel($event)\" [submitBtn]=\"submitBtn\">\r\n    </app-category-define-details>\r\n\r\n    <div class=\"row clearfix\" [hidden]=\"listDiv\">\r\n        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"table-responsive\">\r\n                        <div class=\"row clearfix\">\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                            </div>\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\"text-align: right;\">\r\n                                <mat-form-field appearance=\"fill\">\r\n                                    <mat-label>\r\n                                        <em class=\"fa fa-search\" aria-hidden=\"true\"></em>\r\n                                        Search\r\n                                    </mat-label>\r\n                                    <input matInput (keyup)='tbl_FilterDatatable($event.target.value)'>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n                        <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-cell\" id=\"excel-table\">\r\n                            <ng-container matColumnDef=\"actions\">\r\n                                <mat-header-cell *matHeaderCellDef style=\"color: white;\">Actions</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row;\">\r\n                                    <span *ngFor=\"let item of sidebarData\">\r\n                                        <button *ngIf=\"item.read_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"viewRecord(row)\">\r\n                                            <em class=\"material-icons\">visibility</em>\r\n                                        </button>\r\n                                        <button *ngIf=\"item.write_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"editRecord(row)\">\r\n                                            <em class=\"material-icons\">mode_edit</em>\r\n                                        </button>\r\n                                    </span>\r\n                                </mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"product_type\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header\r\n                                    style=\"color: white;\">Category</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.product_type}}</mat-cell>\r\n                            </ng-container>\r\n\r\n                            <mat-header-row *matHeaderRowDef=\"displayColumns\"\r\n                                style=\"background-color:#666666;\"></mat-header-row>\r\n                            <mat-row *matRowDef=\"let row; columns: displayColumns;\" [style.cursor]=\"'pointer'\"\r\n                                matRipple></mat-row>\r\n                        </mat-table>\r\n                        <mat-paginator #dataSourcePaginator=\"matPaginator\" [pageSizeOptions]=\"[ 10, 15, 20]\"\r\n                            [showFirstLastButtons]=\"true\">\r\n                        </mat-paginator>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>";

/***/ }),

/***/ 772:
/*!************************************************************************************************************************************!*\
  !*** ./src/app/master/file-structure-define/file-structure-define-details/file-structure-define-details.component.html?ngResource ***!
  \************************************************************************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"row clearfix\">\r\n    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n        <form [formGroup]=\"FileStructureDefineForm\" (ngSubmit)=\"onSubmit()\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"row clearfix\">\r\n                        <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>File Name<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <input matInput formControlName=\"file_name\" placeholder=\"Enter File Name\" required>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Type of File<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"file_type_ref_id\" placeholder=\"Select File Type\"\r\n                                        required>\r\n                                        <mat-option [value]=\"file.id\" *ngFor=\"let file of fileTypeData\">\r\n                                            {{file.master_value}}</mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <label class=\"form-label\">Is Temporary?</label><br>\r\n                                <mat-label style=\"margin-right: 0.5rem;\">No</mat-label>\r\n                                <mat-slide-toggle id=\"is_temporary_flag\"\r\n                                    formControlName=\"is_temporary_flag\">Yes</mat-slide-toggle>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <label class=\"form-label\">Is Active?</label><br>\r\n                                <mat-label style=\"margin-right: 0.5rem;\">No</mat-label>\r\n                                <mat-slide-toggle id=\"is_active\" formControlName=\"is_active\"\r\n                                    (change)=\"changeState($event)\">Yes</mat-slide-toggle>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"row clearfix\">\r\n                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                    <div class=\"card\">\r\n                        <div class=\"body\">\r\n                            <h5 style=\"text-align: center;\"><strong>Define {{screenName}} Details</strong></h5><br>\r\n\r\n                            <div class=\"table-responsive\" formArrayName=\"initialItemRow1\">\r\n                                <table id=\"tableExport1\"\r\n                                    class=\"display table table-hover table-checkable order-column m-t-20 width-per-100 table-bordered\">\r\n\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th style=\"text-align:center\">Action</th>\r\n                                            <th style=\"text-align:center\">Field Name</th>\r\n                                            <th style=\"text-align:center\">Actual Field Name</th>\r\n                                            <th style=\"text-align:center\">Refer From</th>\r\n                                            <th style=\"text-align:center\">Field Length</th>\r\n                                            <th style=\"text-align:center\">Data Type</th>\r\n                                        </tr>\r\n                                    </thead>\r\n\r\n                                    <tbody class=\"main_tbody\">\r\n                                        <tr *ngFor=\"let dynamic of FileStructureDefineForm.controls.initialItemRow1['controls']; let i = index\"\r\n                                            [formGroupName]=\"i\">\r\n                                            <td>\r\n                                                <center>\r\n                                                    <ng-container *ngIf=\"i==0\">\r\n                                                        <button class=\"btn btn-primary btn-rounded waves-effect\"\r\n                                                            type=\"button\" (click)=\"addNewRow()\">+</button>\r\n                                                    </ng-container>\r\n                                                    <ng-container *ngIf=\"i>0\">\r\n                                                        <button class=\"btn btn-danger btn-rounded waves-effect\"\r\n                                                            type=\"button\" (click)=\"deleteRow(i)\">-</button>\r\n                                                    </ng-container>\r\n                                                </center>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <input matInput formControlName=\"field_name\" id=\"field_name{{i+1}}\"\r\n                                                        placeholder=\"Enter Field Name\">\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <input matInput formControlName=\"actual_field_name\"\r\n                                                        id=\"actual_field_name{{i+1}}\"\r\n                                                        placeholder=\"Enter Actual Field Name\">\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"refer_from\" id=\"refer_from{{i+1}}\"\r\n                                                        placeholder=\"Select Refer from\" required>\r\n                                                        <mat-option [value]=\"data\" *ngFor=\"let data of ReferFromData\">\r\n                                                            {{data}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <input matInput formControlName=\"field_length\"\r\n                                                        id=\"field_length{{i+1}}\" type=\"number\" min=\"0\"\r\n                                                        placeholder=\"Enter Field Length\" required>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"data_type\" id=\"data_type{{i+1}}\"\r\n                                                        placeholder=\"Select Data Type\" required>\r\n                                                        <mat-option [value]=\"data.id\" *ngFor=\"let data of DatatypeData\">\r\n                                                            {{data.master_value}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                        </tr>\r\n                                    </tbody>\r\n\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"card\">\r\n                <div class=\"body\" style=\"text-align: center;\">\r\n                    <div class=\"button-demo\">\r\n                        <button class=\"btn btn-primary mr5\" type=\"submit\" *ngIf=\"submitBtn\" name=\"submit\"\r\n                            value=\"Submit\">{{BTN_VAL}}</button>\r\n                        <button (click)=\"onCancelForm()\" class=\"btn btn-danger\" value=\"Cancel\">Cancel</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>";

/***/ }),

/***/ 2953:
/*!**********************************************************************************************!*\
  !*** ./src/app/master/file-structure-define/file-structure-define.component.html?ngResource ***!
  \**********************************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <app-screen-header [screenName]=\"screenName\" (showFormList)=\"showFormList($event)\"\r\n        [showList]=\"showList\"></app-screen-header>\r\n    <app-file-structure-define-details *ngIf=\"showList === false\" [rowData]=\"rowData\" (onSave)=\"onSave($event)\"\r\n        (onCancel)=\"onCancel($event)\" [submitBtn]=\"submitBtn\">\r\n    </app-file-structure-define-details>\r\n\r\n    <div class=\"row clearfix\" [hidden]=\"listDiv\">\r\n        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"table-responsive\">\r\n                        <div class=\"row clearfix\">\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                                <div class=\"dt-buttons btn-group flex-wrap\">\r\n                                    <button class=\"btn btn-secondary  buttons-html5\" tabindex=\"0\"\r\n                                        aria-controls=\"example1\" type=\"button\" (click)=\"export_to_excel();\">\r\n                                        <span>Export To Excel</span>\r\n                                    </button>\r\n                                </div>\r\n                            </div>\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\"text-align: right;\">\r\n                                <mat-form-field appearance=\"fill\">\r\n                                    <mat-label>\r\n                                        <em class=\"fa fa-search\" aria-hidden=\"true\"></em>\r\n                                        Search\r\n                                    </mat-label>\r\n                                    <input matInput (keyup)='tbl_FilterDatatable($event.target.value)'>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n                        <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-cell\" id=\"excel-table\">\r\n                            <ng-container matColumnDef=\"actions\">\r\n                                <mat-header-cell *matHeaderCellDef style=\"color: white;\">Actions</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row;\">\r\n                                    <span *ngFor=\"let item of sidebarData\">\r\n                                        <button *ngIf=\"item.read_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"viewRecord(row)\">\r\n                                            <em class=\"material-icons\">visibility</em>\r\n                                        </button>\r\n                                        <button *ngIf=\"row.is_temporary_flag==true && item.write_access==true\"\r\n                                            class=\"btn tblActnBtn h-auto\" (click)=\"editRecord(row)\">\r\n                                            <em class=\"material-icons\">mode_edit</em>\r\n                                        </button>\r\n                                    </span>\r\n                                </mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"file_name\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">File\r\n                                    Name</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.file_name}}</mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"file_type_ref_id\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">File\r\n                                    Type</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.file_type_name}}</mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"is_temporary_flag\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Is_temporary\r\n                                    ?</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.is_temporary_flag}}</mat-cell>\r\n                            </ng-container>\r\n\r\n                            <mat-header-row *matHeaderRowDef=\"displayColumns\"\r\n                                style=\"background-color:#666666;\"></mat-header-row>\r\n                            <mat-row *matRowDef=\"let row; columns: displayColumns;\" [style.cursor]=\"'pointer'\"\r\n                                matRipple></mat-row>\r\n                        </mat-table>\r\n                        <mat-paginator #dataSourcePaginator=\"matPaginator\" [pageSizeOptions]=\"[ 10, 15, 20]\"\r\n                            [showFirstLastButtons]=\"true\">\r\n                        </mat-paginator>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>";

/***/ }),

/***/ 4347:
/*!******************************************************************************************************************!*\
  !*** ./src/app/master/product-mapping/product-mapping-details/product-mapping-details.component.html?ngResource ***!
  \******************************************************************************************************************/
/***/ ((module) => {

module.exports = "<div class=\"row clearfix\">\r\n    <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n        <form [formGroup]=\"ProductMappingForm\" (ngSubmit)=\"onSubmit()\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"row clearfix\">\r\n                        <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                            <div class=\"form-group\">\r\n                                <mat-label>Bank<em style=\"color: red;\">*</em></mat-label>\r\n                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                    <mat-select formControlName=\"bank_ref_id\" placeholder=\"Select Bank Name\" required>\r\n                                        <mat-option [value]=\"data.id\"\r\n                                            *ngFor=\"let data of BankNamesdata\">{{data.tenant_name}}</mat-option>\r\n                                    </mat-select>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"row clearfix\">\r\n                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                    <div class=\"card\">\r\n                        <div class=\"body\">\r\n                            <h5 style=\"text-align: center;\"><strong>Define {{screenName}} Details</strong></h5><br>\r\n\r\n                            <div class=\"table-responsive\" formArrayName=\"initialItemRow1\">\r\n                                <table id=\"tableExport1\"\r\n                                    class=\"display table table-hover table-checkable order-column m-t-20 width-per-100 table-bordered\">\r\n\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th style=\"text-align:center\">Action</th>\r\n                                            <th style=\"text-align:center\">Product Name</th>\r\n                                            <th style=\"text-align:center\">Report Name</th>\r\n                                            <th style=\"text-align:center\">Sub Category</th>\r\n                                            <th style=\"text-align:center\">Comp1</th>\r\n                                            <th style=\"text-align:center\">Comp2</th>\r\n                                            <th style=\"text-align:center\">Segment Code</th>\r\n                                            <th style=\"text-align:center\">Cgl Code</th>\r\n                                            <th style=\"text-align:center\" *ngIf=\"other_segment_code == 'false'\" >Account Type</th>\r\n                                            <th style=\"text-align:center\" *ngIf=\"other_segment_code == 'false'\" >Int Cat</th>\r\n                                            <!-- <th style=\"text-align:center\">Is ALM</th> -->\r\n                                        </tr>\r\n                                    </thead>\r\n\r\n                                    <tbody class=\"main_tbody\">\r\n                                        <tr *ngFor=\"let dynamic of ProductMappingForm.controls.initialItemRow1['controls']; let i = index\"\r\n                                            [formGroupName]=\"i\">\r\n                                            <td>\r\n                                                <center>\r\n                                                    <ng-container *ngIf=\"i==0\">\r\n                                                        <button class=\"btn btn-primary btn-rounded waves-effect\"\r\n                                                            type=\"button\" (click)=\"addNewRow()\">+</button>\r\n                                                    </ng-container>\r\n                                                    <ng-container *ngIf=\"i>0\">\r\n                                                        <button class=\"btn btn-danger btn-rounded waves-effect\"\r\n                                                            type=\"button\" (click)=\"deleteRow(i)\">-</button>\r\n                                                    </ng-container>\r\n                                                </center>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <input matInput formControlName=\"description\"\r\n                                                        id=\"description{{i+1}}\" required\r\n                                                        placeholder=\"Enter Product Name\">\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"report_ref_id\"\r\n                                                        id=\"report_ref_id{{i+1}}\" placeholder=\"Select Report Name\"\r\n                                                        required>\r\n                                                        <mat-option [value]=\"data.id\"\r\n                                                            *ngFor=\"let data of ReportNameData\">\r\n                                                            {{data.report_name}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"details_ref_id\"\r\n                                                        id=\"details_ref_id{{i+1}}\" placeholder=\"Select Sub Category\">\r\n                                                        <mat-option [value]=\"data.id\"\r\n                                                            *ngFor=\"let data of SubCategoryData\">\r\n                                                            {{data.summary_name}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"comp1\" id=\"comp1{{i+1}}\"\r\n                                                        (selectionChange)=\"cgl_code(i)\" placeholder=\"Select Comp1\"\r\n                                                        required>\r\n                                                        <mat-option [value]=\"data.code_number\"\r\n                                                            *ngFor=\"let data of Comp1Data\">\r\n                                                            {{data.code_number}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"comp2\" id=\"comp2{{i+1}}\"\r\n                                                        (selectionChange)=\"cgl_code(i)\" placeholder=\"Select Comp2\"\r\n                                                        required>\r\n                                                        <mat-option [value]=\"data.code_number\"\r\n                                                            *ngFor=\"let data of Comp2Data\">\r\n                                                            {{data.code_number}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"segment_code\" id=\"segment_code{{i+1}}\"\r\n                                                        (selectionChange)=\"cgl_code(i)\"\r\n                                                        placeholder=\"Select Segment Code\" required>\r\n                                                        <mat-option [value]=\"data.code_number\"\r\n                                                            *ngFor=\"let data of SegmentCodeData\">\r\n                                                            {{data.code_number}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td>\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <input matInput formControlName=\"cgl_code\" id=\"cgl_code{{i+1}}\"\r\n                                                        readonly placeholder=\"Enter Cgl Code\">\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\" *ngIf=\"other_segment_code == 'false'\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"acct_type\" id=\"acct_type{{i+1}}\"\r\n                                                        placeholder=\"Select Account Type\" >\r\n                                                        <mat-option [value]=\"data.code_number\"\r\n                                                            *ngFor=\"let data of AccountTypeData\">\r\n                                                            {{data.code_number}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\" *ngIf=\"other_segment_code == 'false'\">\r\n                                                <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                    <mat-select formControlName=\"int_cat\" id=\"int_cat{{i+1}}\"\r\n                                                        placeholder=\"Select Int Cat\" >\r\n                                                        <mat-option [value]=\"data.code_number\"\r\n                                                            *ngFor=\"let data of IntCatData\">\r\n                                                            {{data.code_number}}</mat-option>\r\n                                                    </mat-select>\r\n                                                </mat-form-field>\r\n                                            </td>\r\n\r\n                                            <!-- <td >   \r\n                                                <div class=\"row-container\">\r\n                                                    <span class=\"before-text\">Yes</span>\r\n                                                        <mat-slide-toggle formControlName=\"is_alm\" id=\"is_alm{{i+1}}\" required></mat-slide-toggle> \r\n                                                    <span class=\"after-text\">No</span>\r\n                                                  </div>\r\n                                            </td> -->\r\n\r\n                                        </tr>\r\n                                    </tbody>\r\n\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div class=\"card\">\r\n                <div class=\"body\" style=\"text-align: center;\">\r\n                    <div class=\"button-demo\">\r\n                        <button class=\"btn btn-primary mr5\" type=\"submit\" *ngIf=\"submitBtn\" name=\"submit\"\r\n                            value=\"Submit\">{{BTN_VAL}}</button>\r\n                        <button (click)=\"onCancelForm()\" class=\"btn btn-danger\" value=\"Cancel\">Cancel</button>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </form>\r\n    </div>\r\n</div>";

/***/ }),

/***/ 1727:
/*!**********************************************************************************!*\
  !*** ./src/app/master/product-mapping/product-mapping.component.html?ngResource ***!
  \**********************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <app-screen-header [screenName]=\"screenName\" (showFormList)=\"showFormList($event)\"\r\n        [showList]=\"showList\"></app-screen-header>\r\n    <app-product-mapping-details *ngIf=\"showList === false\" [rowData]=\"rowData\" (onSave)=\"onSave($event)\"\r\n        (onCancel)=\"onCancel($event)\" [submitBtn]=\"submitBtn\">\r\n    </app-product-mapping-details>\r\n\r\n    <div class=\"row clearfix\" [hidden]=\"listDiv\">\r\n        <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n            <div class=\"card\">\r\n                <div class=\"body\">\r\n                    <div class=\"table-responsive\">\r\n                        <div class=\"row clearfix\">\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                            </div>\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\"text-align: right;\">\r\n                                <mat-form-field appearance=\"fill\">\r\n                                    <mat-label>\r\n                                        <em class=\"fa fa-search\" aria-hidden=\"true\"></em>\r\n                                        Search\r\n                                    </mat-label>\r\n                                    <input matInput (keyup)='tbl_FilterDatatable($event.target.value)'>\r\n                                </mat-form-field>\r\n                            </div>\r\n                        </div>\r\n                        <mat-table #table [dataSource]=\"dataSource\" matSort class=\"mat-cell\" id=\"excel-table\">\r\n                            <ng-container matColumnDef=\"actions\">\r\n                                <mat-header-cell *matHeaderCellDef style=\"color: white;\">Actions</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row;\">\r\n                                    <span *ngFor=\"let item of sidebarData\">\r\n                                        <button *ngIf=\"item.read_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"viewRecord(row)\">\r\n                                            <em class=\"material-icons\">visibility</em>\r\n                                        </button>\r\n                                        <button *ngIf=\"item.write_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                            (click)=\"editRecord(row)\">\r\n                                            <em class=\"material-icons\">mode_edit</em>\r\n                                        </button>\r\n                                    </span>\r\n                                </mat-cell>\r\n                            </ng-container>\r\n                            <ng-container matColumnDef=\"tenant_name\">\r\n                                <mat-header-cell *matHeaderCellDef mat-sort-header style=\"color: white;\">Bank\r\n                                    Name</mat-header-cell>\r\n                                <mat-cell *matCellDef=\"let row\"> {{row.tenant_name}}</mat-cell>\r\n                            </ng-container>\r\n\r\n                            <mat-header-row *matHeaderRowDef=\"displayColumns\"\r\n                                style=\"background-color:#666666;\"></mat-header-row>\r\n                            <mat-row *matRowDef=\"let row; columns: displayColumns;\" [style.cursor]=\"'pointer'\"\r\n                                matRipple></mat-row>\r\n                        </mat-table>\r\n                        <mat-paginator #dataSourcePaginator=\"matPaginator\" [pageSizeOptions]=\"[ 10, 15, 20]\"\r\n                            [showFirstLastButtons]=\"true\">\r\n                        </mat-paginator>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</section>";

/***/ })

}]);
//# sourceMappingURL=src_app_master_master_module_ts.js.map