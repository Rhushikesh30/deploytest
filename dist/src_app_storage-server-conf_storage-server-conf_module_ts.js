"use strict";
(self["webpackChunkatrio"] = self["webpackChunkatrio"] || []).push([["src_app_storage-server-conf_storage-server-conf_module_ts"],{

/***/ 2636:
/*!***********************************************************!*\
  !*** ./src/app/core/service/cedge-api-service.service.ts ***!
  \***********************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "CedgeApiServiceService": () => (/* binding */ CedgeApiServiceService)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ 8784);
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs/operators */ 635);
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! src/environments/environment */ 2340);





let CedgeApiServiceService = class CedgeApiServiceService {
    constructor(http) {
        this.http = http;
    }
    getMasterUsingType(master_type) {
        return this.http.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}/web/master/`, { params: { master_type } });
    }
    getMemberBankList() {
        return this.http.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}/web/get_all_bank_names/`);
    }
    getApexTenant(id) {
        return this.http.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}/web/get_tenant_by_tenant_type/?type_id=` + id);
    }
    getAllTenantHierarchyDefine() {
        return this.http.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}/web/tenant_hierarchy_define/`);
    }
    getCompanyServerLinkData() {
        return this.http.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}/web/member_bank_storage_link/`);
    }
    saveCompanySourceStorageLinking(formValue) {
        if (formValue.id) {
            for (var i = 0; i < formValue.initialItemRow.length; i++) {
                if (formValue.initialItemRow[i].id == '') {
                    delete formValue.initialItemRow[i].id;
                }
            }
            return this.http.put(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}`.concat('/web/member_bank_storage_link/' + formValue.id + '/'), formValue)
                .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(data => {
                data['status'] = 2;
                return data;
            }));
        }
        else {
            delete formValue['id'];
            let initialItemRow = formValue.initialItemRow.filter(function (props) {
                delete props.id;
                return true;
            });
            return this.http.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}`.concat('/web/member_bank_storage_link/'), formValue)
                .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(data => {
                data['status'] = 1;
                return data;
            }));
        }
    }
    deleteCompanySourceStorageLinking(value) {
        return this.http.delete(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}`.concat(`/web/member_bank_storage_link/${value}/`)).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(data => {
            return data;
        }));
    }
    getSftpServerData() {
        return this.http.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}/web/define-sftp-server/`);
    }
    saveSftpServer(formValue) {
        if (formValue.id) {
            for (var i = 0; i < formValue.initialItemRow.length; i++) {
                if (formValue.initialItemRow[i].id == '') {
                    delete formValue.initialItemRow[i].id;
                }
            }
            return this.http.put(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}`.concat('/web/define-sftp-server/' + formValue.id + '/'), formValue)
                .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(data => {
                data['status'] = 2;
                return data;
            }));
        }
        else {
            delete formValue['id'];
            let initialItemRow = formValue.initialItemRow.filter(function (props) {
                delete props.id;
                return true;
            });
            return this.http.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}`.concat('/web/define-sftp-server/'), formValue)
                .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(data => {
                data['status'] = 1;
                return data;
            }));
        }
    }
    deleteSftpServer(value) {
        return this.http.delete(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}`.concat(`/web/define-sftp-server/${value}/`)).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(data => {
            return data;
        }));
    }
    getSftpData() {
        return this.http.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}/sftpdata/`);
    }
    getScheduleProcessData() {
        return this.http.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}/web/schedule-process/`);
    }
    getScheduleName() {
        return this.http.get(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}/schedulename/`);
    }
    saveScheduleProcess(formValue) {
        if (formValue.id) {
            for (var i = 0; i < formValue.initialItemRow.length; i++) {
                if (formValue.initialItemRow[i].id == '') {
                    delete formValue.initialItemRow[i].id;
                }
            }
            return this.http.put(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}`.concat('/web/schedule-process/' + formValue.id + '/'), formValue)
                .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(data => {
                data['status'] = 2;
                return data;
            }));
        }
        else {
            delete formValue['id'];
            let initialItemRow = formValue.initialItemRow.filter(function (props) {
                delete props.id;
                return true;
            });
            return this.http.post(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}`.concat('/web/schedule-process/'), formValue)
                .pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(data => {
                data['status'] = 1;
                return data;
            }));
        }
    }
    deleteScheduleProcess(value) {
        return this.http.delete(`${src_environments_environment__WEBPACK_IMPORTED_MODULE_0__.environment.apiUrl}`.concat(`/web/schedule-process/${value}/`)).pipe((0,rxjs_operators__WEBPACK_IMPORTED_MODULE_1__.map)(data => {
            return data;
        }));
    }
};
CedgeApiServiceService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__.HttpClient }
];
CedgeApiServiceService = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.Injectable)({
        providedIn: 'root'
    })
], CedgeApiServiceService);



/***/ }),

/***/ 2458:
/*!****************************************************************************************!*\
  !*** ./src/app/storage-server-conf/bank-strorage-link/bank-strorage-link.component.ts ***!
  \****************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "BankStrorageLinkComponent": () => (/* binding */ BankStrorageLinkComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _bank_strorage_link_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bank-strorage-link.component.html?ngResource */ 7509);
/* harmony import */ var _bank_strorage_link_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bank-strorage-link.component.sass?ngResource */ 140);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/sort */ 4316);
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/collections */ 9502);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_core_service_cedge_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/cedge-api-service.service */ 2636);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);










// import * as XLSX from 'xlsx';

// import { ManageSecurityService } from '';


let BankStrorageLinkComponent = class BankStrorageLinkComponent {
    constructor(router, formBuilder, cedgeService, manageSecurity) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.cedgeService = cedgeService;
        this.manageSecurity = manageSecurity;
        this.displayColumns = [
            'actions',
            'tenant_name',
            'is_different_location'
        ];
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__.SelectionModel(false, []);
        this.hide = true;
        this.companydata = [];
        this.tbl_FilteredData = [];
        this.companyServerLinkData = [];
        this.BTN_VAL = 'Submit';
        this.companyIdCompanyMst = [];
        this.storageTypeData = [];
        this.list_form = true;
        this.list_title = true;
        this.btn_new_entry = true;
        this.btn_list = true;
        this.new_entry_form = true;
        this.new_entry_title = true;
        this.submit = true;
        this.submitted = false;
        this.sftpData = [];
        this.getCompanytoSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl();
        this.Updated_Date_Time = new Date();
    }
    ngOnInit() {
        this.USERID = Number(localStorage.getItem('user_id'));
        this.created_by = Number(localStorage.getItem('user_id'));
        this.CompanySourceCloudLinkingForm = this.formBuilder.group({
            tenant_id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
            tenant_name: [],
            is_different_location: false,
            id: [''],
            is_active: true,
            is_deleted: false,
            created_by: this.created_by,
            updated_by: this.created_by,
            updated_date_time: [this.Updated_Date_Time],
            initialItemRow: this.formBuilder.array([this.initialItemRow()]),
        });
        this.new_entry_form = false;
        this.new_entry_title = false;
        this.btn_list = false;
        this.companydata = [];
        this.cedgeService.getCompanyServerLinkData().subscribe((data) => {
            this.companyServerLinkData = data;
            this.companyServerLinkMasterData = new _angular_material_table__WEBPACK_IMPORTED_MODULE_7__.MatTableDataSource(data);
            this.companyServerLinkMasterData.paginator = this.companyServerLinkPaginator;
            this.companyServerLinkMasterData.sort = this.sort;
            this.companyServerLinkMasterData.connect().subscribe(d => this.renderedData = d);
            this.tbl_FilteredData = data;
        });
        this.cedgeService.getAllTenantHierarchyDefine().subscribe((res) => {
            res.forEach(ele => {
                if (ele.tenant_type_name == 'Member Bank') {
                    this.cedgeService.getApexTenant(ele.id).subscribe((data) => {
                        this.apex_tenant = data[0];
                        console.log(this.apex_tenant);
                        if (Number(localStorage.tenant_ref_id) == this.apex_tenant.id && Number(localStorage.user_type_id) == this.apex_tenant.tenant_type_ref_id) {
                            this.CompanySourceCloudLinkingForm.get('tenant_id').setValue(this.apex_tenant['id']);
                            this.CompanySourceCloudLinkingForm.get('tenant_name').setValue(this.apex_tenant['tenant_name']);
                        }
                    });
                }
            });
        });
        this.cedgeService.getMasterUsingType('Bank Name').subscribe((data) => {
            this.masterTypeAllSourcesData = data;
            (this.CompanySourceCloudLinkingForm.get('initialItemRow')).at(0).get("member_bank_tenant_name").setValue(this.masterTypeAllSourcesData[0].master_value);
            (this.CompanySourceCloudLinkingForm.get('initialItemRow')).at(0).get("member_bank_tenant_id").setValue(this.masterTypeAllSourcesData[0].id);
        });
        this.cedgeService.getMemberBankList().subscribe(data => {
            this.source_types_data = data;
            this.source_types = data;
        });
        this.manageSecurity.getAccessLeftPanel(this.USERID, 'Tenant Hierarchy Define').subscribe(data => {
            this.sidebarData = data;
        });
        // this.CompanySourceCloudLinkingForm.get('tenant_id').valueChanges.subscribe(element=>{
        //   if (element != null) {
        //     this.reconService.getSourceDataAsperComppany(element).subscribe(data => {
        //       this.source_types_data = data;
        //       this.source_types = data;
        //     })
        //     this.reconService.getSecureServerConfig().subscribe(data => {
        //       this.secure_server_data = data
        //       if(this.secure_server_data.length == 0)
        //       {
        //         this.storageTypeData.length = 0;
        //       }
        //       else if(this.secure_server_data.length == 1)
        //       {
        //         for(var i=0;i<this.secure_server_data[0].initialItemRow.length;i++)
        //         {
        //           if(this.secure_server_data[0].initialItemRow[i].cloud_storage_type == this.storageTypeDataSFTP[0].id)
        //           {
        //              this.storageTypeData.push(this.storageTypeDataSFTP[0]);
        //           }
        //           else if(this.secure_server_data[0].initialItemRow[i].cloud_storage_type == this.storageTypeDataGoogle[0].id)
        //           {
        //              this.storageTypeData.push(this.storageTypeDataGoogle[0]);
        //           }
        //         }
        //       }
        //     });
        //   }
        // })
        // this.showList()
        // this.manageSecurity.getAccessLeftPanel(this.USERID,'Company Source Storage link').subscribe(data =>{
        //   this.sidebarData=data;
        // })
        // this.getCompanytoSearch.valueChanges.subscribe(val=>{
        //   this.filterdCompanyName=val;
        // })
        this.cedgeService.getSftpData().subscribe((data) => {
            this.sftpData = data;
        });
    }
    // toggleValueisKey(indexKey){
    //   var indexVariable1=this.CompanySourceCloudLinkingForm.get('detailsItemRow').value[indexKey]
    //   var gridRow2 = (<FormArray>(this.sourceForm.get('detailsItemRow'))).at(indexKey);  
    //   if(indexVariable1.is_key_field==true){
    //     indexVariable1.is_primary_field=false; 
    //     indexVariable1.is_required=true;
    //     gridRow2.get('is_required').setValue(true);
    //   }
    //   else{
    //     gridRow2.get('is_required').setValue(false);
    //     indexVariable1.is_required=false;
    //   }
    // }
    showList() {
        this.list_form = true;
        this.list_title = true;
        this.btn_new_entry = true;
        this.btn_list = false;
        this.new_entry_form = false;
        this.new_entry_title = false;
        // this.list_form=false;
        // this.list_title=false;
        // this.btn_new_entry=false
        // this.btn_list=true;
        // this.new_entry_form=true;
        // this.new_entry_title=true;
    }
    showNewEntry() {
        this.list_form = false;
        this.list_title = false;
        this.btn_new_entry = false;
        this.btn_list = true;
        this.new_entry_form = true;
        this.new_entry_title = true;
        // this.list_form=false;
        // this.list_title=false;
        // this.btn_new_entry=false
        // this.btn_list=true;
        // this.new_entry_form=true;
        // this.new_entry_title=true;
    }
    tbl_FilterDatatable(value) {
        this.companyServerLinkMasterData.filter = value.trim().toLocaleLowerCase();
    }
    isDifferentLocation(e) {
        // var is_different_location = e.checked == true ? 'Y' : 'N';
        // this.CompanySourceCloudLinkingForm.get("is_different_location").setValue(is_different_location);
        // this.is_different_location_checked = !this.is_different_location_checked;
        if (e.checked == false) {
            var i = this.formArr.length - 1;
            while (i > 0) {
                this.formArr.removeAt(i);
                i--;
            }
            (this.CompanySourceCloudLinkingForm.get('initialItemRow')).at(0).get("member_bank_tenant_name").setValue(this.masterTypeAllSourcesData[0].master_value);
            (this.CompanySourceCloudLinkingForm.get('initialItemRow')).at(0).get("member_bank_tenant_id").setValue(this.masterTypeAllSourcesData[0].id);
        }
        else if (e.checked == true) {
            this.formArr.removeAt(0);
            for (var j = 0; j < this.source_types.length; j++) {
                this.formArr.push(this.initialItemRow());
                (this.CompanySourceCloudLinkingForm.get('initialItemRow')).at(j).get("member_bank_tenant_name").setValue(this.source_types[j].tenant_name);
                (this.CompanySourceCloudLinkingForm.get('initialItemRow')).at(j).get("member_bank_tenant_id").setValue(this.source_types[j].id);
            }
        }
    }
    // isActiveChangeMst(event){
    //   var temp_is_deleted = event.checked == true ? false : true;
    //   this.CompanySourceCloudLinkingForm.get('is_deleted').setValue(temp_is_deleted)
    // }
    // isActiveChangeDetails(event,index){
    //   var temp_is_deleted = event.checked == true ? false : true;
    //   (<FormArray>(this.CompanySourceCloudLinkingForm.get('initialItemRow'))).at(index).get("is_deleted").setValue(temp_is_deleted);
    // }
    onSubmit() {
        // if(this.CompanySourceCloudLinkingForm.get("is_different_location").value == '')
        // {
        //   this.CompanySourceCloudLinkingForm.value.is_different_location = 'N';
        // }
        if (this.CompanySourceCloudLinkingForm.invalid) {
            return;
        }
        else {
            let check_active_record = this.companyServerLinkData.filter((item) => item.is_active == true);
            if (check_active_record.length != 0 && this.CompanySourceCloudLinkingForm.value.is_active == true) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                    title: 'Only a single record should be active',
                    icon: 'warning',
                    timer: 5000,
                    showConfirmButton: false
                });
                return;
            }
            this.cedgeService.saveCompanySourceStorageLinking(this.CompanySourceCloudLinkingForm.value).subscribe({
                next: (data) => {
                    if (data['status'] == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: 'Your record has been added successfully!',
                            icon: 'success',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                    if (data['status'] == 2) {
                        this.companyServerLinkData[this.companyServerLinkData.findIndex(item => item.id == data['id'])] = data;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: 'Your record has been updated successfully!',
                            icon: 'success',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                    this.router.navigate(['/storage-configuration/bank-storage-link']).then(() => {
                        setTimeout(function () { window.location.reload(); }, 2000);
                    });
                },
                error: (error) => {
                    error = JSON.parse(error);
                    if (error.status == '400' && error.error.status == "Only one active record allowed") {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: 'Only a single record should be active',
                            icon: 'warning',
                            timer: 5000,
                            showConfirmButton: false
                        });
                        return;
                    }
                    else if (error.status == '400') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: "Invalid Data",
                            icon: 'warning',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: "Something went wrong",
                            icon: 'error',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                }
            });
            //   this.cedgeService.saveCompanySourceStorageLinking(this.CompanySourceCloudLinkingForm.value).subscribe((data: any) => {
            //     if (data['status'] == 1) {
            //       Swal.fire({
            //         title: 'Your record has been added successfully!',
            //         icon: 'success',
            //         timer: 2000,
            //         showConfirmButton: false
            //       });
            //     }
            //     if (data['status'] == 2) {
            //       this.companyServerLinkData[this.companyServerLinkData.findIndex(item => item.id == data['id'])] = data;
            //       Swal.fire({
            //         title: 'Your record has been updated successfully!',
            //         icon: 'success',
            //         timer: 2000,
            //         showConfirmButton: false
            //       });
            //     }
            //     this.router.navigate(['/storage-configuration/bank-storage-link']).then(() => {
            //       setTimeout(function() {window.location.reload();} , 2000);
            //     });
            //   },
            //   error => {
            //     Swal.fire({
            //       title: error,
            //       icon: 'warning',
            //       timer: 2000,
            //       showConfirmButton: false
            //     });
            // });
        }
    }
    initialItemRow() {
        return this.formBuilder.group({
            // cloud_storage_type: '',
            id: '',
            folder_name: '',
            member_bank_tenant_id: '',
            member_bank_tenant_name: '',
            sftp_ref_id: '',
            is_active: true,
            is_deleted: false,
            created_by: [this.created_by],
            updated_by: this.created_by,
            updated_date_time: [this.Updated_Date_Time]
        });
    }
    get formArr() {
        return this.CompanySourceCloudLinkingForm.get('initialItemRow');
    }
    setExistingArray(initialArray = []) {
        const formArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormArray([]);
        initialArray.forEach(element => {
            formArray.push(this.formBuilder.group({
                id: element.id,
                member_bank_tenant_name: element.member_bank_tenant_name,
                member_bank_tenant_id: element.member_bank_tenant_id,
                sftp_ref_id: element.sftp_ref_id,
                is_active: element.is_active,
                is_deleted: element.is_deleted,
                folder_name: element.folder_name,
                created_by: [this.created_by],
                updated_by: this.created_by,
                updated_date_time: [this.Updated_Date_Time]
            }));
        });
        return formArray;
    }
    viewRecord(payload) {
        this.CompanySourceCloudLinkingForm.patchValue({
            tenant_id: this.apex_tenant['id'],
            tenant_name: this.apex_tenant['tenant_name'],
            id: payload.id,
            is_active: payload.is_active,
            is_deleted: payload.is_deleted,
            is_different_location: payload.is_different_location,
            updated_date_time: new Date(),
            updated_by: this.created_by
        });
        // this.is_different_location_checked = payload.is_different_location == 'Y' ? true : false;
        this.newSource = payload.initialItemRow.filter(function (data) {
            return data.is_deleted == false;
        });
        this.CompanySourceCloudLinkingForm.setControl('initialItemRow', this.setExistingArray(this.newSource));
        const id = this.CompanySourceCloudLinkingForm.get('id').value;
        this.companyIdCompanyMst = this.companydata;
        if (id !== '') {
            this.new_entry_form = true;
            this.new_entry_title = true;
            this.btn_list = false;
            this.btn_new_entry = false;
            this.list_form = false;
            this.list_title = false;
            this.submit = false;
        }
    }
    editRecord(payload) {
        this.BTN_VAL = 'Update';
        this.CompanySourceCloudLinkingForm.patchValue({
            tenant_id: this.apex_tenant['id'],
            tenant_name: this.apex_tenant['tenant_name'],
            id: payload.id,
            is_active: payload.is_active,
            is_deleted: payload.is_deleted,
            is_different_location: payload.is_different_location,
            updated_date_time: new Date(),
            updated_by: this.created_by
        });
        // this.is_different_location_checked = payload.is_different_location == 'Y' ? true : false;
        console.log(this.CompanySourceCloudLinkingForm);
        this.newSource = payload.initialItemRow.filter(function (data) {
            return data.is_deleted == false;
        });
        this.CompanySourceCloudLinkingForm.setControl('initialItemRow', this.setExistingArray(this.newSource));
        const id = this.CompanySourceCloudLinkingForm.get('id').value;
        this.companyIdCompanyMst = this.companydata;
        if (id !== '') {
            this.new_entry_form = true;
            this.new_entry_title = true;
            this.btn_list = false;
            this.btn_new_entry = false;
            this.list_form = false;
            this.list_title = false;
            this.submit = true;
        }
    }
    deleteCompanyData(company) {
        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
            title: 'Are you sure?',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, keep it'
        }).then((result) => {
            if (result.value) {
                this.cedgeService.deleteCompanySourceStorageLinking(company)
                    .subscribe({
                    next: data => {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire('Deleted!', 'success');
                        this.router.navigate(['/storage-configuration/bank-storage-link']).then(() => {
                            setTimeout(function () { window.location.reload(); }, 2000);
                        });
                    },
                    error: error => {
                        var result = JSON.parse(error);
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire(result.error.status);
                    }
                });
            }
            else if (result.dismiss === (sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().DismissReason.cancel)) {
                sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire('Cancelled', '', 'error');
            }
        });
    }
    // export_isp_usrs() {
    //   var XlsMasterData = this.companyServerLinkData.data.map(({company_name}) =>
    //     ({company_name}));
    //   const workSheet = XLSX.utils.json_to_sheet(XlsMasterData);
    //   const workBook: XLSX.WorkBook = XLSX.utils.book_new();
    //   XLSX.utils.book_append_sheet(workBook, workSheet, 'SheetName');
    //   XLSX.writeFile(workBook, 'company_data.xlsx');
    // }
    addNewLink() {
        var companyIdSecure = this.companyServerLinkData.map(data => data.tenant_id);
        var companyIdCompany = this.companydata.map(data => data.id);
        var missingCompanyId = companyIdCompany.filter(e => !companyIdSecure.includes(e));
        this.companyIdCompanyMst = this.companydata.filter(e => missingCompanyId.includes(e.id));
    }
    changeState(event) {
        if (!event.checked) {
            this.CompanySourceCloudLinkingForm.get('is_deleted').setValue(true);
            this.formArr.value.forEach(element => {
                element.is_active = false;
                element.is_deleted = true;
            });
        }
        else {
            this.CompanySourceCloudLinkingForm.get('is_deleted').setValue(false);
            this.formArr.value.forEach(element => {
                element.is_active = true;
                element.is_deleted = false;
            });
        }
        console.log(this.CompanySourceCloudLinkingForm);
    }
};
BankStrorageLinkComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_8__.Router },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: src_app_core_service_cedge_api_service_service__WEBPACK_IMPORTED_MODULE_3__.CedgeApiServiceService },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_4__.ManageSecurityService }
];
BankStrorageLinkComponent.propDecorators = {
    companyServerLinkPaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_9__.ViewChild, args: ['companyServerLinkPaginator', { read: _angular_material_paginator__WEBPACK_IMPORTED_MODULE_10__.MatPaginator },] }],
    sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_9__.ViewChild, args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_11__.MatSort, { static: true },] }]
};
BankStrorageLinkComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_12__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_9__.Component)({
        selector: 'app-bank-strorage-link',
        template: _bank_strorage_link_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_bank_strorage_link_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], BankStrorageLinkComponent);



/***/ }),

/***/ 1922:
/*!******************************************************************************!*\
  !*** ./src/app/storage-server-conf/define-server/define-server.component.ts ***!
  \******************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "DefineServerComponent": () => (/* binding */ DefineServerComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _define_server_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./define-server.component.html?ngResource */ 7420);
/* harmony import */ var _define_server_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./define-server.component.sass?ngResource */ 2790);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/sort */ 4316);
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/collections */ 9502);
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! xlsx */ 4126);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var src_app_core_service_cedge_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/cedge-api-service.service */ 2636);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);














let DefineServerComponent = class DefineServerComponent {
    constructor(router, formBuilder, cedgeService, manageSecurity) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.cedgeService = cedgeService;
        this.manageSecurity = manageSecurity;
        this.displayColumns = [
            'action',
            'tenant_name',
            'is_multiple_sftp_definition'
        ];
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__.SelectionModel(false, []);
        this.hide = true;
        this.companydata = [];
        this.tbl_FilteredData = [];
        this.companyServerLinkData = [];
        this.BTN_VAL = 'Submit';
        this.companyIdCompanyMst = [];
        this.storageTypeData = [];
        this.is_different_location_checked = false;
        this.list_form = true;
        this.list_title = true;
        this.btn_new_entry = true;
        this.btn_list = true;
        this.new_entry_form = true;
        this.new_entry_title = true;
        this.submit = true;
        this.submitted = false;
        this.isDisabled = true;
        this.Updated_Date_Time = new Date();
    }
    ngOnInit() {
        this.USERID = Number(localStorage.getItem('user_id'));
        this.created_by = Number(localStorage.getItem('user_id'));
        this.CompanySourceCloudLinkingForm = this.formBuilder.group({
            tenant_id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
            tenant_name: [],
            is_multiple_sftp_definition: false,
            id: [''],
            is_active: true,
            is_deleted: false,
            created_by: this.created_by,
            updated_by: this.created_by,
            updated_date_time: [this.Updated_Date_Time],
            initialItemRow: this.formBuilder.array([this.initialItemRow()]),
        });
        this.new_entry_form = false;
        this.new_entry_title = false;
        this.btn_list = false;
        this.companydata = [];
        this.cedgeService.getSftpServerData().subscribe((data) => {
            this.companyServerLinkData = data;
            this.companyServerLinkMasterData = new _angular_material_table__WEBPACK_IMPORTED_MODULE_7__.MatTableDataSource(data);
            this.companyServerLinkMasterData.paginator = this.SecureServerPaginator;
            this.companyServerLinkMasterData.sort = this.sort;
            this.companyServerLinkMasterData.connect().subscribe(d => this.renderedData = d);
            this.tbl_FilteredData = data;
        });
        this.cedgeService.getAllTenantHierarchyDefine().subscribe((res) => {
            res.forEach(ele => {
                if (ele.tenant_type_name == 'Member Bank') {
                    this.cedgeService.getApexTenant(ele.id).subscribe((data) => {
                        this.apex_tenant = data[0];
                        if (Number(localStorage.tenant_ref_id) == this.apex_tenant.id && Number(localStorage.user_type_id) == this.apex_tenant.tenant_type_ref_id) {
                            this.CompanySourceCloudLinkingForm.get('tenant_id').setValue(this.apex_tenant['id']);
                            this.CompanySourceCloudLinkingForm.get('tenant_name').setValue(this.apex_tenant['tenant_name']);
                        }
                    });
                }
            });
        });
        this.manageSecurity.getAccessLeftPanel(this.USERID, 'Tenant Hierarchy Define').subscribe(data => {
            this.sidebarData = data;
        });
    }
    showList() {
        this.list_form = true;
        this.list_title = true;
        this.btn_new_entry = true;
        this.btn_list = false;
        this.new_entry_form = false;
        this.new_entry_title = false;
    }
    showNewEntry() {
        this.list_form = false;
        this.list_title = false;
        this.btn_new_entry = false;
        this.btn_list = true;
        this.new_entry_form = true;
        this.new_entry_title = true;
    }
    tbl_FilterDatatable(value) {
        this.companyServerLinkMasterData.filter = value.trim().toLocaleLowerCase();
    }
    isDifferentLocation(e) {
        if (e.checked == false) {
            this.isDisabled = true;
            this.CompanySourceCloudLinkingForm = this.formBuilder.group({
                tenant_id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
                tenant_name: [],
                is_multiple_sftp_definition: false,
                id: [''],
                is_active: true,
                is_deleted: false,
                created_by: this.created_by,
                updated_by: this.created_by,
                updated_date_time: [this.Updated_Date_Time],
                initialItemRow: this.formBuilder.array([this.initialItemRow()]),
            });
        }
        else if (e.checked == true) {
            this.isDisabled = false;
        }
    }
    onSubmit() {
        if (this.CompanySourceCloudLinkingForm.invalid) {
            return;
        }
        else {
            this.cedgeService.saveSftpServer(this.CompanySourceCloudLinkingForm.value).subscribe({
                next: (data) => {
                    if (data['status'] == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: 'Your record has been added successfully!',
                            icon: 'success',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                    if (data['status'] == 2) {
                        this.companyServerLinkData[this.companyServerLinkData.findIndex(item => item.id == data['id'])] = data;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: 'Your record has been updated successfully!',
                            icon: 'success',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                    this.router.navigate(['/storage-configuration/define-server']).then(() => {
                        setTimeout(function () { window.location.reload(); }, 2000);
                    });
                },
                error: (error) => {
                    error = JSON.parse(error);
                    if (error.status == '400' && error.error.status == "Only one active record allowed") {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: 'Only a single record should be active',
                            icon: 'warning',
                            timer: 5000,
                            showConfirmButton: false
                        });
                        return;
                    }
                    else if (error.status == '400') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: error.error,
                            icon: 'warning',
                            timer: 4000,
                            showConfirmButton: false
                        });
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: "Something went wrong",
                            icon: 'error',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                }
            });
        }
    }
    initialItemRow() {
        return this.formBuilder.group({
            id: '',
            sftp_name: '',
            host_name: '',
            sftp_username: '',
            sftp_password: '',
            file_key_path: '',
            is_active: true,
            is_deleted: false,
            created_by: [this.created_by],
            updated_by: this.created_by,
            updated_date_time: [this.Updated_Date_Time]
        });
    }
    addNewRow(i) {
        document.getElementById('add_button').disabled = false;
        this.formArr.push(this.initialItemRow());
    }
    deleteRow(index) {
        if (this.formArr.length == 1) {
            return false;
        }
        else {
            this.formArr.removeAt(index);
            return true;
        }
    }
    get formArr() {
        return this.CompanySourceCloudLinkingForm.get('initialItemRow');
    }
    setExistingArray(initialArray = []) {
        const formArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormArray([]);
        initialArray.forEach(element => {
            formArray.push(this.formBuilder.group({
                id: element.id,
                host_name: element.host_name,
                sftp_name: element.sftp_name,
                sftp_username: element.sftp_username,
                sftp_password: element.sftp_password,
                file_key_path: element.file_key_path,
                is_active: element.is_active,
                is_deleted: element.is_deleted,
                folder_name: element.folder_name,
                created_by: [this.created_by],
                updated_by: this.created_by,
                updated_date_time: [this.Updated_Date_Time]
            }));
        });
        return formArray;
    }
    viewRecord(payload) {
        this.CompanySourceCloudLinkingForm.patchValue({
            tenant_id: this.apex_tenant['id'],
            tenant_name: this.apex_tenant['tenant_name'],
            id: payload.id,
            is_active: payload.is_active,
            is_deleted: payload.is_deleted,
            is_multiple_sftp_definition: payload.is_multiple_sftp_definition,
            updated_date_time: new Date(),
            updated_by: this.created_by
        });
        this.newSource = payload.initialItemRow.filter(function (data) {
            return data.is_deleted == false;
        });
        this.CompanySourceCloudLinkingForm.setControl('initialItemRow', this.setExistingArray(this.newSource));
        const id = this.CompanySourceCloudLinkingForm.get('id').value;
        this.companyIdCompanyMst = this.companydata;
        if (id !== '') {
            this.new_entry_form = true;
            this.new_entry_title = true;
            this.btn_list = true;
            this.btn_new_entry = false;
            this.list_form = false;
            this.list_title = false;
            this.submit = false;
        }
    }
    editRecord(payload) {
        this.BTN_VAL = 'Update';
        this.CompanySourceCloudLinkingForm.patchValue({
            tenant_id: this.apex_tenant['id'],
            tenant_name: this.apex_tenant['tenant_name'],
            id: payload.id,
            is_active: payload.is_active,
            is_deleted: payload.is_deleted,
            is_multiple_sftp_definition: payload.is_multiple_sftp_definition,
            updated_date_time: new Date(),
            updated_by: this.created_by
        });
        this.newSource = payload.initialItemRow.filter(function (data) {
            return data.is_deleted == false;
        });
        this.CompanySourceCloudLinkingForm.setControl('initialItemRow', this.setExistingArray(this.newSource));
        const id = this.CompanySourceCloudLinkingForm.get('id').value;
        this.companyIdCompanyMst = this.companydata;
        if (id !== '') {
            this.new_entry_form = true;
            this.new_entry_title = true;
            this.btn_list = true;
            this.btn_new_entry = false;
            this.list_form = false;
            this.list_title = false;
            this.submit = true;
        }
    }
    export_isp_usrs() {
        var XlsMasterData = this.companyServerLinkMasterData.data.map(({ tenant_name }) => ({ tenant_name }));
        const workSheet = xlsx__WEBPACK_IMPORTED_MODULE_8__.utils.json_to_sheet(XlsMasterData);
        const workBook = xlsx__WEBPACK_IMPORTED_MODULE_8__.utils.book_new();
        xlsx__WEBPACK_IMPORTED_MODULE_8__.utils.book_append_sheet(workBook, workSheet, 'SheetName');
        xlsx__WEBPACK_IMPORTED_MODULE_8__.writeFile(workBook, 'Define Server.xlsx');
    }
    addNewLink() {
        var companyIdSecure = this.companyServerLinkData.map(data => data.tenant_id);
        var companyIdCompany = this.companydata.map(data => data.id);
        var missingCompanyId = companyIdCompany.filter(e => !companyIdSecure.includes(e));
        this.companyIdCompanyMst = this.companydata.filter(e => missingCompanyId.includes(e.id));
    }
    changeState(event) {
        if (!event.checked) {
            this.CompanySourceCloudLinkingForm.get('is_deleted').setValue(true);
            this.formArr.value.forEach(element => {
                element.is_active = false;
                element.is_deleted = true;
            });
        }
        else {
            this.CompanySourceCloudLinkingForm.get('is_deleted').setValue(false);
            this.formArr.value.forEach(element => {
                element.is_active = true;
                element.is_deleted = false;
            });
        }
        console.log(this.CompanySourceCloudLinkingForm);
    }
};
DefineServerComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: src_app_core_service_cedge_api_service_service__WEBPACK_IMPORTED_MODULE_3__.CedgeApiServiceService },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_4__.ManageSecurityService }
];
DefineServerComponent.propDecorators = {
    SecureServerPaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_10__.ViewChild, args: ['SecureServerPaginator', { read: _angular_material_paginator__WEBPACK_IMPORTED_MODULE_11__.MatPaginator },] }],
    sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_10__.ViewChild, args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_12__.MatSort, { static: true },] }]
};
DefineServerComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_13__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_10__.Component)({
        selector: 'app-define-server',
        template: _define_server_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        styles: [_define_server_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], DefineServerComponent);



/***/ }),

/***/ 2154:
/*!************************************************************************************!*\
  !*** ./src/app/storage-server-conf/schedule-process/schedule-process.component.ts ***!
  \************************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "ScheduleProcessComponent": () => (/* binding */ ScheduleProcessComponent)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _schedule_process_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./schedule-process.component.html?ngResource */ 2187);
/* harmony import */ var _schedule_process_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./schedule-process.component.sass?ngResource */ 3605);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_sort__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/sort */ 4316);
/* harmony import */ var _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/cdk/collections */ 9502);
/* harmony import */ var xlsx__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! xlsx */ 4126);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! sweetalert2 */ 598);
/* harmony import */ var sweetalert2__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(sweetalert2__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var src_app_core_service_cedge_api_service_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/core/service/cedge-api-service.service */ 2636);
/* harmony import */ var src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/core/service/manage-security.service */ 3695);















let ScheduleProcessComponent = class ScheduleProcessComponent {
    constructor(router, formBuilder, cedgeService, datepipe, manageSecurity) {
        this.router = router;
        this.formBuilder = formBuilder;
        this.cedgeService = cedgeService;
        this.datepipe = datepipe;
        this.manageSecurity = manageSecurity;
        this.displayColumns = [
            'actions',
            'company_name',
            'is_active'
        ];
        this.selection = new _angular_cdk_collections__WEBPACK_IMPORTED_MODULE_5__.SelectionModel(false, []);
        this.hide = true;
        this.companydata = [];
        this.tbl_FilteredData = [];
        this.companyServerLinkData = [];
        this.BTN_VAL = 'Submit';
        this.companyIdCompanyMst = [];
        this.storageTypeData = [];
        this.is_different_location_checked = false;
        this.list_form = true;
        this.list_title = true;
        this.btn_new_entry = true;
        this.btn_list = true;
        this.new_entry_form = true;
        this.new_entry_title = true;
        this.submit = true;
        this.submitted = false;
        this.getCompanytoSearch = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormControl();
        this.isDisabled = true;
        this.scheduleProcessData = [];
        this.todayDate = new Date().toJSON().split('T')[0];
        this.Updated_Date_Time = new Date();
    }
    ngOnInit() {
        this.USERID = Number(localStorage.getItem('user_id'));
        this.created_by = Number(localStorage.getItem('user_id'));
        this.CompanySourceCloudLinkingForm = this.formBuilder.group({
            tenant_id: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_6__.Validators.required],
            tenant_name: [],
            from_date: [],
            to_date: [],
            id: [''],
            is_active: true,
            is_deleted: false,
            created_by: this.created_by,
            updated_by: this.created_by,
            updated_date_time: [this.Updated_Date_Time],
            initialItemRow: this.formBuilder.array([this.initialItemRow()]),
        });
        this.new_entry_form = false;
        this.new_entry_title = false;
        this.btn_list = false;
        this.companydata = [];
        this.cedgeService.getScheduleProcessData().subscribe((data) => {
            this.companyServerLinkData = data;
            this.companyServerLinkMasterData = new _angular_material_table__WEBPACK_IMPORTED_MODULE_7__.MatTableDataSource(data);
            this.companyServerLinkMasterData.paginator = this.SecureServerPaginator;
            this.companyServerLinkMasterData.sort = this.sort;
            this.companyServerLinkMasterData.connect().subscribe(d => this.renderedData = d);
            this.tbl_FilteredData = data;
        });
        this.cedgeService.getAllTenantHierarchyDefine().subscribe((res) => {
            res.forEach(ele => {
                if (ele.tenant_type_name == 'Member Bank') {
                    this.cedgeService.getApexTenant(ele.id).subscribe((data) => {
                        this.apex_tenant = data[0];
                        if (Number(localStorage.tenant_ref_id) == this.apex_tenant.id && Number(localStorage.user_type_id) == this.apex_tenant.tenant_type_ref_id) {
                            this.CompanySourceCloudLinkingForm.get('tenant_id').setValue(this.apex_tenant['id']);
                            this.CompanySourceCloudLinkingForm.get('tenant_name').setValue(this.apex_tenant['tenant_name']);
                        }
                    });
                }
            });
        });
        this.cedgeService.getMasterUsingType('Process Type').subscribe((data) => {
            this.scheduleProcessData = data;
        });
        this.manageSecurity.getAccessLeftPanel(this.USERID, 'Tenant Hierarchy Define').subscribe(data => {
            this.sidebarData = data;
        });
    }
    showList() {
        this.list_form = true;
        this.list_title = true;
        this.btn_new_entry = true;
        this.btn_list = false;
        this.new_entry_form = false;
        this.new_entry_title = false;
    }
    showNewEntry() {
        this.list_form = false;
        this.list_title = false;
        this.btn_new_entry = false;
        this.btn_list = true;
        this.new_entry_form = true;
        this.new_entry_title = true;
    }
    tbl_FilterDatatable(value) {
        this.companyServerLinkMasterData.filter = value.trim().toLocaleLowerCase();
    }
    onSubmit() {
        this.CompanySourceCloudLinkingForm.value.from_date = this.datepipe.transform(this.CompanySourceCloudLinkingForm.value.from_date, "yyyy-MM-dd");
        this.CompanySourceCloudLinkingForm.value.to_date = this.datepipe.transform(this.CompanySourceCloudLinkingForm.value.to_date, "yyyy-MM-dd");
        if (this.CompanySourceCloudLinkingForm.invalid) {
            return;
        }
        else {
            this.cedgeService.saveScheduleProcess(this.CompanySourceCloudLinkingForm.value).subscribe({
                next: (data) => {
                    if (data['status'] == 1) {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: 'Your record has been added successfully!',
                            icon: 'success',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                    if (data['status'] == 2) {
                        this.companyServerLinkData[this.companyServerLinkData.findIndex(item => item.id == data['id'])] = data;
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: 'Your record has been updated successfully!',
                            icon: 'success',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                    this.router.navigate(['/storage-configuration/schedule-process']).then(() => {
                        setTimeout(function () { window.location.reload(); }, 2000);
                    });
                },
                error: (error) => {
                    error = JSON.parse(error);
                    if (error.status == '400' && error.error.status == "Only one active record allowed") {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: 'Only a single record should be active',
                            icon: 'warning',
                            timer: 2000,
                            showConfirmButton: false
                        });
                        return;
                    }
                    else if (error.status == '400' && error.error.status == "Process Type Already Taken") {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: 'Process Type Already Taken',
                            icon: 'warning',
                            timer: 2000,
                            showConfirmButton: false
                        });
                        return;
                    }
                    else if (error.status == '400') {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: error.error,
                            icon: 'warning',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                    else {
                        sweetalert2__WEBPACK_IMPORTED_MODULE_2___default().fire({
                            title: "Something went wrong",
                            icon: 'error',
                            timer: 2000,
                            showConfirmButton: false
                        });
                    }
                }
            });
        }
    }
    initialItemRow() {
        return this.formBuilder.group({
            id: '',
            process_name: '',
            process_start_time: '',
            process_id: '',
            is_active: true,
            is_deleted: false,
            created_by: [this.created_by],
            updated_by: this.created_by,
            updated_date_time: [this.Updated_Date_Time]
        });
    }
    addNewRow(i) {
        document.getElementById('add_button').disabled = false;
        this.formArr.push(this.initialItemRow());
    }
    deleteRow(index) {
        if (this.formArr.length == 1) {
            return false;
        }
        else {
            this.formArr.removeAt(index);
            return true;
        }
    }
    get formArr() {
        return this.CompanySourceCloudLinkingForm.get('initialItemRow');
    }
    setExistingArray(initialArray = []) {
        const formArray = new _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormArray([]);
        initialArray.forEach(element => {
            formArray.push(this.formBuilder.group({
                id: element.id,
                process_name: element.process_name,
                process_start_time: element.process_start_time,
                process_id: element.process_id,
                is_active: element.is_active,
                is_deleted: element.is_deleted,
                created_by: [this.created_by],
                updated_by: this.created_by,
                updated_date_time: [this.Updated_Date_Time]
            }));
        });
        return formArray;
    }
    viewRecord(payload) {
        this.CompanySourceCloudLinkingForm.patchValue({
            tenant_id: this.apex_tenant['id'],
            tenant_name: this.apex_tenant['tenant_name'],
            from_date: payload.from_date,
            to_date: payload.to_date,
            id: payload.id,
            is_active: payload.is_active,
            is_deleted: payload.is_deleted,
            updated_date_time: new Date(),
            updated_by: this.created_by
        });
        this.newSource = payload.initialItemRow.filter(function (data) {
            return data.is_deleted == false;
        });
        this.CompanySourceCloudLinkingForm.setControl('initialItemRow', this.setExistingArray(this.newSource));
        const id = this.CompanySourceCloudLinkingForm.get('id').value;
        this.companyIdCompanyMst = this.companydata;
        if (id !== '') {
            this.new_entry_form = true;
            this.new_entry_title = true;
            this.btn_list = false;
            this.btn_new_entry = false;
            this.list_form = false;
            this.list_title = false;
            this.submit = false;
        }
    }
    editRecord(payload) {
        this.BTN_VAL = 'Update';
        this.CompanySourceCloudLinkingForm.patchValue({
            tenant_id: this.apex_tenant['id'],
            tenant_name: this.apex_tenant['tenant_name'],
            from_date: new Date(payload.from_date),
            to_date: new Date(payload.to_date),
            id: payload.id,
            is_active: payload.is_active,
            is_deleted: payload.is_deleted,
            is_multiple_sftp_definition: payload.is_multiple_sftp_definition,
            updated_date_time: new Date(),
            updated_by: this.created_by
        });
        console.log(this.CompanySourceCloudLinkingForm);
        this.newSource = payload.initialItemRow.filter(function (data) {
            return data.is_deleted == false;
        });
        this.CompanySourceCloudLinkingForm.setControl('initialItemRow', this.setExistingArray(this.newSource));
        const id = this.CompanySourceCloudLinkingForm.get('id').value;
        this.companyIdCompanyMst = this.companydata;
        if (id !== '') {
            this.new_entry_form = true;
            this.new_entry_title = true;
            this.btn_list = false;
            this.btn_new_entry = false;
            this.list_form = false;
            this.list_title = false;
            this.submit = true;
        }
    }
    export_isp_usrs() {
        var XlsMasterData = this.companyServerLinkMasterData.data.map(({ tenant_name }) => ({ tenant_name }));
        const workSheet = xlsx__WEBPACK_IMPORTED_MODULE_8__.utils.json_to_sheet(XlsMasterData);
        const workBook = xlsx__WEBPACK_IMPORTED_MODULE_8__.utils.book_new();
        xlsx__WEBPACK_IMPORTED_MODULE_8__.utils.book_append_sheet(workBook, workSheet, 'SheetName');
        xlsx__WEBPACK_IMPORTED_MODULE_8__.writeFile(workBook, 'Schedule Process.xlsx');
    }
    addNewLink() {
        var companyIdSecure = this.companyServerLinkData.map(data => data.tenant_id);
        var companyIdCompany = this.companydata.map(data => data.id);
        var missingCompanyId = companyIdCompany.filter(e => !companyIdSecure.includes(e));
        this.companyIdCompanyMst = this.companydata.filter(e => missingCompanyId.includes(e.id));
    }
    changeState(event) {
        if (!event.checked) {
            this.CompanySourceCloudLinkingForm.get('is_deleted').setValue(true);
            this.formArr.value.forEach(element => {
                element.is_active = false;
                element.is_deleted = true;
            });
        }
        else {
            this.CompanySourceCloudLinkingForm.get('is_deleted').setValue(false);
            this.formArr.value.forEach(element => {
                element.is_active = true;
                element.is_deleted = false;
            });
        }
        console.log(this.CompanySourceCloudLinkingForm);
    }
};
ScheduleProcessComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_9__.Router },
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormBuilder },
    { type: src_app_core_service_cedge_api_service_service__WEBPACK_IMPORTED_MODULE_3__.CedgeApiServiceService },
    { type: _angular_common__WEBPACK_IMPORTED_MODULE_10__.DatePipe },
    { type: src_app_core_service_manage_security_service__WEBPACK_IMPORTED_MODULE_4__.ManageSecurityService }
];
ScheduleProcessComponent.propDecorators = {
    SecureServerPaginator: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_11__.ViewChild, args: ['SecureServerPaginator', { read: _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__.MatPaginator },] }],
    sort: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_11__.ViewChild, args: [_angular_material_sort__WEBPACK_IMPORTED_MODULE_13__.MatSort, { static: true },] }]
};
ScheduleProcessComponent = (0,tslib__WEBPACK_IMPORTED_MODULE_14__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_11__.Component)({
        selector: 'app-schedule-process',
        template: _schedule_process_component_html_ngResource__WEBPACK_IMPORTED_MODULE_0__,
        providers: [_angular_common__WEBPACK_IMPORTED_MODULE_10__.DatePipe],
        styles: [_schedule_process_component_sass_ngResource__WEBPACK_IMPORTED_MODULE_1__]
    })
], ScheduleProcessComponent);



/***/ }),

/***/ 4418:
/*!***************************************************************************!*\
  !*** ./src/app/storage-server-conf/storage-server-conf-routing.module.ts ***!
  \***************************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StorageServerConfRoutingModule": () => (/* binding */ StorageServerConfRoutingModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ 2816);
/* harmony import */ var _bank_strorage_link_bank_strorage_link_component__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./bank-strorage-link/bank-strorage-link.component */ 2458);
/* harmony import */ var _define_server_define_server_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./define-server/define-server.component */ 1922);
/* harmony import */ var _schedule_process_schedule_process_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./schedule-process/schedule-process.component */ 2154);






const routes = [
    {
        path: 'bank-storage-link',
        component: _bank_strorage_link_bank_strorage_link_component__WEBPACK_IMPORTED_MODULE_0__.BankStrorageLinkComponent
    },
    {
        path: 'define-server',
        component: _define_server_define_server_component__WEBPACK_IMPORTED_MODULE_1__.DefineServerComponent
    },
    {
        path: 'schedule-process',
        component: _schedule_process_schedule_process_component__WEBPACK_IMPORTED_MODULE_2__.ScheduleProcessComponent
    },
];
let StorageServerConfRoutingModule = class StorageServerConfRoutingModule {
};
StorageServerConfRoutingModule = (0,tslib__WEBPACK_IMPORTED_MODULE_3__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_4__.NgModule)({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule.forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_5__.RouterModule]
    })
], StorageServerConfRoutingModule);



/***/ }),

/***/ 7319:
/*!*******************************************************************!*\
  !*** ./src/app/storage-server-conf/storage-server-conf.module.ts ***!
  \*******************************************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "StorageServerConfModule": () => (/* binding */ StorageServerConfModule)
/* harmony export */ });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! tslib */ 4929);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/core */ 3184);
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/common */ 6362);
/* harmony import */ var _angular_material_select__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/material/select */ 1434);
/* harmony import */ var _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/material/slide-toggle */ 6623);
/* harmony import */ var _angular_material_input__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/material/input */ 3365);
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ 587);
/* harmony import */ var _angular_material_table__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/material/table */ 7217);
/* harmony import */ var _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @angular/material/paginator */ 6439);
/* harmony import */ var _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @angular/material/datepicker */ 5818);
/* harmony import */ var _angular_material_core__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @angular/material/core */ 8133);
/* harmony import */ var _storage_server_conf_routing_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./storage-server-conf-routing.module */ 4418);
/* harmony import */ var _bank_strorage_link_bank_strorage_link_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./bank-strorage-link/bank-strorage-link.component */ 2458);
/* harmony import */ var _define_server_define_server_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./define-server/define-server.component */ 1922);
/* harmony import */ var _schedule_process_schedule_process_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./schedule-process/schedule-process.component */ 2154);















let StorageServerConfModule = class StorageServerConfModule {
};
StorageServerConfModule = (0,tslib__WEBPACK_IMPORTED_MODULE_4__.__decorate)([
    (0,_angular_core__WEBPACK_IMPORTED_MODULE_5__.NgModule)({
        declarations: [
            _bank_strorage_link_bank_strorage_link_component__WEBPACK_IMPORTED_MODULE_1__.BankStrorageLinkComponent,
            _define_server_define_server_component__WEBPACK_IMPORTED_MODULE_2__.DefineServerComponent,
            _schedule_process_schedule_process_component__WEBPACK_IMPORTED_MODULE_3__.ScheduleProcessComponent
        ],
        imports: [
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.FormsModule,
            _angular_forms__WEBPACK_IMPORTED_MODULE_6__.ReactiveFormsModule,
            _angular_common__WEBPACK_IMPORTED_MODULE_7__.CommonModule,
            _storage_server_conf_routing_module__WEBPACK_IMPORTED_MODULE_0__.StorageServerConfRoutingModule,
            _angular_material_select__WEBPACK_IMPORTED_MODULE_8__.MatSelectModule,
            _angular_material_slide_toggle__WEBPACK_IMPORTED_MODULE_9__.MatSlideToggleModule,
            _angular_material_input__WEBPACK_IMPORTED_MODULE_10__.MatInputModule,
            _angular_material_table__WEBPACK_IMPORTED_MODULE_11__.MatTableModule,
            _angular_material_paginator__WEBPACK_IMPORTED_MODULE_12__.MatPaginatorModule,
            _angular_material_core__WEBPACK_IMPORTED_MODULE_13__.MatNativeDateModule,
            _angular_material_datepicker__WEBPACK_IMPORTED_MODULE_14__.MatDatepickerModule
        ]
    })
], StorageServerConfModule);



/***/ }),

/***/ 140:
/*!*****************************************************************************************************!*\
  !*** ./src/app/storage-server-conf/bank-strorage-link/bank-strorage-link.component.sass?ngResource ***!
  \*****************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJiYW5rLXN0cm9yYWdlLWxpbmsuY29tcG9uZW50LnNhc3MifQ== */";

/***/ }),

/***/ 2790:
/*!*******************************************************************************************!*\
  !*** ./src/app/storage-server-conf/define-server/define-server.component.sass?ngResource ***!
  \*******************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJkZWZpbmUtc2VydmVyLmNvbXBvbmVudC5zYXNzIn0= */";

/***/ }),

/***/ 3605:
/*!*************************************************************************************************!*\
  !*** ./src/app/storage-server-conf/schedule-process/schedule-process.component.sass?ngResource ***!
  \*************************************************************************************************/
/***/ ((module) => {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzY2hlZHVsZS1wcm9jZXNzLmNvbXBvbmVudC5zYXNzIn0= */";

/***/ }),

/***/ 7509:
/*!*****************************************************************************************************!*\
  !*** ./src/app/storage-server-conf/bank-strorage-link/bank-strorage-link.component.html?ngResource ***!
  \*****************************************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"row clearfix\">\r\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                <div class=\"card\">\r\n                    <div class=\"row clearfix\">\r\n\r\n                        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"list_title\">\r\n                            <div class=\"header\">\r\n                                <h2>\r\n                                    <strong>List of Member Bank Storage link</strong>\r\n                                </h2>\r\n                            </div>\r\n                        </div>\r\n                        <ng-container>\r\n                            <!-- <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" id=\"btn_new_entry\" -->\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"btn_new_entry\"\r\n                                style=\"text-align: right;\">\r\n                                <div class=\"header\">\r\n                                    <h2>\r\n                                        <button class=\"btn btn-primary btn-rounded waves-effect\" type=\"button\"\r\n                                            (click)=\"showNewEntry();addNewLink()\"><i class=\"fa  fa-plus\"></i>Add New\r\n                                            Link</button>\r\n                                    </h2>\r\n                                </div>\r\n                            </div>\r\n                        </ng-container>\r\n                        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"new_entry_title\">\r\n                            <div class=\"header\">\r\n                                <h2>\r\n                                    <strong>Member Bank Storage link</strong>\r\n                                </h2>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"btn_list\" style=\"text-align: right;\">\r\n                            <div class=\"header\">\r\n                                <h2>\r\n                                    <button class=\"btn btn-primary btn-rounded waves-effect\" type=\"button\"\r\n                                        (click)=\"showList()\"><i class=\"fa fa-calendar\"></i>List of Links</button>\r\n                                </h2>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"new_entry_form\">\r\n            <div class=\"row clearfix\">\r\n                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                    <form [formGroup]=\"CompanySourceCloudLinkingForm\" (ngSubmit)=\"onSubmit()\">\r\n                        <div class=\"card\">\r\n                            <div class=\"body\">\r\n                                <div class=\"row clearfix\">\r\n                                    <input type=\"hidden\" formControlName=\"id\" id=\"id\" [value]=\"\" class=\"form-control\">\r\n                                    <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                        <div class=\"form-group\">\r\n                                            <mat-label>Tenant Name <i style=\"color: red;\">*</i></mat-label>\r\n                                            <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                <input matInput formControlName=\"tenant_id\" hidden>\r\n                                                <input matInput formControlName=\"tenant_name\" placeholder=\"Tenant Name\"\r\n                                                    readonly>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                        <div class=\"form-group\">\r\n                                            <!-- <label class=\"form-label\">All Sources will have Different Cloud Storage?\r\n                                            </label><br>\r\n                                            <label class=\"form-label\">No &nbsp;</label>\r\n                                            <mat-slide-toggle id=\"is_different_location\"\r\n                                                [checked]=\"is_different_location\"\r\n                                                formControlName=\"is_different_location\"\r\n                                                (change)=\"isDifferentLocation($event)\"> Yes</mat-slide-toggle> -->\r\n\r\n                                            <label class=\"form-label\">All Sources will have Different Cloud\r\n                                                Storage?</label><br>\r\n                                            <mat-label>No &nbsp;</mat-label>\r\n                                            <mat-slide-toggle id=\"is_different_location\"\r\n                                                (change)=\"isDifferentLocation($event)\"\r\n                                                formControlName=\"is_different_location\">Yes</mat-slide-toggle>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                        <div class=\"form-group\">\r\n                                            <label class=\"form-label\">Is Active?</label><br>\r\n                                            <mat-label>No &nbsp;</mat-label>\r\n                                            <mat-slide-toggle id=\"is_active\" (change)=\"changeState($event)\"\r\n                                                formControlName=\"is_active\">Yes</mat-slide-toggle>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"card\">\r\n                            <div class=\"body\">\r\n                                <h5 style=\"text-align: center;\"><strong></strong></h5><br>\r\n                                <div class=\"table-responsive\" formArrayName=\"initialItemRow\">\r\n                                    <table id=\"tableExport1\"\r\n                                        class=\"display table table-hover table-checkable order-column m-t-20 width-per-100 table-bordered\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th>Bank Name <label style=\"color: red; font-size: smaller;\">*</label>\r\n                                                </th>\r\n                                                <th>SFTP Name <label style=\"color: red; font-size: smaller;\">*</label>\r\n                                                </th>\r\n                                                <!-- <th>Cloud Storage Type <label style=\"color: red; font-size: smaller;\">*</label></th> -->\r\n                                                <th>Folder Path<label style=\"color: red; font-size: smaller;\">*</label>\r\n                                                </th>\r\n                                                <th>Is Active<label style=\"font-size: smaller;\">*</label></th>\r\n                                            </tr>\r\n                                        </thead>\r\n\r\n                                        <tbody class=\"main_tbody\">\r\n\r\n                                            <tr *ngFor=\"let dynamic of CompanySourceCloudLinkingForm.controls.initialItemRow['controls']; let i = index\"\r\n                                                [formGroupName]=\"i\">\r\n\r\n                                                <td class=\"col_input\">\r\n                                                    <input class=\"form-control\" type=\"text\" placeholder=\"\"\r\n                                                        id=\"member_bank_tenant_name{{i+1}}\"\r\n                                                        formControlName=\"member_bank_tenant_name\" style=\"width:300px;\"\r\n                                                        required readonly>\r\n                                                    <input class=\"form-control\" type=\"hidden\" placeholder=\"\"\r\n                                                        id=\"member_bank_tenant_id{{i+1}}\"\r\n                                                        formControlName=\"member_bank_tenant_id\" style=\"width:300px;\"\r\n                                                        required readonly>\r\n                                                </td>\r\n\r\n                                                <td>\r\n                                                    <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                        <mat-select formControlName=\"sftp_ref_id\">\r\n                                                            <mat-option value=\"\">Select</mat-option>\r\n                                                            <mat-option [value]=\"obj.id\"\r\n                                                                *ngFor=\"let obj of sftpData\">{{obj.sftp_name}}</mat-option>\r\n                                                        </mat-select>\r\n                                                    </mat-form-field>\r\n                                                </td>\r\n\r\n                                                <!-- <td class=\"col_input\">\r\n                                                    <select class=\"form-select\" id=\"cloud_storage_type{{i+1}}\" formControlName=\"cloud_storage_type\" style=\"width:230px;\" required>\r\n                                                        <option value=\"\">Select Cloud Storage Type</option>\r\n                                                        <option [value]=\"data.id\"\r\n                                                            *ngFor=\"let data of storageTypeData\">\r\n                                                            {{data.master_key}}</option>\r\n                                                    </select>\r\n                                                </td> -->\r\n\r\n                                                <td class=\"col_input\">\r\n                                                    <input class=\"form-control\" type=\"text\" id=\"folder_name{{i+1}}\"\r\n                                                        formControlName=\"folder_name\" style=\"width:130px;\" required>\r\n                                                </td>\r\n\r\n                                                <td>\r\n                                                    <div class=\"col-6\">\r\n                                                        <mat-slide-toggle formControlName=\"is_active\"\r\n                                                            id=\"is_active{{i+1}}\" [checked]=\"selection.isSelected(i)\">\r\n                                                        </mat-slide-toggle>\r\n                                                    </div>\r\n                                                </td>\r\n\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"card\">\r\n                            <div class=\"body\" style=\"text-align: center;\">\r\n                                <div class=\"button-demo\">\r\n                                    <button type=\"submit\" *ngIf=\"submit\"\r\n                                        [disabled]=\"!CompanySourceCloudLinkingForm.valid\"\r\n                                        class=\"btn btn-primary btn-border-radius waves-effect\">{{BTN_VAL}}</button>\r\n                                    <a mat-raised-button onclick=\"location.reload(true);\"\r\n                                        class=\"btn btn-danger\">Cancel</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </form>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <!-- List Page -->\r\n        <div *ngIf=\"list_form\">\r\n            <div class=\"row clearfix\">\r\n                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                    <div class=\"card\">\r\n                        <div class=\"body\">\r\n                            <div class=\"table-responsive\">\r\n                                <div class=\"row clearfix\">\r\n                                    <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                                        <div class=\"dt-buttons btn-group flex-wrap\">\r\n                                            <button class=\"btn btn-secondary buttons-copy buttons-html5\" tabindex=\"0\"\r\n                                                aria-controls=\"example1\" type=\"button\" (click)=\"export_isp_usrs()\">\r\n                                                <span>Excel</span>\r\n                                            </button>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\"text-align: right;\">\r\n                                        <mat-form-field appearance=\"fill\">\r\n                                            <mat-label>\r\n                                                <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\r\n                                                Search\r\n                                            </mat-label>\r\n                                            <input matInput (keyup)='tbl_FilterDatatable($event.target.value)'>\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <mat-table #table [dataSource]=\"companyServerLinkMasterData\" matSort class=\"mat-cell\">\r\n                                    <ng-container matColumnDef=\"actions\">\r\n                                        <mat-header-cell *matHeaderCellDef\r\n                                            style=\"color: white;\">Actions</mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row;\">\r\n                                            <span *ngFor=\"let item of sidebarData\">\r\n                                                <button *ngIf=\"item.read_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                                    (click)=\"viewRecord(row)\">\r\n                                                    <em class=\"material-icons\">visibility</em>\r\n                                                </button>\r\n                                                <button *ngIf=\"item.write_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                                    (click)=\"editRecord(row)\">\r\n                                                    <em class=\"material-icons\">mode_edit</em>\r\n                                                </button>\r\n                                            </span>\r\n                                            <!-- <span *ngFor=\"let item of sidebarData\"> -->\r\n                                            <!-- <button class=\"btn tblActnBtn h-auto\" data-toggle=\"modal\" data-target=\"#editModal\" (click)=\"viewCompanyData(row)\">\r\n                                                    <i class=\"material-icons\">visibility</i>\r\n                                                </button>\r\n                                                <button class=\"btn tblActnBtn h-auto\" data-toggle=\"modal\" data-target=\"#editModal\" (click)=\"editCompanyData(row)\">\r\n                                                    <i class=\"material-icons\">mode_edit</i>\r\n                                                </button>\r\n                                                <button class=\"btn tblActnBtn h-auto\" (click)=\"deleteCompanyData(row.id)\">\r\n                                                    <i class=\"material-icons\">delete</i>\r\n                                                </button> -->\r\n                                            <!-- </span> -->\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"tenant_name\">\r\n                                        <mat-header-cell mat-header-cell *matHeaderCellDef style=\"color: white;\">Tenant\r\n                                            Name</mat-header-cell>\r\n                                        <mat-cell mat-cell *matCellDef=\"let row\"> {{row.tenant_name}}</mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"is_different_location\">\r\n                                        <mat-header-cell mat-header-cell *matHeaderCellDef style=\"color: white;\">Is\r\n                                            Different Location</mat-header-cell>\r\n                                        <mat-cell mat-cell *matCellDef=\"let row\">\r\n                                            {{row.is_different_location}}</mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <mat-header-row *matHeaderRowDef=\"displayColumns\"\r\n                                        style=\"background-color:#666666;\"></mat-header-row>\r\n\r\n                                    <mat-row *matRowDef=\"let row; columns: displayColumns;\" [style.cursor]=\"'pointer'\"\r\n                                        matRipple></mat-row>\r\n\r\n                                </mat-table>\r\n                                <mat-paginator #SecureServerPaginator=\"matPaginator\" [pageSizeOptions]=\"[ 10, 15, 20]\"\r\n                                    [showFirstLastButtons]=\"true\">\r\n                                </mat-paginator>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n</section>\r\n";

/***/ }),

/***/ 7420:
/*!*******************************************************************************************!*\
  !*** ./src/app/storage-server-conf/define-server/define-server.component.html?ngResource ***!
  \*******************************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <div class=\"container-fluid\">\r\n        <div class=\"row clearfix\">\r\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                <div class=\"card\">\r\n                    <div class=\"row clearfix\">\r\n                        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"list_title\">\r\n                            <div class=\"header\">\r\n                                <h2>\r\n                                    <strong>List of SFTP Server</strong>\r\n                                </h2>\r\n                            </div>\r\n                        </div>\r\n                        <ng-container>\r\n                            <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"btn_new_entry\"\r\n                                style=\"text-align: right;\">\r\n                                <div class=\"header\">\r\n                                    <h2>\r\n                                        <button class=\"btn btn-primary btn-rounded waves-effect\" type=\"button\"\r\n                                            (click)=\"showNewEntry();addNewLink()\"><i class=\"fa  fa-plus\"></i> Add New\r\n                                            SFTP Server</button>\r\n                                    </h2>\r\n                                </div>\r\n                            </div>\r\n                        </ng-container>\r\n                        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"new_entry_title\">\r\n                            <div class=\"header\">\r\n                                <h2>\r\n                                    <strong>Define SFTP Server</strong>\r\n                                </h2>\r\n                            </div>\r\n                        </div>\r\n                        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"btn_list\" style=\"text-align: right;\">\r\n                            <div class=\"header\">\r\n                                <h2>\r\n                                    <button class=\"btn btn-primary btn-rounded waves-effect\" type=\"button\"\r\n                                        (click)=\"showList()\"><i class=\"fa fa-calendar\"></i> List of SFTP Server</button>\r\n                                </h2>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"new_entry_form\" class=\"row clearfix\">\r\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                <form [formGroup]=\"CompanySourceCloudLinkingForm\" (ngSubmit)=\"onSubmit()\">\r\n                    <div class=\"card\">\r\n                        <div class=\"body\">\r\n                            <div class=\"row clearfix\">\r\n                                <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                    <div class=\"form-group\">\r\n                                        <mat-label>Tenant Name <i style=\"color: red;\">*</i></mat-label>\r\n                                        <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                            <input matInput formControlName=\"tenant_id\" hidden>\r\n                                            <input matInput formControlName=\"tenant_name\" placeholder=\"Tenant Name\">\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                    <div class=\"form-group\">\r\n                                        <label class=\"form-label\">Define Multiple SFTP Server? </label>\r\n                                        <br>\r\n                                        <label class=\"form-label\" style=\"padding-bottom: 12px;\">No &nbsp;</label>\r\n                                        <mat-slide-toggle id=\"is_multiple_sftp_definition\"\r\n                                            [checked]=\"is_multiple_sftp_definition\"\r\n                                            formControlName=\"is_multiple_sftp_definition\"\r\n                                            (change)=\"isDifferentLocation($event)\"> Yes</mat-slide-toggle>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                    <div class=\"form-group\">\r\n                                        <label class=\"form-label\">Is Active?</label><br>\r\n                                        <mat-label>No &nbsp;</mat-label>\r\n                                        <mat-slide-toggle id=\"is_active\" (change)=\"changeState($event)\"\r\n                                            formControlName=\"is_active\">Yes</mat-slide-toggle>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"card\">\r\n                        <div class=\"body\">\r\n                            <h5 style=\"text-align: center;\"><strong>Define SFTP Server Details</strong></h5><br>\r\n                            <div class=\"table-responsive\" formArrayName=\"initialItemRow\">\r\n                                <table id=\"tableExport1\"\r\n                                    class=\"display table table-hover table-checkable order-column m-t-20 width-per-100 table-bordered\">\r\n                                    <thead>\r\n                                        <tr>\r\n                                            <th>Action</th>\r\n                                            <th>SFTP Name <label style=\"color: red; font-size: smaller;\">*</label>\r\n                                            </th>\r\n                                            <th>Hostname <label style=\"color: red; font-size: smaller;\">*</label>\r\n                                            </th>\r\n                                            <th>Username <label style=\"color: red; font-size: smaller;\">*</label>\r\n                                            </th>\r\n                                            <th>Password <label style=\"color: red; font-size: smaller;\">*</label>\r\n                                            </th>\r\n                                            <th>Upload Key File </th>\r\n                                            <th>File Path </th>\r\n                                        </tr>\r\n                                    </thead>\r\n\r\n                                    <tbody class=\"main_tbody\">\r\n                                        <tr *ngFor=\"let dynamic of CompanySourceCloudLinkingForm.controls.initialItemRow['controls']; let i = index\"\r\n                                            [formGroupName]=\"i\">\r\n                                            <td>\r\n                                                <ng-container *ngIf=\"i==0\">\r\n                                                    <button type=\"button\" id=\"add_button\"\r\n                                                        class=\"btn btn-primary btn-rounded waves-effect\"\r\n                                                        (click)=\"addNewRow(i)\" [disabled]=\"isDisabled\">+\r\n                                                    </button>\r\n                                                </ng-container>\r\n                                                <ng-container *ngIf=\"i>0\">\r\n                                                    <button type=\"button\"\r\n                                                        class=\"btn btn-danger btn-rounded waves-effect\"\r\n                                                        (click)=\"deleteRow(i)\">-\r\n                                                    </button>\r\n                                                </ng-container>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <input class=\"form-control\" type=\"text\" id=\"sftp_name{{i+1}}\"\r\n                                                    formControlName=\"sftp_name\" style=\"width:130px;\"\r\n                                                    placeholder=\"Enter SFTP Name\" required>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <input class=\"form-control\" type=\"text\" placeholder=\"Enter Host Name\"\r\n                                                    id=\"host_name{{i+1}}\" formControlName=\"host_name\"\r\n                                                    style=\"width:300px;\" required>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <input class=\"form-control\" type=\"text\" id=\"sftp_username{{i+1}}\"\r\n                                                    formControlName=\"sftp_username\" style=\"width:130px;\"\r\n                                                    placeholder=\"Enter SFTP User Name\" required>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <input class=\"form-control\" type=\"text\" id=\"sftp_password{{i+1}}\"\r\n                                                    formControlName=\"sftp_password\" style=\"width:130px;\"\r\n                                                    placeholder=\"Enter SFTP Password\" required>\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <input class=\"form-control\" type=\"file\" id=\"upload_key{{i+1}}\"\r\n                                                    (change)=\"setFileName($event, i)\" style=\"width:230px;\">\r\n                                                <label id=\"upload_key_google_ins{{i+1}}\"\r\n                                                    style=\"color: red; font-size: smaller;\" hidden>*Select json\r\n                                                    format file</label>\r\n                                                <label id=\"upload_key_sftp_ins{{i+1}}\"\r\n                                                    style=\"color: red; font-size: smaller;\" hidden>*Select ppk\r\n                                                    format file</label><br>\r\n                                                <button type=\"button\" id=\"upload_key_button{{i+1}}\"\r\n                                                    class=\"btn btn-secondary btn-rounded waves-effect\"\r\n                                                    style=\"width:230px;\" (click)=\"uploadFile(i)\">Upload</button>\r\n\r\n                                            </td>\r\n\r\n                                            <td class=\"col_input\">\r\n                                                <input class=\"form-control\" type=\"text\" id=\"file_key_path{{i+1}}\"\r\n                                                    formControlName=\"file_key_path\" style=\"width:400px;\"\r\n                                                    placeholder=\"/storage-credential-files/<your_file_name>\">\r\n                                            </td>\r\n                                        </tr>\r\n                                    </tbody>\r\n                                </table>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n\r\n                    <div class=\"card\">\r\n                        <div class=\"body\" style=\"text-align: center;\">\r\n                            <div class=\"button-demo\">\r\n                                <button type=\"submit\" *ngIf=\"submit\" [disabled]=\"!CompanySourceCloudLinkingForm.valid\"\r\n                                    class=\"btn btn-primary btn-border-radius waves-effect\">{{BTN_VAL}}</button>\r\n                                <a mat-raised-button onclick=\"location.reload(true);\" class=\"btn btn-danger\">Cancel</a>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </form>\r\n            </div>\r\n        </div>\r\n    </div>\r\n\r\n    <div *ngIf=\"list_form\">\r\n        <div class=\"row clearfix\">\r\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                <div class=\"card\">\r\n                    <div class=\"body\">\r\n                        <div class=\"table-responsive\">\r\n                            <div class=\"row clearfix\">\r\n                                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                                    <div class=\"dt-buttons btn-group flex-wrap\">\r\n                                        <button class=\"btn btn-secondary buttons-copy buttons-html5\" tabindex=\"0\"\r\n                                            aria-controls=\"example1\" type=\"button\" (click)=\"export_isp_usrs()\">\r\n                                            <span>Excel</span>\r\n                                        </button>\r\n                                    </div>\r\n                                </div>\r\n                                <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\"text-align: right;\">\r\n                                    <mat-form-field appearance=\"fill\">\r\n                                        <mat-label>\r\n                                            <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\r\n                                            Search\r\n                                        </mat-label>\r\n                                        <input matInput (keyup)='tbl_FilterDatatable($event.target.value)'>\r\n                                    </mat-form-field>\r\n                                </div>\r\n                            </div>\r\n\r\n                            <mat-table #table [dataSource]=\"companyServerLinkMasterData\" matSort class=\"mat-cell\">\r\n                                <ng-container matColumnDef=\"action\">\r\n                                    <mat-header-cell *matHeaderCellDef style=\"color: white;\">Actions</mat-header-cell>\r\n                                    <mat-cell *matCellDef=\"let row;\">\r\n                                        <span *ngFor=\"let item of sidebarData\">\r\n                                            <button *ngIf=\"item.read_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                                (click)=\"viewRecord(row)\">\r\n                                                <em class=\"material-icons\">visibility</em>\r\n                                            </button>\r\n                                            <button *ngIf=\"item.write_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                                (click)=\"editRecord(row)\">\r\n                                                <em class=\"material-icons\">mode_edit</em>\r\n                                            </button>\r\n                                        </span>\r\n                                        <!-- <button class=\"btn tblActnBtn h-auto\" data-toggle=\"modal\"\r\n                                                data-target=\"#editModal\" (click)=\"viewCompanyData(row)\">\r\n                                                <i class=\"material-icons\">visibility</i>\r\n                                            </button>\r\n                                            <button class=\"btn tblActnBtn h-auto\" data-toggle=\"modal\"\r\n                                                data-target=\"#editModal\" (click)=\"editCompanyData(row)\">\r\n                                                <i class=\"material-icons\">mode_edit</i>\r\n                                            </button>\r\n                                            <button class=\"btn tblActnBtn h-auto\" (click)=\"deleteCompanyData(row.id)\">\r\n                                                <i class=\"material-icons\">delete</i>\r\n                                            </button> -->\r\n                                    </mat-cell>\r\n                                </ng-container>\r\n\r\n                                <ng-container matColumnDef=\"tenant_name\">\r\n                                    <mat-header-cell mat-header-cell *matHeaderCellDef style=\"color: white;\">Tenant\r\n                                        Name</mat-header-cell>\r\n                                    <mat-cell mat-cell *matCellDef=\"let row\"> {{row.tenant_name}}</mat-cell>\r\n                                </ng-container>\r\n\r\n                                <ng-container matColumnDef=\"is_multiple_sftp_definition\">\r\n                                    <mat-header-cell mat-header-cell *matHeaderCellDef style=\"color: white;\">Is\r\n                                        Multiple SFTP Definition</mat-header-cell>\r\n                                    <mat-cell mat-cell *matCellDef=\"let row\">\r\n                                        {{row.is_multiple_sftp_definition}}</mat-cell>\r\n                                </ng-container>\r\n\r\n                                <mat-header-row *matHeaderRowDef=\"displayColumns\"\r\n                                    style=\"background-color:#666666;\"></mat-header-row>\r\n\r\n                                <mat-row *matRowDef=\"let row; columns: displayColumns;\" [style.cursor]=\"'pointer'\"\r\n                                    matRipple></mat-row>\r\n\r\n                            </mat-table>\r\n                            <mat-paginator #SecureServerPaginator=\"matPaginator\" [pageSizeOptions]=\"[ 10, 15, 20]\"\r\n                                [showFirstLastButtons]=\"true\">\r\n                            </mat-paginator>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n</section>";

/***/ }),

/***/ 2187:
/*!*************************************************************************************************!*\
  !*** ./src/app/storage-server-conf/schedule-process/schedule-process.component.html?ngResource ***!
  \*************************************************************************************************/
/***/ ((module) => {

module.exports = "<section class=\"content\">\r\n    <div class=\"container-fluid\">\r\n\r\n        <div class=\"row clearfix\">\r\n            <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                <div class=\"card\">\r\n                    <div class=\"row clearfix\">\r\n\r\n                        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"list_title\">\r\n                            <div class=\"header\">\r\n                                <h2>\r\n                                    <strong>List of Schedule Process</strong>\r\n                                </h2>\r\n                            </div>\r\n                        </div>\r\n                        <ng-container>\r\n                        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"btn_new_entry\" style=\"text-align: right;\">\r\n                            <div class=\"header\">\r\n                                <h2>\r\n                                    <button class=\"btn btn-primary btn-rounded waves-effect\" type=\"button\"\r\n                                        (click)=\"showNewEntry();addNewLink()\"><i class=\"fa  fa-plus\"></i>Add New Schedule Process</button>\r\n                                </h2>\r\n                            </div>\r\n                        </div>\r\n                        </ng-container>\r\n                        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"new_entry_title\">\r\n                            <div class=\"header\">\r\n                                <h2>\r\n                                    <strong>Define Schedule Process</strong>\r\n                                </h2>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" *ngIf=\"btn_list\" style=\"text-align: right;\">\r\n                            <div class=\"header\">\r\n                                <h2>\r\n                                    <button class=\"btn btn-primary btn-rounded waves-effect\" type=\"button\"\r\n                                        (click)=\"showList()\"><i class=\"fa fa-calendar\"></i>List of Schedule Process</button>\r\n                                </h2>\r\n                            </div>\r\n                        </div>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"new_entry_form\">\r\n            <div class=\"row clearfix\">\r\n                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                    <form [formGroup]=\"CompanySourceCloudLinkingForm\" (ngSubmit)=\"onSubmit()\">\r\n                        <div class=\"card\">\r\n                            <div class=\"body\">\r\n                                <div class=\"row clearfix\">\r\n                                    <input type=\"hidden\" formControlName=\"id\" id=\"id\" [value]=\"\" class=\"form-control\">\r\n                                    <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-8\">\r\n                                        <div class=\"form-group\">\r\n                                            <mat-label>Tenant Name <i style=\"color: red;\">*</i></mat-label>\r\n                                            <mat-form-field class=\"example-full-width\" appearance=\"outline\">                                            \r\n                                                <input matInput formControlName=\"tenant_id\" hidden>\r\n                                                <input matInput formControlName=\"tenant_name\" placeholder=\"Tenant Name\" required>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                    \r\n                                    <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                        <div class=\"form-group\">\r\n                                            <mat-label>From Date<i style=\"color: red;\">*</i></mat-label>\r\n                                            <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                <input matInput [matDatepicker]=\"from_date\" (click)=\"from_date.open()\" readonly id=\"from_date\" [min]=\"todayDate\"\r\n                                                    formControlName=\"from_date\" placeholder=\"Choose Date\" required>\r\n                                                <mat-datepicker-toggle matSuffix [for]=\"from_date\"></mat-datepicker-toggle>\r\n                                                <mat-datepicker #from_date></mat-datepicker>\r\n                                                <mat-error *ngIf=\"CompanySourceCloudLinkingForm.get('from_date')\">Please Select From date</mat-error>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                    \r\n                                    <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                        <div class=\"form-group\">\r\n                                            <mat-label>To Date<i style=\"color: red;\">*</i></mat-label>\r\n                                            <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                <input matInput [matDatepicker]=\"to_date\" (click)=\"to_date.open()\" readonly id=\"to_date\" [min]=\"todayDate\"\r\n                                                    formControlName=\"to_date\" placeholder=\"Choose Date\" required>\r\n                                                <mat-datepicker-toggle matSuffix [for]=\"to_date\"></mat-datepicker-toggle>\r\n                                                <mat-datepicker #to_date></mat-datepicker>\r\n                                                <mat-error *ngIf=\"CompanySourceCloudLinkingForm.get('to_date')\">Please Select To date</mat-error>\r\n                                            </mat-form-field>\r\n                                        </div>\r\n                                    </div>\r\n                                    \r\n                                    <div class=\"col-xl-4 col-lg-4 col-md-4 col-sm-12 mb-4\">\r\n                                        <div class=\"form-group\">\r\n                                            <label class=\"form-label\">Is Active?</label>\r\n                                            <mat-label>&nbsp; &nbsp; No &nbsp;</mat-label>\r\n                                            <mat-slide-toggle id=\"is_active\" (change)=\"changeState($event)\"\r\n                                                formControlName=\"is_active\">Yes</mat-slide-toggle>\r\n                                        </div>\r\n                                    </div>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"card\">\r\n                            <div class=\"body\">\r\n                                <h5 style=\"text-align: center;\"><strong>Schedule Process Details</strong></h5><br>\r\n                                <div class=\"body table-responsive\" formArrayName=\"initialItemRow\">\r\n                                    <table id=\"tableExport1\"\r\n                                        class=\"display table table-hover table-checkable order-column m-t-20 width-per-100 table-bordered\">\r\n                                        <thead>\r\n                                            <tr>\r\n                                                <th>Action</th>\r\n                                                <th>Process Name <label style=\"color: red; font-size: smaller;\">*</label></th>\r\n                                                <th>Process Type <label style=\"color: red; font-size: smaller;\">*</label></th>\r\n                                                <th>Process Start Time <label style=\"color: red; font-size: smaller;\">*</label></th>\r\n                                            </tr>\r\n                                        </thead>\r\n\r\n                                        <tbody class=\"main_tbody\">\r\n\r\n                                            <tr *ngFor=\"let dynamic of CompanySourceCloudLinkingForm.controls.initialItemRow['controls']; let i = index\"\r\n                                                [formGroupName]=\"i\">\r\n\r\n                                                <td>\r\n                                                    <ng-container *ngIf=\"i==0\">\r\n                                                        <button type=\"button\" id=\"add_button\"\r\n                                                            class=\"btn btn-primary btn-rounded waves-effect\"\r\n                                                            (click)=\"addNewRow(i)\" [disabled]=\"isDisabled\">+\r\n                                                        </button>\r\n                                                    </ng-container>\r\n                                                    <ng-container *ngIf=\"i>0\">\r\n                                                        <button type=\"button\"\r\n                                                            class=\"btn btn-danger btn-rounded waves-effect\"\r\n                                                            (click)=\"deleteRow(i)\">-\r\n                                                        </button>\r\n                                                    </ng-container>\r\n                                                </td>\r\n\r\n                                                <td class=\"col_input\">\r\n                                                    <input class=\"form-control\" type=\"text\" id=\"process_name{{i+1}}\" formControlName=\"process_name\"  required>\r\n                                                </td>\r\n\r\n                                                <td class=\"col_input\">\r\n                                                    <mat-form-field class=\"example-full-width\" appearance=\"outline\">\r\n                                                        <mat-select formControlName=\"process_id\" required>\r\n                                                            <mat-option value=\"\">Select</mat-option>\r\n                                                            <mat-option [value]=\"obj.id\" *ngFor=\"let obj of scheduleProcessData\">{{obj.master_value}}</mat-option>\r\n                                                        </mat-select>\r\n                                                    </mat-form-field>\r\n                                                </td>\r\n\r\n                                                <td class=\"col_input\">\r\n                                                    <input class=\"form-control\" type=\"time\" id=\"process_start_time{{i+1}}\" formControlName=\"process_start_time\" style=\"width:330px;\" required>\r\n                                                </td>\r\n                                            </tr>\r\n                                        </tbody>\r\n                                    </table>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div class=\"card\">\r\n                            <div class=\"body\" style=\"text-align: center;\">\r\n                                <div class=\"button-demo\">\r\n                                    <button type=\"submit\" *ngIf=\"submit\" [disabled]=\"!CompanySourceCloudLinkingForm.valid\" class=\"btn btn-primary btn-border-radius waves-effect\">{{BTN_VAL}}</button>\r\n                                    <a mat-raised-button onclick=\"location.reload(true);\" class=\"btn btn-danger\">Cancel</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </form>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n        <div *ngIf=\"list_form\">\r\n            <div class=\"row clearfix\">\r\n                <div class=\"col-lg-12 col-md-12 col-sm-12 col-xs-12\">\r\n                    <div class=\"card\">\r\n                        <div class=\"body\">\r\n                            <div class=\"table-responsive\">\r\n                                <div class=\"row clearfix\">\r\n                                    <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\">\r\n                                        <div class=\"dt-buttons btn-group flex-wrap\">\r\n                                            <button class=\"btn btn-secondary buttons-copy buttons-html5\"\r\n                                                tabindex=\"0\" aria-controls=\"example1\" type=\"button\"\r\n                                                (click)=\"export_isp_usrs()\">\r\n                                                <span>Excel</span>\r\n                                            </button>\r\n                                        </div>\r\n                                    </div>\r\n                                    <div class=\"col-lg-6 col-md-6 col-sm-6 col-xs-12\" style=\"text-align: right;\">\r\n                                        <mat-form-field appearance=\"fill\">\r\n                                            <mat-label>\r\n                                            <i class=\"fa fa-search\" aria-hidden=\"true\"></i>\r\n                                            Search\r\n                                            </mat-label>\r\n                                                <input matInput (keyup)='tbl_FilterDatatable($event.target.value)'>\r\n                                        </mat-form-field>\r\n                                    </div>\r\n                                </div>\r\n\r\n                                <mat-table #table [dataSource]=\"companyServerLinkMasterData\" matSort class=\"mat-cell\">\r\n                                    <ng-container matColumnDef=\"actions\">\r\n                                        <mat-header-cell *matHeaderCellDef style=\"color: white;\">Actions</mat-header-cell>\r\n                                        <mat-cell *matCellDef=\"let row;\">\r\n                                            <span *ngFor=\"let item of sidebarData\">\r\n                                                <button *ngIf=\"item.read_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                                    (click)=\"viewRecord(row)\">\r\n                                                    <em class=\"material-icons\">visibility</em>\r\n                                                </button>\r\n                                                <button *ngIf=\"item.write_access==true\" class=\"btn tblActnBtn h-auto\"\r\n                                                    (click)=\"editRecord(row)\">\r\n                                                    <em class=\"material-icons\">mode_edit</em>\r\n                                                </button>\r\n                                            </span>\r\n                                                <!-- <button class=\"btn tblActnBtn h-auto\" data-toggle=\"modal\" data-target=\"#editModal\" (click)=\"viewCompanyData(row)\">\r\n                                                    <i class=\"material-icons\">visibility</i>\r\n                                                </button>\r\n                                                <button class=\"btn tblActnBtn h-auto\" data-toggle=\"modal\" data-target=\"#editModal\" (click)=\"editCompanyData(row)\">\r\n                                                    <i class=\"material-icons\">mode_edit</i>\r\n                                                </button>\r\n                                                <button class=\"btn tblActnBtn h-auto\" (click)=\"deleteCompanyData(row.id)\">\r\n                                                    <i class=\"material-icons\">delete</i>\r\n                                                </button> -->\r\n                                        </mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"company_name\">\r\n                                        <mat-header-cell mat-header-cell *matHeaderCellDef style=\"color: white;\">Tenant Name</mat-header-cell>\r\n                                        <mat-cell mat-cell *matCellDef=\"let row\" > {{row.tenant_name}}</mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <ng-container matColumnDef=\"is_active\">\r\n                                        <mat-header-cell mat-header-cell *matHeaderCellDef style=\"color: white;\">Is Active</mat-header-cell>\r\n                                        <mat-cell mat-cell *matCellDef=\"let row\" > {{row.is_active}}</mat-cell>\r\n                                    </ng-container>\r\n\r\n                                    <mat-header-row *matHeaderRowDef=\"displayColumns\" style=\"background-color:#666666;\"></mat-header-row>\r\n\r\n                                    <mat-row *matRowDef=\"let row; columns: displayColumns;\" [style.cursor]=\"'pointer'\" matRipple></mat-row>\r\n\r\n                                </mat-table>\r\n                                <mat-paginator #SecureServerPaginator=\"matPaginator\" [pageSizeOptions]=\"[ 10, 15, 20]\" [showFirstLastButtons]=\"true\">\r\n                                </mat-paginator>\r\n                            </div>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n\r\n    </div>\r\n</section>";

/***/ })

}]);
//# sourceMappingURL=src_app_storage-server-conf_storage-server-conf_module_ts.js.map