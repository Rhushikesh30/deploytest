FROM nginx:1.18
RUN rm /usr/share/nginx/html/index.html
RUN apt-get update && apt-get install -y git
RUN git clone https://gitlab.com/Rhushikesh30/deploytest.git /tmp
RUN ls -l

COPY dist/ /usr/share/nginx/html
